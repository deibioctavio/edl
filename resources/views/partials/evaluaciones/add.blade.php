<script type="text/javascript">
	$(document).ready(function(){

		var profileData = null;
		var areaDataData = null;
		var cargoDataData = null;
		var selectedProfile = null;
		var selectedDocumentId = null;
		
		$( "#periodo_evaluado_inicio" )
			.datepicker({ 
							dateFormat: 'yy-mm-dd',
							changeMonth: true,
							changeYear: true,
							yearRange:'-99:+10'
						});
			
		$( "#periodo_evaluado_fin" )
			.datepicker({ 
							dateFormat: 'yy-mm-dd',
							changeMonth: true,
							changeYear: true,
							yearRange:'-99:+10'
						});

		$( "#fecha_fijacion_compromiso" )
			.datepicker({ 
							dateFormat: 'yy-mm-dd',
							changeMonth: true,
							changeYear: true,
							yearRange:'-99:+10'
						});
		
		$("#register-frm").validate({

	        rules: {
	            
	            periodo_evaluado_inicio: 
	            { 
	            	required: true
	            },
	            periodo_evaluado_fin: 
	            { 
	            	required: true,
	            	dateGreaterThan: {
	            						anotherDate: $('input[name=periodo_evaluado_inicio]')
	            					}
	            },
	            fecha_fijacion_compromiso: 
	            { 
	            	required: true,
	            	dateGreaterOrEqualThan: {
	            						anotherDate: $('input[name=periodo_evaluado_inicio]')
	            					},
	            	dateLessThan: {
	            						anotherDate: $('input[name=periodo_evaluado_fin]')
	            					}
	            },
	            proposito: { required: true},
	        },	      
	        messages: {
	           
	            periodo_evaluado_inicio: {
	                required: "Seleccione la fecha"
	            },
	            periodo_evaluado_fin: {
	                required: "Seleccione la fecha"
	            },
	            fecha_fijacion_compromiso: {
	                required: "Ingrese la fecha"
	            },
	            fecha_fijacion_compromiso: {
	                required: "Seleccione la fecha"
	            }
	            ,
	            proposito: {
	                required: "Diligencie este campo"
	            }
	        },
	        
	        submitHandler: function(form) {
	            form.submit();
	        }		
		});

		//$('input[name=periodo_evaluado_inicio]').rules('add',{dateGreaterThan:$('input[name=periodo_evaluado_fin]')});

		$( "#documento" ).rules( "add", 
			{
  				required: true,
  				messages: {required: "Ingrese el número de documento"}
			});

		$( "#documentoe1" ).rules( "add", 
			{
  				required: true,
  				messages: {required: "Ingrese el número de documento"}
			});

		$( "#documentoe2" ).rules( "add", 
			{
  				required: true,
  				messages: {required: "Ingrese el número de documento"}
			});

		// Evaluado

		$('input[item=evaluado][name=documento]').autocomplete({
      		source: function( request, response ) {
        		$.ajax({
          			url: base_url+"/getbyuserid",
          			dataType: "json",
          			data: {
            				id: request.term,
            				_token: $('input[name=_token]').val()
          			},
          			type: "post",
	          		success: function( data ) {
	            		profileData = data.profile;
	            		areaData = data.area;
	            		cargoData = data.cargo;
	            		//console.log(data);
	            		var array = new Array();

	            		$.each(profileData,function(index,item){
	            			array.push(item.document_number);
	            		});
	            		
	            		response( array );
	            		//console.log(array);
	          		}
        		});
      		},
      		
      		minLength: 2,
    		
    		select: function( event, ui ) {

		    },
      		open: function() {
        		
      		},
      		close: function() {
        		//console.log(profileData);
        		selectedDocumentId = $('input[item=evaluado][name=documento]').val();
		    	//console.log("selectedDocumentId: "+ selectedDocumentId);

		    	$.each(profileData,function(index,item){
	            	
	            	if (item.document_number == selectedDocumentId){
	            		selectedProfile = item;
	            	}
	            });

	            $('#usuario_evaluado_id').val(selectedProfile.user_id);
	            $('#usuario_evaluado_cargo_id').val(selectedProfile.cargo_id);
	            $('#usuario_evaluado_area_id').val(selectedProfile.area_id);

	            $('input[item=evaluado][id=nombre_apellido]').val(selectedProfile.name + " " + selectedProfile.lastname);
	            $('input[item=evaluado][id=nivel_jerarquico]').val(selectedProfile.cargo);
	            $('input[item=evaluado][id=area]').val(selectedProfile.area);

	            //console.log(selectedProfile);
      		}
    	});

		// Evaluador 1

		$('input[item=evaluador1]').autocomplete({
      		source: function( request, response ) {
        		$.ajax({
          			url: base_url+"/getbyuserid",
          			dataType: "json",
          			data: {
            				id: request.term,
            				_token: $('input[name=_token]').val()
          			},
          			type: "post",
	          		success: function( data ) {
	            		profileData = data.profile;
	            		areaData = data.area;
	            		cargoData = data.cargo;
	            		//console.log(data);
	            		var array = new Array();

	            		$.each(profileData,function(index,item){
	            			array.push(item.document_number);
	            		});
	            		
	            		response( array );
	            		//console.log(array);
	          		}
        		});
      		},
      		
      		minLength: 2,
    		
    		select: function( event, ui ) {

		    },
      		open: function() {
        		
      		},
      		close: function() {
        		//console.log(profileData);
        		selectedDocumentId = $('input[item=evaluador1]').val();
		    	//console.log("selectedDocumentId: "+ selectedDocumentId);

		    	$.each(profileData,function(index,item){
	            	
	            	if (item.document_number == selectedDocumentId){
	            		selectedProfile = item;
	            	}
	            });

	            $('#usuario_evaluador1_id').val(selectedProfile.user_id);

	            $('input[item=evaluador1][id=nombre_apellido]').val(selectedProfile.name + " " + selectedProfile.lastname);
	            $('input[item=evaluador1][id=nivel_jerarquico]').val(selectedProfile.cargo);
	            $('input[item=evaluador1][id=area]').val(selectedProfile.area);

	            //console.log(selectedProfile);
      		}
    	});

		// Evaluador 2

		$('input[item=evaluador2]').autocomplete({
      		source: function( request, response ) {
        		$.ajax({
          			url: base_url+"/getbyuserid",
          			dataType: "json",
          			data: {
            				id: request.term,
            				_token: $('input[name=_token]').val()
          			},
          			type: "post",
	          		success: function( data ) {
	            		profileData = data.profile;
	            		areaData = data.area;
	            		cargoData = data.cargo;
	            		//console.log(data);
	            		var array = new Array();

	            		$.each(profileData,function(index,item){
	            			array.push(item.document_number);
	            		});
	            		
	            		response( array );
	            		//console.log(array);
	          		}
        		});
      		},
      		
      		minLength: 2,
    		
    		select: function( event, ui ) {

		    },
      		open: function() {
        		
      		},
      		close: function() {
        		//console.log(profileData);
        		selectedDocumentId = $('input[item=evaluador2]').val();
		    	//console.log("selectedDocumentId: "+ selectedDocumentId);

		    	$.each(profileData,function(index,item){
	            	
	            	if (item.document_number == selectedDocumentId){
	            		selectedProfile = item;
	            	}
	            });

	            $('#usuario_evaluador2_id').val(selectedProfile.user_id);

	            $('input[item=evaluador2][id=nombre_apellido]').val(selectedProfile.name + " " + selectedProfile.lastname);
	            $('input[item=evaluador2][id=nivel_jerarquico]').val(selectedProfile.cargo);
	            $('input[item=evaluador2][id=area]').val(selectedProfile.area);

	            //console.log(selectedProfile);
      		}
    	});
	});
</script>