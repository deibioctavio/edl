<script type="text/javascript">

	var metasArray = new Array();
	var totalPactado = 0;

	function metasArrayCheckAdd(jsObject){

		var localObject = null;

		jQuery.each(metasArray, function(index,element) {
			
			if(element.key == jsObject.key)
				localObject = jsObject;
		});

		if( localObject == null ){
			metasArray.push(jsObject);
		}
	}

	function metasArrayGetByKey(key){

		var tmp = null;

		jQuery.each(metasArray, function(index,element) {
			
			if(element.key == key){
				tmp = element;
			}
		});

		return tmp;
	}

	function calculateTotalPactado(){

		totalPactado = 0;

		$('select[id^=procentaje_cumpimiento_pactado-]').each(

			function(index,element)
			{
				var val = parseInt( $(element).val() );
				totalPactado += val;
    		}
        );
		console.log(totalPactado);
        $('#total_pactado').val(totalPactado);
        //$('#total-pactado-container').html(totalPactado);
	}

	function showMetasByProyectoIdComboBox(element){

		var selectNumberId = $(element).attr('number');
		var proyectoId = $(element).val();
		var localElement = null;
		var selectDesc = 0;
		//console.log(proyectoId);
		$.ajax(
			    {
			        type: "get",
			        url: base_url+'/evaluaciones/getmetasbyproyectoidajax/'+proyectoId,
			        data: {},
			        beforeSend: function(){
			        	$('div[id=container-meta-proyecto-'+selectNumberId+']').html(jsAjaxLoadingImageObjectString24);
			        	$('input[type=submit][value=Registrar]').addClass('hidden');
			        },
			        success: function(response)
			        {
			        	
			        	//console.log(response);

			        	var select = '<select id="meta-'+selectNumberId+'" name="meta['+selectNumberId+']">';

			        	jQuery.each(response, function(index,element) {
			        		select += '<option value="'+element.id+'">'+element.name+'</option>';

			        		jsObject = { key: element.id, value: element.description};

			        		metasArrayCheckAdd(jsObject);
			        	});

			        	select += '</select>';

			        	//console.log("LENGTH: " + metasArray.length);

			        	$('div[id=container-meta-proyecto-'+selectNumberId+']').html(select);

			        	$('select[id=meta-'+selectNumberId+']').bind('change',function(){
			        		
			        		var lValue = metasArrayGetByKey( $(this).val() );
			        		$('div[id=container-meta-proyecto-descripcion-'+selectNumberId+']').html(lValue.value);
			        	});

			        	selectDesc = $('select[id=meta-'+selectNumberId+']').val();
			        	localElement = metasArrayGetByKey(selectDesc);

			        	$('div[id=container-meta-proyecto-descripcion-'+selectNumberId+']').html(localElement.value);
			        	$('input[type=submit][value=Registrar]').removeClass('hidden');
			        },
			        error: function(xhr, textStatus, errorThrown)
			        {
			            alert('error cargando el ajax '+url + query);
			            return false;
			        }
			    });
	}

	$(document).ready(function(){

		$("#register-frm").validate({

	        rules: {

	            total_pactado: 
					{ 
						required: true,
						equalTo: "#total"
					}
	        },
	        
	        // Specify the validation error messages
	        messages: {
	           
	            total_pactado: {
	                required: "Valor requerido",
	                equalTo: "El total de los valores de los compromisos debe ser igual a 100%"
	            }
	        },
	        submitHandler: function(form) {
	            form.submit();
	        }			
		});

		$('select[id=proyecto]').each(

			function(index,element){
				
				$(element).on('change',function(){
					showMetasByProyectoIdComboBox(this);
				});

				showMetasByProyectoIdComboBox(element);
			}
		);

		$('select[id^=procentaje_cumpimiento_pactado-]').each(

			function(index,element)
			{
				$(element).on('change',function(){
					calculateTotalPactado();
				})
    		}
        );


		calculateTotalPactado();

        //$('input[name=total_pactado]').rules('add',{validate100:100});
	});
</script>