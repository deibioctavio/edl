<?php

  $_formId = isset($formId)?$formId:0;
  $_uploadControllerRoute = isset($uploadControllerRoute)?$uploadControllerRoute:"addajax";
  $_uploadControllerRouteAlias = isset($uploadControllerRouteAlias)?$uploadControllerRouteAlias:"addentryajax";
  $_uploadLinkName = isset($uploadLinkName)?$uploadLinkName:"Subir Archivo";
  $_hiddemForm = isset($hiddemForm)?$hiddemForm:true;

  $hidenDivText = $_hiddemForm?' class="hidden "':"";
?>
<script type="text/javascript">
    $(document).ready(function(){
        
        /*Add new catagory Event*/

        var filename<?php echo $_formId?> = "";

        $('#loading-animation<?php echo $_formId?>').hide();
        $('#anchor-link-delete<?php echo $_formId?>').hide();

        $('#anchor-link-delete<?php echo $_formId?>').click(function(){
          $('#loading-animation<?php echo $_formId?>').hide();
          $('#anchor-link-delete<?php echo $_formId?>').hide();
          $("#ajax-submit<?php echo $_formId?>").show();
        });        

        $("#ajax-submit<?php echo $_formId?>").click(function(e){

            e.preventDefault();
            $('#filefield<?php echo $_formId?>').trigger("click");            
         });

        $("#filefield<?php echo $_formId?>").click(function(){

            filename<?php echo $_formId?> = $('input[type=file][id=filefield<?php echo $_formId?>]').val();

            console.log(filename<?php echo $_formId?>);

            //$("#ajax-submit<?php echo $_formId?>").html(filename<?php echo $_formId?>);
            $("#ajax-submit<?php echo $_formId?>").hide();
            //$('#loading-animation<?php echo $_formId?>').show();
            $('#anchor-link-delete<?php echo $_formId?>').hide();

         });

        $("#filefield<?php echo $_formId?>").on("change",function(){

            $('#loading-animation<?php echo $_formId?>').show();

            $.ajax({
                        url:'fileentry/<?php echo $_uploadControllerRoute?>',
                        data:new FormData($("#upload_form<?php echo $_formId?>")[0]),
                        dataType:'json',
                        async:false,
                        type:'post',
                        processData: false,
                        contentType: false,
                        success:function(response){
                          console.log(response);
                          console.log(filename<?php echo $_formId?>);

                          //$("#ajax-submit<?php echo $_formId?>").html(response.filedata.filename+ " >> Clic para subir otro" );
                          $('#loading-animation<?php echo $_formId?>').hide();
                          $('#anchor-link-delete<?php echo $_formId?>').show();
                        },
            });
        });

        /*Add new catagory Event*/
    });
</script>
<div id="UploadFormDiv<?php echo $_formId?>" <?php echo $hidenDivText?>>
  <form 
        action="{{route('<?php echo $_uploadControllerRouteAlias?>', [])}}" 
        method="post" enctype="multipart/form-data" 
        id="upload_form<?php echo $_formId?>"
  >
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <input type="hidden" name="element_id" value="<?php echo $_formId?>" />
    <input type="file" name="filefield<?php echo $_formId?>" id="filefield<?php echo $_formId?>">
  </form>  
</div>
<div class="row-fluid">
    <div id="anchor-link<?php echo $_formId?>">
      <a href="#" id="ajax-submit<?php echo $_formId?>"><?php echo $_uploadLinkName?></a>  
    </div>
    <div id="loading-animation<?php echo $_formId?>">
      <img src="{{ asset('assets/images/loading.gif') }}" height="16" width="16" />
    </div>
    <div id="anchor-link-delete<?php echo $_formId?>">
      <a href="#" id="ajax-delete<?php echo $_formId?>">Quitar</a>  
    </div>
</div>