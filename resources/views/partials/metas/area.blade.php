<script type="text/javascript">
    $(document).ready(function(){
		
		var areasJson = <?php echo $areas?>;
		var selectedAera = <?php echo isset($area->id)?$area->id:0 ?>;
		var proyectosJson = <?php echo $proyectos?>;
		var selectedProyecto = <?php echo isset($proyecto->id)?$proyecto->id:0 ?>;

		$('select[name=area_id][id=area_id]').on("change",recargar_listado_proyectos);

		if( cargarProyectosDefault == true ){
			recargar_listado_proyectos();
		}
    });

    function recargar_listado_proyectos(){

    	var newSelectedArea = $('select[name=area_id][id=area_id]').val();

    	$.ajax({
		                url:base_url+'/proyectolistjson/'+newSelectedArea,
		                dataType:'json',
		                async:false,
		                type:'get',
		                processData: false,
		                contentType: false,
		                success:function(response){
		                  var selectHTML = "";
		                  selectHTML += '<select id="proyecto_id" name="proyecto_id">';

							$.each(response, function(index,element) {
								
								selectHTML += '<option value="'+element.id+'">'+element.name+'</option>';
							});

		                  selectHTML += "</select>";

		                  $("div[id=proyectos-list-container]").html(selectHTML);
		                },
		    });
    }
</script>