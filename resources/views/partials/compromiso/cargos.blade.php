<script type="text/javascript">
    $(document).ready(function(){
		
		var nivelesJson = <?php echo $niveles?>;
		var selectedNivel = <?php echo isset($nivel->id)?$nivel->id:0 ?>;
		var cargoJson = <?php echo $cargos?>;
		var selectedCargo = <?php echo isset($cargo->id)?$cargo->id:0 ?>;

		$('select[name=nivel_jerarquico_id][id=nivel_jerarquico_id]').on("change",recargar_listado_cargos);

		if( cargarCargosDefault == true ){
			recargar_listado_cargos();
		}
    });

    function recargar_listado_cargos(){

    	var newSelectedNivel = $('select[name=nivel_jerarquico_id][id=nivel_jerarquico_id]').val();

    	$.ajax({
		                url:base_url+'/cargoslistjson/'+newSelectedNivel,
		                dataType:'json',
		                async:false,
		                type:'get',
		                processData: false,
		                contentType: false,
		                success:function(response){
		                  var selectHTML = "";
		                  selectHTML += '<select id="cargo_id" name="cargo_id">';

							$.each(response, function(index,element) {
								
								selectHTML += '<option value="'+element.id+'">'+element.name+'</option>';
							});

		                  selectHTML += "</select>";

		                  $("div[id=cargo-list-container]").html(selectHTML);
		                },
		    });
    }
</script>