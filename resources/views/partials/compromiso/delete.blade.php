<script type="text/javascript">
    $( document ).ready( function(){

        $("a[id=compromiso-del]").confirm({
            title:"Confirmar eliminación",
            text: "Este procedimiento es irreversible y no se puede deshacer. Los datos eliminados no se podrán recuperar. ¿Está seguro que querer realizarlo?",
            confirm: function(button) {
               confirmarBorrar(button.attr("number"));
            },
            cancel: function(button) {
                button.fadeOut(2000).fadeIn(2000);
                alert("Procedimiento de borrado cancelado.");
            },
            confirmButton: "Si, borrar",
            cancelButton: "No, cancelar borrado"
        });
    });

  function confirmarBorrar(id){
     var e = $("a[id=compromiso-del][number="+id+"]");
     var h = e.attr("href");
     window.location =  e.attr("href");;
  }
</script>