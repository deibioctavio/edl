<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{URL::to('/')}}">
                {!! Html::image('assets/images/logo-edl-default.png', 'Evaluación de Desempeño Laboral - EDL, Armenia - Quindío '.date('Y'), array('class' => 'raleway-logo')) !!}
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse navHeaderCollapse" role="navigation">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{URL::to('/')}}">Inicio</a></li>
                @if (Auth::guest())
                    <li><a href="{{route('auth/login')}}">Autenticación</a></li>
                @else
                    <li>
                        <a href="<?php echo route('profile');?>">{{ Auth::user()->name }}</a>
                    </li>
                    @role('admin')
                        @include('partials.page.menuadmin')
                    @endrole
                    @role('asistente')
                        @include('partials.page.menuasistente')
                    @endrole
                    @role('evaluador')
                        @include('partials.page.menuevaluador')
                    @endrole
                    @role('funcionario')
                        @include('partials.page.menufuncionario')
                    @endrole
                    
                    <li><a href="{{route('auth/logout')}}">Salir</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>