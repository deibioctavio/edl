<li id="crud_evaluacion" class="dropdown">
    <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
        Evaluación
        <span class="caret"></span>
        <ul role="menu" class="dropdown-menu">
            <li><a href="{{route('evaluacionadd')}}">Crear Evaluación</a></li>
            <li><a href="{{route('evaluacionshowallbyevaluador')}}">Listar Evaluaciones</a></li>
            
        </ul>
    </a>
</li>
<li id="crud_evidencia" class="dropdown">
    <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
        Evidencias
        <span class="caret"></span>
        <ul role="menu" class="dropdown-menu animated fadeIn dropdown-animation">
            <li><a href="#">Evidencias 1</a></li>
            <li><a href="#">Evidencias 2</a></li>
        </ul>
    </a>
</li>