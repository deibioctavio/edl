<li id="crud_admin" class="dropdown dropdown-main">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        Administrar
        <span class="caret"></span>
    </a>
        <ul class="dropdown-menu dropdown-subhover dropdown-animation animated fadeIn">
            <?php
                $routes = getRoutesByUserId(Auth::user()->id);
                $tmp = array();
                $i = 0;
                foreach ($routes as $r) {
                    
                    if($r->menu_slug == 'admin'){
                        $tmp[$r->group][$i]['route'] = $r->route;
                         $tmp[$r->group][$i]['description'] = $r->description;
                         $i++;
                    }
                }

                foreach ($tmp as $k => $v){
            ?>
                <li class="dropdown dropdown-submenu">
                    <a class="trigger"><?php echo ucfirst($k);?></a>
                    <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
            <?php
                    foreach ($v as $element){
                        
                        $url = route($element['route']);
            ?>
                        <li>
                            <a tabindex="-1" href="{{ $url }}">{{ $element['description'] }}</a>
                        </li>
            <?php
                    }
            ?>
                    </ul>                                       
                </li> 
            <?php        
                    
                }
            ?>
            <li>
                <a href="{{route('auth/register')}}"> Registra usuarios </a>
            </li>                             
        </ul>
</li>
<li id="crud_evaluacion" class="dropdown dropdown-main">
    <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
        Evaluación
        <span class="caret"></span>
        <ul role="menu" class="dropdown-menu animated fadeIn dropdown-animation">
            <li><a href="{{route('evaluacionadd')}}">Crear Evaluación</a></li>
            <li><a href="{{route('evaluacionshowallbyevaluador')}}">Listar Evaluaciones</a></li>
        </ul>
    </a>
</li>
<!--
<li id="crud_evidencia" class="dropdown dropdown-main">
    <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
        Evidencias
        <span class="caret"></span>
        <ul role="menu" class="dropdown-menu animated fadeIn dropdown-animation">
            <li><a href="#">Evidencias 1</a></li>
            <li><a href="#">Evidencias 2</a></li>
            <li><a href="#">Evidencias 3</a></li>
            <li><a href="#">Evidencias 4</a></li>
        </ul>
    </a>
</li>
<li id="crud_reporte" class="dropdown dropdown-main">
    <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
        Reportes
        <span class="caret"></span>
        <ul role="menu" class="dropdown-menu animated fadeIn dropdown-animation">
            <li><a href="#">Reporte 1</a></li>
            <li><a href="#">Reporte 2</a></li>
            <li><a href="#">Reporte 3</a></li>
        </ul>
    </a>
</li>
-->