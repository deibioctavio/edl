<head>
    <meta charset="utf-8"/>
    <meta name="author" content="Deibi Octavio Riascos Botero" />
    <meta name="description" content="Evaluación del Desempeño Laboral | HTML5, CSS3, Responsive Parallax Bootrap 3 Template">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon" href="{{ asset('assets/images/favicon.png') }}">
    <title>Evaluación del Desempeño Laboral</title>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- Bootstrap Latest compiled and minified CSS -->
    {!! Html::style('assets/css/relaway/royal_preloader.css') !!}

    {!! Html::script('assets/js/relaway/jquery2.1.3.min.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/relaway/royal_preloader.min.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/jquery.validate.min.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/forms-customs-validations.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/jquery.confirm.min.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/jquery-ui.min.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/utils/customValidationRules.js', array('type' => 'text/javascript')) !!}
    
    <script type="text/javascript">
        Royal_Preloader.config({
            mode:           'progress',
            showProgress:   true,
            background:     '#ffffff'
        });

        var base_url = '<?php echo $app->make('url')->to('/')?>';

        var jsAjaxLoadingImageObjectString = '<img src="{{ asset('assets/images/loading.gif') }}" />';
        var jsAjaxLoadingImageObjectString16 = '<img width="16" height="16" src="{{ asset('assets/images/loading.gif') }}" />';
        var jsAjaxLoadingImageObjectString24 = '<img width="24" height="24" src="{{ asset('assets/images/loading.gif') }}" />';
        var jsAjaxLoadingImageObjectString32 = '<img width="32" height="32" src="{{ asset('assets/images/loading.gif') }}" />';

        $(document).ready(function(){

            $(".dropdown-menu > li > a.trigger").on("click", function(e) {
                
                $("ul[id=final-level]").each(function(){
                    $(this).slideUp();
                });

                var current = $(this).next();
                current.toggle();
                e.stopPropagation();
            });
        });
    </script>

    {!! Html::style('assets/css/relaway/bootstrap.min.css') !!}
    {!! Html::style('assets/css/relaway/style.css') !!}
    {!! Html::style('assets/css/relaway/bootstrap-social.css') !!}
    {!! Html::style('assets/css/relaway/animate.min.css') !!}
    {!! Html::style('assets/css/relaway/owl.carousel.css') !!}
    {!! Html::style('assets/css/relaway/jquery.snippet.css') !!}
    {!! Html::style('assets/css/relaway/buttons.css') !!}
    {!! Html::style('assets/css/relaway/colors/blue.css') !!}
    {!! Html::style('assets/css/relaway/ionicons.min.css') !!}
    {!! Html::style('assets/css/relaway/font-awesome.css') !!}
    {!! Html::style('assets/css/relaway/magnific-popup.css') !!}
    @if (!isset($isReportView))
        {!! Html::style('assets/css/jumbotron-narrow.css') !!}
    @endif
    {!! Html::style('assets/css/jquery-ui.css') !!}
    {!! Html::style('assets/css/others.css') !!}
 </head>