<script type="text/javascript">
	$(document).ready(function(){
		
		$("#register-frm").validate({

	        rules: {
	            
	            name: { required: true}
	        },
	        
	        // Specify the validation error messages
	        messages: {
	           
	            name: {
	                required: "Ingrese el nombre del cargo"
	            }
	        },
	        
	        submitHandler: function(form) {
	            form.submit();
	        }			
		});
	});
</script>