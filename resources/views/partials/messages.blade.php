@if (Session::has('flash_success_message'))
    <div class="alert alert-success" role="alert">{{ Session::get('flash_success_message') }}</div>
@endif
@if (Session::has('flash_info_message'))
    <div class="alert alert-info" role="alert">{{ Session::get('flash_info_message') }}</div>
@endif
@if (Session::has('flash_warning_message'))
    <div class="alert alert-warning" role="alert">{{ Session::get('flash_warning_message') }}</div>
@endif

@if (Session::has('flash_error_message'))
    <div class="alert alert-danger" role="alert">{{ Session::get('flash_error_message') }}</div>
@endif