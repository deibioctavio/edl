@extends('app')
 
@section('content')
<script type="text/javascript">
    var cargarProyectosDefault = false;
</script>
@include('partials.metas.area')
@include('partials.proyectos.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Metas</h2>
            <h4>Diligencie la información de la Meta</h4>
            @if (count($areas) < 1 || count($proyectos) < 1)
                <div class="form-group right"> 
                        <div class="col-sm-12">
                            No existen áreas ó proyectos Registradas
                        </div>
                </div>
                <div>                            
                        <input id="regresar" route="metasshowall" type="button" value="Regresar" class="btn btn-rw btn-primary center-block">
                </div>
            @elseif (count($areas) >= 1)
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('metaedit', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="id" class="form-control" value="{{ $meta->id }}">
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione el Área</label>
                    <div class="col-sm-9">
                        <select name="area_id" id="area_id">
                        @foreach ($areas as $a)
                            @if( $a->id === $proyecto->area_id)
                                <option value="{{ $a->id }}" selected>{{ $a->name }}</option>
                            @else
                                <option value="{{ $a->id }}">{{ $a->name }}</option>
                            @endif 
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione el Proyecto</label>
                    <div class="col-sm-9" id="proyectos-list-container">
                        <select name="proyecto_id" id="proyecto_id">
                        @foreach ($proyectos as $p)
                            @if( $p->id === $proyecto->id)
                                <option value="{{ $p->id }}" selected>{{ $p->name }}</option>
                            @else
                                <option value="{{ $p->id }}">{{ $p->name }}</option>
                            @endif 
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre Corto (sin espacios)</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control" value="{{ $meta->name }}">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Nombre Completo</label>
                     <div class="col-sm-9">
                        <input type="description" name="description" class="form-control" value="{{ $meta->description }}">
                     </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Vigencia</label>
                    <div class="col-sm-3">
                        <label class="col-sm-3"><?php echo $meta->vigencia?></label>
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Estado</label>
                     <div class="col-sm-9">
                        <select name="active">
                            <option value="0" <?php echo ($meta->active == 0)?"selected":""?>>inactivo</option>
                            <option value="1" <?php echo ($meta->active == 1)?"selected":""?>>activo</option>
                        </select>
                     </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            @endif 
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection