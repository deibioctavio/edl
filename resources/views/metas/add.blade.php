@extends('app')
 
@section('content')
<script type="text/javascript">
    var cargarProyectosDefault = true;
</script>
@include('partials.metas.area')
@include('partials.proyectos.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Metas</h2>
            <h4>Diligencie la información de la Meta</h4>
            @if (count($areas) < 1 || count($proyectos) < 1)
                <div class="form-group right"> 
                        <div class="col-sm-12">
                            No existen áreas ó proyectos Registradas
                        </div>
                </div>
                <div>                            
                        <input id="regresar" route="metasshowall" type="button" value="Regresar" class="btn btn-rw btn-primary center-block">
                </div>
            @elseif (count($areas) >= 1)
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('metaadd', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione el Área</label>
                    <div class="col-sm-9">
                        <select name="area_id" id="area_id">
                        @foreach ($areas as $a)
                            <option value="{{ $a->id }}">{{ $a->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione el Proyecto</label>
                    <div class="col-sm-9" id="proyectos-list-container">
                        <select name="proyecto_id" id="proyecto_id">
                        @foreach ($proyectos as $p)
                                <option value="{{ $p->id }}">{{ $p->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre Corto (sin espacios)</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Nombre Completo</label>
                     <div class="col-sm-9">
                        <input type="description" name="description" class="form-control" value="">
                     </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Vigencia</label>
                    <div class="col-sm-3">
                        <select name="vigencia">
                        <?php
                            for($i = 2015; $i < date('Y') + 1; $i++ ){
                        ?>
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php  
                            }
                        ?>
                        </select>
                    </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            @endif 
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection