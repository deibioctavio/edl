@extends('app')
 
@section('content')
@include('partials.metas.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Meta</h2>
            <h4>Listado de Metas de Proyectos Registrados</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre del Área</th>
                            <th>Nombre Proyecto</th>
                            <th>Nombre Meta</th>
                            <th colspan="1">Vigencia</th>
                            <th colspan="2">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if (count($areaProyectoMeta) < 1){
                        ?>
                            <tr>
                                <td colspan="7">No existen metas registrados</td>
                            </tr>    
                        <?php
                            }
                            
                            else{
                            
                                $i = 0;

                                foreach ($areaProyectoMeta as $a){
                                    $i++;
?>
                                            <tr>
                                                <td><?php echo $i?></td>
                                                <td><?php echo $a['area_name']?></td>
                                                <td><?php echo $a['proyecto_name']?></td>
                                                <td><?php echo $a['meta_name']?></td>
                                                <td><?php echo $a['meta_vigencia']?></td>
                                                <td>
                                                    <a 
                                                        href="<?php echo url('metaedit',[$a['meta_id']])?>"
                                                    >Actualizar
                                                    </a>
                                                </td>
                                                <td>
                                                    <a 
                                                        id="meta-del" 
                                                        number="<?php echo $a['meta_id']?>" 
                                                        href="<?php echo url('metadelete',[$a['meta_id']])?>"
                                                    >
                                                        Eliminar
                                                    </a>
                                                </td>
                                            </tr>
                            <?php
                                            
                                }
                            }
                            ?>
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection