@extends('app')
 
@section('content')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Area/Dependencia</h2>
            <h4>Listado de Áreas/Dependencias Registradas</h4>    
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre Corto</th>
                            <th>Nombre Completo (Descripción)</th>
                            <th>Vigencia</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($areas) < 1)
                            <tr>
                                <td colspan="5">No existen áreas/dependencias registradas</td>
                            </tr>
                        @elseif (count($areas) >= 1)
                            @foreach ($areas as $a)
                                <tr>
                                    <td>{{ $a->id }}</td>
                                    <td>{{ $a->name }}</td>
                                    <td>{{ $a->description }}</td>
                                    <td>{{ $a->vigencia }}</td>
                                    </td>
                                </tr>
                            @endforeach
                        @endif 
                    </tbody>
                </table>
            </div>


            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection