@extends('app')
 
@section('content')
@include('partials.areas.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Macroproceso</h2>
            <h4>Listado de Macroproceso Registradas</h4>    
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Vigencia</th>
                            <th colspan="2">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($macroproceso) < 1)
                            <tr>
                                <td colspan="5">No existen Macroprocesos registrados</td>
                            </tr>
                        @elseif (count($macroproceso) >= 1)
                            @foreach ($macroproceso as $m)
                                <tr>
                                    <td>{{ $m->id }}</td>
                                    <td>{{ $m->name }}</td>
                                    <td>{{ $m->description }}</td>
                                    <td>{{ $m->vigencia }}</td>
                                    <td><a href="<?php echo url('macroprocesoedit',[$m->id])?>">Actualizar</a></td>
                                    <td><a id="area-del" number="<?php echo $m['id']?>"  href="<?php echo url('macroprocesodelete',[$m->id])?>">Eliminar</a></td>
                                    </td>
                                </tr>
                            @endforeach
                        @endif 
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection