@extends('app')
 
@section('content')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Macroproceso</h2>
            <h4>Diligencie la información del Macroproceso</h4>    
                            
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('macroprocesoedit', [])}}" method="POST">
            <input type="hidden" value="{{ $macroproceso->id }}" name="id" class="form-control">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre</label>
                    <div class="col-sm-9">
                        <input type="text" value="{{$macroproceso->name}}" name="name" class="form-control">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Descripción</label>
                     <div class="col-sm-9">
                        <input type="description" value="{{$macroproceso->description}}" name="description" class="form-control">
                     </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Activo</label>
                    <div class="col-sm-3">
                        <select name="active">
                            <option value="1" selected>Activo</option>
                            <option value="2">Inactivo</option>
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Vigencia</label>
                    <div class="col-sm-3">
                        <select name="vigencia">
                        <?php
                            for($i = 2015; $i < date('Y') + 1; $i++ ){
                        ?>
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php  
                            }
                        ?>
                        </select>
                    </div>
                </div>
                <div class="form-group right hidden">
                    <label class="col-sm-3">Color</label>
                     <div class="col-sm-9">
                        <input type="description" value="{{$macroproceso->color}}"name="color" class="form-control">
                     </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection