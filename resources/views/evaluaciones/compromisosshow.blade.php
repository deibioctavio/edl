@extends('app')

@section('content')
 <div class="container">
    <style type="text/css">
        .center-cell
        {
            vertical-align: middle;
            display: table-cell;
        };
    </style>
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Formato de Compromisos</h2>
            <h4>Verifique la información de los Compromisos</h4>
            <form
                    id="register-frm" class="form-horizontal" accept-charset="UTF-8"
                    action="{{route('evaluacionaddcompromiso', [])}}" method="POST"
            >
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="evaluacion_id" value="{{ $evaluacion->id }}" />
                <div class="form-group right">
                    <label class="col-sm-2">Tipo de Evaluación</label>
                    <div class="col-sm-3">{{ $evaluacion->tipo_evaluacion }}</div>
                    <label class="col-sm-1">Vigencia</label>
                    <div class="col-sm-1">{{ $evaluacion->vigencia }}</div>
                    <label class="col-sm-1">Días Evaluados</label>
                    <div class="col-sm-1">{{ $evaluacion->dias_evaluados }}</div>
                    <label class="col-sm-1">Porcentaje Evaluado</label>{{ $evaluacion->porcentaje_evaluado }}
                </div>
                <div class="form-group right">
                    <div class="input-group">
                        <span class="input-group-addon">PERIODO EVALUACIÓN</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->periodo_evaluado_inicio }}"
                        >
                        <span class="input-group-addon">Al</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->periodo_evaluado_fin }}"
                        >
                        <span class="input-group-addon">FECHA FIJACIÓN DE COMPROMISOS</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->fecha_fijacion_compromiso }}"
                        >
                    </div>
                </div>
                <div class="form-group right">
                   <h4>INTERVINIENTES</h4>
                </div>
                <div class="form-group right">
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADO</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->document_number }}"
                        >
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->nombre }} {{ $evaluacion->apellido }}"
                        >
                    </div>
                </div>
                <div class="form-group right">
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADOR (Jefe Inmediato)</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e1document_number }}"
                        >
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e1nombre }} {{ $evaluacion->e1apellido }}"
                        >
                    </div>
                </div>

                <div class="form-group right">
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADOR (Funcionario LNR en Caso de conformar comisión)</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e2document_number }}"
                        >
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e2nombre }} {{ $evaluacion->e2apellido }}"
                        >
                    </div>
                </div>

                <div class="form-group right">
                    <label class="col-sm-3">PROPOSITO DEL EMPLEO</label>
                    <div class="col-sm-9">
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->proposito }}"
                        >
                    </div>
                </div>
                <div class="form-group right">&nbsp;</div>
                <div class="form-group right">
                   <h4>COMPROMISOS LABORALES</h4>
                </div>

                <!--
                <pre>
                    <?php //print_r($compromisosLaborales)?>
                </pre>

                <pre>
                    <?php //print_r(count($evidenciacompromisosLaborales[9]))?>
                </pre>-->

                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th colspan="2"><center>Metas Institucionales o de la dependencia</center></th>
                                <th colspan="2"><center>Comprimisos Laborales con sus condiciones de resultado</center></th>
                                <th rowspan="1"><center>% Cumplimiento pactado</center></th>
                            </tr>
                            <tr>
                                <td colspan="2">Proyecto / Meta</td>
                                <td colspan="1">Compromiso</td>
                                <td>Condiciones de Resultado</td>
                                <td>&nbsp</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0; $totalPactado = 0;?>
                        @foreach ($compromisosLaborales as $cL)
                            <tr>
                                <td colspan="2"><b>Proyecto:</b> {{ $cL->proyecto_name }}
                                    <div id="container-meta-proyecto-{{ $i }}">
                                    <b>Meta:</b> {{ $cL->meta_name }}
                                    </div>
                                    <div id="container-meta-proyecto-descripcion-{{ $i }}">
                                    <b>Meta Descripción:</b> {{ $cL->meta_description }}
                                    </div>
                                </td>
                                <td colspan="1">{{ $cL->compromiso_name }}</td>
                                <td>{{ $cL->condicion_resultado }}</td>
                                <td>{{ $cL->procentaje_cumpimiento_pactado }}%</td>
                            </tr>
                            <tr>
                                <td colspan="4"><center><b>Evidencias</b></center></td>
                                <td colspan="1">
                                <?php if( count( $evidenciacompromisosLaborales[$cL->id] ) == 0 ){ ?>
                                    <a
                                        href="<?php echo url('evaluaciones/compromisos/evidenciadefine',[$cL->id,$evaluacion->id])?>"
                                        id="evidencia-add"
                                        evidencia="<?php echo $cL->id;?>"
                                    >
                                       Definir Evidencias
                                    </a>
                                <?php } else echo " -- ";?>
                                </td>
                            </tr>
                            <?php if( count( $evidenciacompromisosLaborales[$cL->id] ) >0 ){ ?>
                                <tr>
                                    <td colspan="2"><center>Descripción</center></td>
                                    <td><center>Peso porcentual</center></td>
                                    <td><center>Fecha Registro Evidencia</center></td>
                                    <td><center>Fecha Verificación</center></td>
                                </tr>
                                @foreach( $evidenciacompromisosLaborales[$cL->id] as $eCl)
                                    <tr>
                                        <td colspan="2">
                                            {{ $eCl->description }}
                                        </td>
                                        <td>
                                            {{ $eCl->percentual_weight }}
                                        </td>
                                        <td>
                                            {{ $eCl->created_at }}
                                        </td>
                                        <td>
                                            {{ $eCl->verification_date == NULL?"Sin verificación":$eCl->verification_date }}
                                        </td>
                                    </tr>
                                @endforeach
                            <?php } ?>
                            <tr><td colspan="5">&nbsp;</td></tr>
                            <?php $i++; $totalPactado += $cL->procentaje_cumpimiento_pactado;?>
                        @endforeach
                            <tr>
                                <td colspan="5">
                                    Porcentaje Total pactado <b>{{ $totalPactado }}%</b>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
