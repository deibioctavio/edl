@extends('app')
 
@section('content')
@include('partials.evaluaciones.evidenciadefine')
 <div class="container">
    <style type="text/css">
        .center-cell
        {
            vertical-align: middle;
            display: table-cell;
        };
    </style>
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Formato de Compromisos</h2>
            <h4>Verifique la información de los Compromisos</h4>
            <form 
                    id="register-frm" class="form-horizontal" accept-charset="UTF-8" 
                    action="{{route('evaluacionescompromisosevidenciadefinesave', [])}}" method="POST"
            >
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="evaluacion_id" value="{{ $evaluacion->id }}" />
                <div class="form-group right"> 
                    <label class="col-sm-2">Tipo de Evaluación</label>
                    <div class="col-sm-3">{{ $evaluacion->tipo_evaluacion }}</div>
                    <label class="col-sm-1">Vigencia</label>
                    <div class="col-sm-1">{{ $evaluacion->vigencia }}</div>
                    <label class="col-sm-1">Días Evaluados</label>
                    <div class="col-sm-1">{{ $evaluacion->dias_evaluados }}</div>
                    <label class="col-sm-1">Porcentaje Evaluado</label>{{ $evaluacion->porcentaje_evaluado }}
                </div>
                <div class="form-group right">
                    <div class="input-group">
                        <span class="input-group-addon">PERIODO EVALUACIÓN</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->periodo_evaluado_inicio }}" 
                        >
                        <span class="input-group-addon">Al</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->periodo_evaluado_fin }}" 
                        >
                        <span class="input-group-addon">FECHA FIJACIÓN DE COMPROMISOS</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->fecha_fijacion_compromiso }}" 
                        >
                    </div>
                </div>
                <div class="form-group right">
                   <h4>INTERVINIENTES</h4>
                </div>
                <div class="form-group right">    
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADO</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->document_number }}" 
                        >
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->nombre }} {{ $evaluacion->apellido }}" 
                        >
                    </div>
                </div>
                <div class="form-group right">    
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADOR (Jefe Inmediato)</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e1document_number }}" 
                        >
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e1nombre }} {{ $evaluacion->e1apellido }}" 
                        >
                    </div>
                </div>

                <div class="form-group right">    
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADOR (Funcionario LNR en Caso de conformar comisión)</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e2document_number }}" 
                        >
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e2nombre }} {{ $evaluacion->e2apellido }}" 
                        >
                    </div>
                </div>
                
                <div class="form-group right"> 
                    <label class="col-sm-3">PROPOSITO DEL EMPLEO</label>
                    <div class="col-sm-9">
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->proposito }}" 
                        >
                    </div>
                </div>
                <div class="form-group right">&nbsp;</div>
                <div class="form-group right">
                   <h4>COMPROMISO LABORAL</h4>
                </div>
                
                <!--
                <pre>
                    <?php print_r($compromisosLaborales)?>
                </pre>
                -->

                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th colspan="2"><center>Meta Institucional o de la dependencia</center></th>
                                <th colspan="2"><center>Comprimiso Laboral con sus condición de resultado</center></th>
                                <th rowspan="1"><center>% Cumplimiento pactado</center></th>
                            </tr>
                            <tr>
                                <td colspan="2">Proyecto / Meta</td>
                                <td colspan="1">Compromiso</td>
                                <td>Condiciones de Resultado</td>
                                <td>&nbsp</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0;?>
                            @foreach ($compromisosLaborales as $cL)
                                <?php $totalPactado = $cL->procentaje_cumpimiento_pactado;?>
                            <input type="hidden" name="compromiso_id" value="{{ $cL->id }}" />
                                <tr>
                                    <td colspan="2"><b>Proyecto:</b> {{ $cL->proyecto_name }}
                                        <div id="container-meta-proyecto-{{ $i }}">
                                        <b>Meta:</b> {{ $cL->meta_name }}
                                        </div>
                                        <div id="container-meta-proyecto-descripcion-{{ $i }}">
                                        <b>Meta Descripción:</b> {{ $cL->meta_description }}
                                        </div>
                                    </td>
                                    <td colspan="1">{{ $cL->compromiso_name }}</td>
                                    <td>{{ $cL->condicion_resultado }}</td>
                                    <td>{{ $cL->procentaje_cumpimiento_pactado }}%</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    <div class="form-group right">&nbsp;</div>
                    <div class="form-group right">
                        <h4>EVIDENCIAS</h4>
                    </div>
                    
                    <table class="table table-hover table-bordered" id="tabla-evidencias">
                        <thead>
                            <tr>
                                <th colspan="2"><center>Descripción</center></th>
                                <th colspan="1"><center>Peso</center></th>
                                <th colspan="2"><center>Observaciones</center></th>
                                <th colspan="1"><center>Acción</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="row-evidencia-0">
                                <td colspan="2">
                                    <center><input type="text" name="description[]" id="description_0" size="30" /></center>
                                </td>
                                <td>
                                    <center><select id="percentual_weight_0" name="percentual_weight[]">
                                        <?php

                                            for($i = 1; $i <= $totalPactado; $i++){
                                        ?>
                                        <option value="{{ $i }}">{{ $i }}</option>
                                        <?php

                                            }

                                        ?>
                                    </select></center>
                                </td>
                                <td colspan="2">
                                    <center><textarea id="comments_0" name="comments[]" rows="2" cols="40"></textarea></center>
                                </td>
                                <td>
                                    <center>
                                        <a href="#" id="evidencia-define-0">
                                            <b>Adicionar</b>
                                        </a>
                                    </center>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-hover table-bordered">
                        <tbody>
                            <tr>
                                <td>
                                    Porcentaje Total pactado:&nbsp;<b><?php echo (int)$totalPactado;?>%</b>
                                    <input 
                                        type="hidden"
                                        readonly
                                        id="total_pactado"
                                        name="total_pactado"
                                        value="<?php echo (int)$totalPactado;?>" 

                                    />
                                </td>
                                <td>
                                    Porcentaje Total registrado:&nbsp;<input 
                                                                        type="text"
                                                                        readonly
                                                                        id="total_registrado"
                                                                        name="total_registrado"
                                                                        value="0" 
                                                                        size="3"
                                                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection