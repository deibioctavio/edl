@extends('app')
 
@section('content')
@include('partials.evaluaciones.compromisosadd')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Formato de Compromisos</h2>
            <h4>Diligencie la información de los Compromisos</h4>
            <form 
                    id="register-frm" class="form-horizontal" accept-charset="UTF-8" 
                    action="{{route('evaluacionaddcompromiso', [])}}" method="POST"
            >
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="evaluacion_id" value="{{ $evaluacion->id }}" />
                <div class="form-group right"> 
                    <label class="col-sm-2">Tipo de Evaluación</label>
                    <div class="col-sm-3">{{ $evaluacion->tipo_evaluacion }}</div>
                    <label class="col-sm-1">Vigencia</label>
                    <div class="col-sm-1">{{ $evaluacion->vigencia }}</div>
                    <label class="col-sm-1">Días Evaluados</label>
                    <div class="col-sm-1">{{ $evaluacion->dias_evaluados }}</div>
                    <label class="col-sm-1">Porcentaje Evaluado</label>{{ $evaluacion->porcentaje_evaluado }}
                </div>
                <div class="form-group right">
                    <div class="input-group">
                        <span class="input-group-addon">PERIODO EVALUACIÓN</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->periodo_evaluado_inicio }}" 
                        >
                        <span class="input-group-addon">Al</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->periodo_evaluado_fin }}" 
                        >
                        <span class="input-group-addon">FECHA FIJACIÓN DE COMPROMISOS</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->fecha_fijacion_compromiso }}" 
                        >
                    </div>
                </div>
                <div class="form-group right">
                   <h4>INTERVINIENTES</h4>
                </div>
                <div class="form-group right">    
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADO</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->document_number }}" 
                        >
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->nombre }} {{ $evaluacion->apellido }}" 
                        >
                    </div>
                </div>
                <div class="form-group right">    
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADOR (Jefe Inmediato)</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e1document_number }}" 
                        >
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e1nombre }} {{ $evaluacion->e1apellido }}" 
                        >
                    </div>
                </div>

                <div class="form-group right">    
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADOR (Funcionario LNR en Caso de conformar comisión)</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e2document_number }}" 
                        >
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e2nombre }} {{ $evaluacion->e2apellido }}" 
                        >
                    </div>
                </div>
                
                <div class="form-group right"> 
                    <label class="col-sm-3">PROPOSITO DEL EMPLEO</label>
                    <div class="col-sm-9">
                        <input 
                                type="text" 
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->proposito }}" 
                        >
                    </div>
                </div>
                <div class="form-group right">&nbsp;</div>
                <div class="form-group right">
                   <h4>COMPROMISOS LABORALES</h4>
                </div>

                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th colspan="2"><center>Metas Institucionales o de la dependencia</center></th>
                                <th colspan="2"><center>Comprimisos Laborales con sus condiciones de resultado</center></th>
                                <th rowspan="1"><center>% Cumplimiento pactado</center></th>
                            </tr>
                            <tr>
                                <td colspan="2">Proyecto / Meta</td>
                                <td colspan="1">Compromiso</td>
                                <td>Condiciones de Resultado</td>
                                <td>% pactado</td>
                            </tr>
                        </thead>
                        <tbody>

                        @for ($i = 0; $i < $evaluacion->n_compromisos_laborales; $i++)
                            <tr>
                                <td colspan="2">
                                    <select name="proyecto[{{ $i }}]" id="proyecto" number="{{ $i }}">
                                        @foreach ($proyectos as $p)
                                            <option value="{{ $p->id }}">{{ $p->name }}</option>
                                        @endforeach
                                    </select>
                                    <div id="container-meta-proyecto-{{ $i }}">
                                    Meta
                                    </div>
                                    <div id="container-meta-proyecto-descripcion-{{ $i }}">
                                    Meta Descripción
                                    </div>
                                </td>
                                <td colspan="1">
                                    <select name="compromiso[{{ $i }}]" id="compromiso-{{ $i }}">
                                        @foreach ($compromisos as $c)
                                            <option value="{{ $c->id }}">{{ $c->description }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><textarea 
                                                    name="condicion_resultado[{{ $i }}]"
                                                    id="condicion_resultado_{{ $i }}"
                                                    cols="20" 
                                                    rows="5"  
                                                ></textarea>
                                </td>
                                <td>
                                    Seleccione %
                                    <select 
                                            name="procentaje_cumpimiento_pactado[{{ $i }}]" 
                                            id="procentaje_cumpimiento_pactado-{{ $i }}"
                                    >
                                        @for($j = 1; $j<= 100; $j++)
                                            <option value="{{ $j }}">{{ $j }}</option>
                                        @endfor
                                    </select>
                                </td>
                            </tr>
                        @endfor
                            <tr>
                                <td colspan="5">
                                    Porcentaje Total pactado
                                        <input 
                                            type="text" 
                                            readonly="" 
                                            id="total_pactado" 
                                            name="total_pactado"  
                                        />&nbsp;
                                        <input 
                                            type="text" 
                                            readonly="" 
                                            id="total" 
                                            name="total"
                                            class="hidden"
                                            value="100" 
                                        />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection