@extends('app')
 
@section('content')
@include('partials.evaluaciones.add')
@include('partials.datepicker')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Formato de Evaluación</h2>
            <h4>Diligencie la información de la Evaluación</h4>
            <form 
                    id="register-frm" class="form-horizontal" accept-charset="UTF-8" 
                    action="{{route('evaluacionadd', [])}}" method="POST"
            >
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group right"> 
                    <label class="col-sm-2">Tipo de Evaluación</label>
                    <div class="col-sm-3">
                        <select name="tipo_evaluacion_id" id="tipo_evaluacion_id">
                            @foreach ($tiposEvaluaciones as $t)
                                <option value="{{ $t->id }}">{{ $t->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <label class="col-sm-1">Vigencia</label>
                    <div class="col-sm-1">
                        @if( count($vigencias) > 0 )
                            <select name="vigencia" id="vigencia">
                                @foreach($vigencias as $v)
                                    <option value="{{$v->vigencia}}">{{$v->vigencia}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                    <label class="col-sm-1">Días Evaluados</label>
                    <div class="col-sm-1">
                        <select name="dias_evaluados" id="dias_evaluados">
                            @for($i = 1; $i <= 360; $i++ )
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <label class="col-sm-1">Porcentaje Evaluado</label>
                    <div class="col-sm-1">
                        <select name="porcentaje_evaluado" id="porcentaje_evaluado">
                            @for($j = 1; $j <= 100; $j++ )
                                <option value="{{$j}}">{{$j}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="form-group right">
                    <div class="input-group">
                        <span class="input-group-addon">PERIODO EVALUACIÓN</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Elija fecha" 
                                aria-describedby="sizing-addon1"
                                name="periodo_evaluado_inicio" 
                                id="periodo_evaluado_inicio"
                                readonly
                        >
                        <span class="input-group-addon">Al</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Elija fecha" 
                                aria-describedby="sizing-addon1"
                                name="periodo_evaluado_fin" 
                                id="periodo_evaluado_fin"
                                readonly
                        >
                        <span class="input-group-addon">FECHA FIJACIÓN DE COMPROMISOS</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Elija fecha" 
                                aria-describedby="sizing-addon1"
                                name="fecha_fijacion_compromiso" 
                                id="fecha_fijacion_compromiso"
                                readonly    
                        >
                    </div>
                </div>
                <div class="form-group right">
                   <h4>INTERVINIENTES</h4>
                </div>
                <div class="form-group right">    
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADO</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Ingrese número de identificación" 
                                aria-describedby="sizing-addon1"
                                name="documento" 
                                id="documento"
                                item="evaluado"
                        >
                        <input type="hidden" name="usuario_evaluado_id" id="usuario_evaluado_id">
                        <input type="hidden" name="usuario_evaluado_cargo_id" id="usuario_evaluado_cargo_id">
                        <input type="hidden" name="usuario_evaluado_area_id" id="usuario_evaluado_area_id">
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Nombres y Apellidos" 
                                aria-describedby="sizing-addon1"
                                name="nombre_apellido" 
                                id="nombre_apellido"
                                readonly
                                item="evaluado"
                        >
                    </div> 
                    <div class="input-group">
                        <span class="input-group-addon">Nivel Jerárquico</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Nivel Jerárquico y Denominación del empleo" 
                                aria-describedby="sizing-addon1"
                                name="nivel_jerarquico" 
                                id="nivel_jerarquico"
                                readonly 
                                item="evaluado"
                        >
                        <span class="input-group-addon">Dependencia/Área</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Dependencia o Área funcional" 
                                aria-describedby="sizing-addon1"
                                name="area" 
                                id="area"
                                readonly 
                                item="evaluado"
                        >
                    </div>
                </div>

                <div class="form-group right"> 
                    <label class="col-sm-12">&nbsp;</label>
                </div>

                <div class="form-group right">    
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADOR (Jefe Inmediato)</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Ingrese número de identificación" 
                                aria-describedby="sizing-addon1"
                                name="documento1" 
                                id="documentoe1"
                                item="evaluador1"
                        >

                        <input type="hidden" name="usuario_evaluador1_id" id="usuario_evaluador1_id">
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Nombres y Apellidos" 
                                aria-describedby="sizing-addon1"
                                name="nombre_apellido" 
                                id="nombre_apellido"
                                readonly
                                item="evaluador1"
                        >
                    </div> 
                    <div class="input-group">
                        <span class="input-group-addon">Nivel Jerárquico</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Nivel Jerárquico y Denominación del empleo" 
                                aria-describedby="sizing-addon1"
                                name="nivel_jerarquico" 
                                id="nivel_jerarquico"
                                readonly 
                                item="evaluador1"
                        >
                        <span class="input-group-addon">Dependencia/Área</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Dependencia o Área funcional" 
                                aria-describedby="sizing-addon1"
                                name="area" 
                                id="area"
                                readonly 
                                item="evaluador1"
                        >
                    </div>
                </div>

                <div class="form-group right"> 
                    <label class="col-sm-12">&nbsp;</label>
                </div>

                <div class="form-group right">    
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADOR (Funcionario LNR en Caso de conformar comisión)</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Ingrese número de identificación" 
                                aria-describedby="sizing-addon1"
                                name="documentoe2" 
                                id="documentoe2"
                                item="evaluador2"
                        >

                        <input type="hidden" name="usuario_evaluador2_id" id="usuario_evaluador2_id">
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Nombres y Apellidos" 
                                aria-describedby="sizing-addon1"
                                name="nombre_apellido" 
                                id="nombre_apellido"
                                readonly
                                item="evaluador2"
                        >
                    </div> 
                    <div class="input-group">
                        <span class="input-group-addon">Nivel Jerárquico</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Nivel Jerárquico y Denominación del empleo" 
                                aria-describedby="sizing-addon1"
                                name="nivel_jerarquico" 
                                id="nivel_jerarquico"
                                readonly 
                                item="evaluador2"
                        >
                        <span class="input-group-addon">Dependencia/Área</span>
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Dependencia o Área funcional" 
                                aria-describedby="sizing-addon1"
                                name="area" 
                                id="area"
                                readonly 
                                item="evaluador2"
                        >
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-12">&nbsp;</label>
                </div>
                
                <div class="form-group right"> 
                    <label class="col-sm-3">PROPOSITO DEL EMPLEO</label>
                    <div class="col-sm-9">
                        <input 
                                type="text" 
                                class="form-control" 
                                placeholder="Indique el proposito del empleo" 
                                aria-describedby="sizing-addon1"
                                name="proposito" 
                                id="proposito" 
                        >
                    </div>
                </div>

                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection