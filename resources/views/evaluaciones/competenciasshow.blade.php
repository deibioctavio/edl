@extends('app')

@section('content')
 <div class="container">
    <style type="text/css">
        .center-cell
        {
            vertical-align: middle;
            display: table-cell;
        };
    </style>
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Formato de Compromisos</h2>
            <h4>Verifique la información de los Compromisos</h4>
            <form
                    id="register-frm" class="form-horizontal" accept-charset="UTF-8"
                    action="{{route('evaluacionaddcompromiso', [])}}" method="POST"
            >
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="evaluacion_id" value="{{ $evaluacion->id }}" />
                <div class="form-group right">
                    <label class="col-sm-2">Tipo de Evaluación</label>
                    <div class="col-sm-3">{{ $evaluacion->tipo_evaluacion }}</div>
                    <label class="col-sm-1">Vigencia</label>
                    <div class="col-sm-1">{{ $evaluacion->vigencia }}</div>
                    <label class="col-sm-1">Días Evaluados</label>
                    <div class="col-sm-1">{{ $evaluacion->dias_evaluados }}</div>
                    <label class="col-sm-1">Porcentaje Evaluado</label>{{ $evaluacion->porcentaje_evaluado }}
                </div>
                <div class="form-group right">
                    <div class="input-group">
                        <span class="input-group-addon">PERIODO EVALUACIÓN</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->periodo_evaluado_inicio }}"
                        >
                        <span class="input-group-addon">Al</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->periodo_evaluado_fin }}"
                        >
                        <span class="input-group-addon">FECHA FIJACIÓN DE COMPROMISOS</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->fecha_fijacion_compromiso }}"
                        >
                    </div>
                </div>
                <div class="form-group right">
                   <h4>INTERVINIENTES</h4>
                </div>
                <div class="form-group right">
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADO</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->document_number }}"
                        >
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->nombre }} {{ $evaluacion->apellido }}"
                        >
                    </div>
                </div>
                <div class="form-group right">
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADOR (Jefe Inmediato)</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e1document_number }}"
                        >
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e1nombre }} {{ $evaluacion->e1apellido }}"
                        >
                    </div>
                </div>

                <div class="form-group right">
                    <div class="input-group">
                        <span class="input-group-addon">EVALUADOR (Funcionario LNR en Caso de conformar comisión)</span>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">Documento de Identidad</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e2document_number }}"
                        >
                        <span class="input-group-addon">Nombres y Apellidos</span>
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->e2nombre }} {{ $evaluacion->e2apellido }}"
                        >
                    </div>
                </div>

                <div class="form-group right">
                    <label class="col-sm-3">PROPOSITO DEL EMPLEO</label>
                    <div class="col-sm-9">
                        <input
                                type="text"
                                class="form-control"
                                aria-describedby="sizing-addon1"
                                readonly
                                value="{{ $evaluacion->proposito }}"
                        >
                    </div>
                </div>
                <div class="form-group right">&nbsp;</div>
                <div class="form-group right">
                   <h4>COMPETENCIAS COMPORTAMENTALES</h4>
                </div>

                <!--
                <pre>
                    <?php print_r($competenciasComportamentales)?>
                </pre>-->

                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th colspan="1"><center>Competencia</center></th>
                                <th colspan="1"><center>Conducta Asociada</center></th>
                                <th rowspan="1"><center>Puntaje Máximo Esperado</center></th>
                                <th rowspan="1"><center>Seleccionar</center></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0; $totalPactado = 0;?>
                        <?php
                            if(count($competenciasComportamentales)<=0){
                        ?>
                            <tr><td colspan="5"><center>No existen Competencias Comportamentales Registradas</center></td></tr>
                        <?php
                            }else{
                        ?>
                            @foreach ($competenciasComportamentales as $cL)
                                <tr>
                                    <td colspan="1">{{ $cL->competencia_name }}</td>
                                    <td colspan="1">{{ $cL->conducta_name }}</td>
                                    <td colspan="1">{{ $cL->procentaje_cumpimiento_pactado }}%</td>
                                    <td colspan="1">
                                      <input type="checkbox" name="conducta[{{$cL->conducta_id}}]" />
                                    </td>
                                </tr>
                            @endforeach
                        <?php
                            }
                        ?>
                        </tbody>
                    </table>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
