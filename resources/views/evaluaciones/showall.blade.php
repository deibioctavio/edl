@extends('app')

@section('content')
@include('partials.evaluaciones.list')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Evaluaciones</h2>
            <h4>Listado de Evaluaciones Donde El Usuario <span class="bold"><?php echo Auth::user()->name;?></span> es Evaluador</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th><center>Tipo de Evaluación</center></th>
                            <th><center>Vigencia</center></th>
                            <th><center>Días Evaluados</center></th>
                            <th><center>Porcentaje Evaluados</center></th>
                            <th><center>Funcionario Evaluado</center></th>
                            <th><center>Periodo Evaluado</center></th>
                            <th colspan="2"><center>Acción</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (count($evaluaciones) < 1){
                        ?>
                            <tr>
                                <td colspan="7">No existen evaluaciones registradas</td>
                            </tr>
                        <?php
                            }

                            else{

                                $i = 0;

                                foreach ($evaluaciones as $e){
                                    $i++;
?>
                                            <tr>
                                                <!--<td><?php echo $e->id?></td>-->
                                                <td><?php echo $e->tipo_evaluacion?></td>
                                                <td><?php echo $e->vigencia?></td>
                                                <td><?php echo $e->dias_evaluados?></td>
                                                <td><?php echo $e->porcentaje_evaluado?></td>
                                                <td><?php echo $e->nombre." ".$e->apellido?></td>
                                                <td><?php
                                                        echo $e->periodo_evaluado_inicio.
                                                            " al ".
                                                            $e->periodo_evaluado_fin
                                                    ?>
                                                </td>
                                                <td>
                                                    <img
                                                        width="24"
                                                        height="24"
                                                        src="{{ asset('assets/images/loading.gif') }}"
                                                        id="evaluacion-<?php echo $e->id;?>"
                                                    />
                                                    <a
                                                        href="<?php echo url('evaluacionaddcompromiso',[$e->id])?>"
                                                        id="evaluacion-add"
                                                        evaluacion="<?php echo $e->id;?>"
                                                        class="hidden"
                                                    >
                                                        Definir&nbsp;Compromisos
                                                    </a>
                                                    <a
                                                        href="<?php echo url('evaluaciones/showcompromisolaboralesbyevaluacion',[$e->id])?>"
                                                        id="evaluacion-show"
                                                        evaluacion="<?php echo $e->id;?>"
                                                        class="hidden"
                                                    >
                                                        Ver&nbsp;Compromisos
                                                    </a>
                                                </td>
                                                <td>
                                                    <a
                                                        href="<?php echo url('evaluaciones/showcompromisocomportamentalesbyevaluacion',[$e->id])?>"
                                                        evaluacion="<?php echo $e->id;?>"
                                                    >
                                                        Definir&nbsp;Competencias
                                                    </a>
                                                </td>
                                            </tr>
                            <?php

                                }
                            }
                            ?>
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
