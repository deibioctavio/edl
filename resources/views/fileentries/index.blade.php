@extends('app')

@section('content')
    <script type="text/javascript">
    $(document).ready(function(){
        
        /*Add new catagory Event*/
        $("#ajax-submit").click(function(){
            $.ajax({
                      url:'fileentry/addajax',
                      data:new FormData($("#upload_form")[0]),
                      dataType:'json',
                      async:false,
                      type:'post',
                      processData: false,
                      contentType: false,
                      success:function(response){
                        console.log(response);
                      },
                });
         });
        /*Add new catagory Event*/
    });
    </script>
    <form action="{{route('addentryajax', [])}}" method="post" enctype="multipart/form-data" id="upload_form">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <input type="file" name="filefield">
        <input type="button" id="ajax-submit" value="Send Ajax">
    </form>
 
 <h1> Pictures list</h1>
 <div class="row">
        <ul class="thumbnails">
             @foreach($files as $entry)
                <div class="col-md-2">
                    <div class="thumbnail">
                        <img src="{{route('getentry', $entry->filename)}}" alt="ALT NAME" class="img-responsive" />
                        <div class="caption">
                            <p>{{$entry->original_filename}}</p>
                        </div>
                    </div>
                </div>
             @endforeach
        </ul>
 </div>

@endsection