@extends('app')
 
@section('content')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Actividad</h2>
            <h4>Diligencie la información de la Actividad</h4>    
                            
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('actividadedit', [])}}" method="POST">
            <input type="hidden" value="{{ $actividad->id }}" name="id" class="form-control">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre</label>
                    <div class="col-sm-9">
                        <input type="text" value="{{$actividad->name}}" name="name" class="form-control">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Descripción</label>
                     <div class="col-sm-9">
                        <input type="description" value="{{$actividad->description}}" name="description" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Estado</label>
                     <div class="col-sm-9">
                        <select name="active">
                            <option value="0" <?php echo ($actividad->active == 0)?"selected":""?>>inactivo</option>
                            <option value="1" <?php echo ($actividad->active == 1)?"selected":""?>>activo</option>
                        </select>
                     </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Vigencia</label>
                    <div class="col-sm-3">
                        <select name="vigencia">
                        <?php
                            for($i = 2015; $i < date('Y') + 1; $i++ ){
                        ?>
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php  
                            }
                        ?>
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione el Proceso</label>
                    <div class="col-sm-9">
                        <select name="proceso_id" id="proceso_id">
                        @foreach ($proceso as $p)
                            @if( $p->id === $actividad->proceso_id)
                                <option value="{{ $p->id }}" selected>{{ $p->name }}</option>
                            @else
                                <option value="{{ $p->id }}">{{ $p->name }}</option>
                            @endif 
                        @endforeach
                        </select>
                    </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection