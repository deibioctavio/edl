@extends('app')
 
@section('content')
@include('partials.areas.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Actividades</h2>
            <h4>Listado de Actividades Registradas</h4>    
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Vigencia</th>
                            <th>Proceso</th>
                            <th colspan="2">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($actividad) < 1)
                            <tr>
                                <td colspan="5">No existen Actividades registradas</td>
                            </tr>
                        @elseif (count($actividad) >= 1)
                            @foreach ($actividad as $a)
                                <tr>
                                    <td>{{ $a->id }}</td>
                                    <td>{{ $a->name }}</td>
                                    <td>{{ $a->description }}</td>
                                    <td>{{ $a->vigencia }}</td>
                                     @foreach ($proceso as $pro)
                                        @if($a->proceso_id == $pro->id)
                                        <td>{{ $pro->name }}</td>
                                        @endif
                                    @endforeach
                                    <td><a href="<?php echo url('actividadedit',[$a->id])?>">Actualizar</a></td>
                                    <td><a id="area-del" number="<?php echo $a['id']?>"  href="<?php echo url('actividaddelete',[$a->id])?>">Eliminar</a></td>
                                    </td>
                                </tr>
                            @endforeach
                        @endif 
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection