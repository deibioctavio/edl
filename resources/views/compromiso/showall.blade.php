@extends('app')
 
@section('content')
@include('partials.compromiso.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Compromiso</h2>
            <h4>Listado de los Compromisos de los Cargos Registrados</h4>    
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre del Nivel Jerarquico</th>
                            <th>Nombre Cargo</th>
                            <th>Descripcion del  Compromiso</th>
                            <th colspan="1">Vigencia</th>
                            <th colspan="2">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if (count($nivelCargoCompromiso) < 1){
                        ?>
                            <tr>
                                <td colspan="7">No existen compromisos registrados</td>
                            </tr>    
                        <?php
                            }
                            
                            else{
                            
                                $i = 0;

                                foreach ($nivelCargoCompromiso as $n){
                                    $i++;
?>
                                            <tr>
                                                <td><?php echo $i?></td>
                                                <td><?php echo $n['nivel_name']?></td>
                                                <td><?php echo $n['cargo_name']?></td>
                                                <td><?php echo $n['compromiso_description']?></td>
                                                <td><?php echo $n['compromiso_vigencia']?></td>
                                                <td>
                                                    <a 
                                                        href="<?php echo url('compromisoedit',[$n['compromiso_id']])?>"
                                                    >Actualizar
                                                    </a>
                                                </td>
                                                <td>
                                                    <a 
                                                        id="compromiso-del" 
                                                        number="<?php echo $n['compromiso_id']?>" 
                                                        href="<?php echo url('compromisodelete',[$n['compromiso_id']])?>"
                                                    >
                                                        Eliminar
                                                    </a>
                                                </td>
                                            </tr>
                            <?php
                                            
                                        }
                            }
                            ?>
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection