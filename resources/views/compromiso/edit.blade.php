@extends('app')
 
@section('content')
<script type="text/javascript">
    var cargarCargosDefault = false;
</script>
@include('partials.compromiso.cargos')
@include('partials.cargo.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Compromiso</h2>
            <h4>Diligencie la información del Compromiso</h4>
            @if (count($niveles) < 1 || count($cargos) < 1)
                <div class="form-group right"> 
                        <div class="col-sm-12">
                            No existen niveles ó cargos Registradas
                        </div>
                </div>
                <div>                            
                        <input id="regresar" route="compromisoshowall" type="button" value="Regresar" class="btn btn-rw btn-primary center-block">
                </div>
            @elseif (count($niveles) >= 1)
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('compromisoedit', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="id" class="form-control" value="{{ $compromiso->id }}">
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione el Nivel Jerarquico</label>
                    <div class="col-sm-9">
                        <select name="nivel_jerarquico_id" id="nivel_jerarquico_id">
                        @foreach ($niveles as $n)
                            @if( $n->id === $cargo->nivel_jerarquico_id)
                                <option value="{{ $n->id }}" selected>{{ $n->name }}</option>
                            @else
                                <option value="{{ $n->id }}">{{ $n->name }}</option>
                            @endif 
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione el Cargo</label>
                    <div class="col-sm-9" id="cargo-list-container">
                        <select name="cargo_id" id="cargo_id">
                        @foreach ($cargos as $c)
                            @if( $c->id === $cargo->id)
                                <option value="{{ $c->id }}" selected>{{ $c->name }}</option>
                            @else
                                <option value="{{ $c->id }}">{{ $c->name }}</option>
                            @endif 
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Nombre Completo</label>
                     <div class="col-sm-9">
                        <input type="description" name="description" class="form-control" value="{{ $compromiso->description }}">
                     </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Vigencia</label>
                    <div class="col-sm-3">
                        <label class="col-sm-3"><?php echo $compromiso->vigencia?></label>
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Estado</label>
                     <div class="col-sm-9">
                        <select name="active">
                            <option value="0" <?php echo ($compromiso->active == 0)?"selected":""?>>inactivo</option>
                            <option value="1" <?php echo ($compromiso->active == 1)?"selected":""?>>activo</option>
                        </select>
                     </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            @endif 
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection