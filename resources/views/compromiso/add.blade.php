@extends('app')
 
@section('content')
<script type="text/javascript">
    var cargarCargosDefault = true;
</script>
@include('partials.compromiso.add')
@include('partials.compromiso.cargos')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Compromiso</h2>
            <h4>Diligencie la información del Compromiso</h4>
            @if (count($niveles) < 1 || count($cargos) < 1)
                <div class="form-group right"> 
                        <div class="col-sm-12">
                            No existen niveles ó cargos Registradas
                        </div>
                </div>
                <div>                            
                        <input id="regresar" route="compromisoshowall" type="button" value="Regresar" class="btn btn-rw btn-primary center-block">
                </div>
            @elseif (count($niveles) >= 1)
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('compromisoadd', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione el Nivel Jerarquico</label>
                    <div class="col-sm-9">
                        <select name="nivel_jerarquico_id" id="nivel_jerarquico_id">
                        @foreach ($niveles as $n)
                            <option value="{{ $n->id }}">{{ $n->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione el Cargo</label>
                    <div class="col-sm-9" id="cargo-list-container">
                        <select name="cargo_id" id="cargo_id">
                        @foreach ($cargos as $c)
                                <option value="{{ $c->id }}">{{ $c->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Nombre Completo</label>
                     <div class="col-sm-9">
                        <input type="description" name="description" class="form-control" value="">
                     </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Vigencia</label>
                    <div class="col-sm-3">
                        <select name="vigencia">
                        <?php
                            for($i = 2015; $i < date('Y') + 1; $i++ ){
                        ?>
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php  
                            }
                        ?>
                        </select>
                    </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            @endif 
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection