@extends('app')
 
@section('content')
@include('partials.datepicker')
@include('partials.profiles.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Perfil</h2>
            <h4>Diligencie la información del Perfil</h4>
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('profileedit', [])}}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="selectedCargo" id="selectedCargo" value="<?php echo $selectedCargo;?>" />
                
                <div class="form-group right"> 
                    <label class="col-sm-3">Area/Dependencia</label>
                    <div class="col-sm-9">
                        <select name="area_id">
                            @foreach ($areas as $a)
                                <option value="{{ $a->id }}" <?php echo ($a->id == $profile->area_id)?"selected":"";?>>{{ $a->description }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group right"> 
                    <label class="col-sm-3">Nivel Jerárquico</label>
                    <div class="col-sm-9">
                        <select name="nivel_id" id="nivel_id">
                            @foreach ($niveles as $n)
                                <option value="{{ $n->id }}" <?php echo ($n->id == $selectedNivel)?"selected":"";?>>{{ $n->description }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Cargo</label>
                    <div class="col-sm-9" id= "cargo-list-container">
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">nombre de usuario</label>
                    <div class="col-sm-9">
                        <input type="text" value="<?php echo $user->name?>" name="user_name" class="form-control" readonly>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre</label>
                    <div class="col-sm-9">
                        <input type="text" value="{{ $profile->name }}" name="name" id="name" class="form-control">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Apellido</label>
                     <div class="col-sm-9">
                        <input type="description" value="{{ $profile->lastname }}" name="lastname" id="lastname" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Tipo de documento de identificación</label>
                     <div class="col-sm-9">
                        <select name="document_type" id="document_type">
                            <option value="CC" <?php echo ($profile->document_type=="CC")?"selected":""?>>Cédula de Ciudadanía</option>
                            <option value="CE" <?php echo ($profile->document_type=="CE")?"selected":""?>>Cédula de Extrangería</option>
                            <option value="PP" <?php echo ($profile->document_type=="PP")?"selected":""?>>Pasaporte</option>
                            <option value="TI" <?php echo ($profile->document_type=="TI")?"selected":""?>>Tarjeta de Identidad</option>
                        </select>
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Número de documento</label>
                     <div class="col-sm-9">
                        <input type="description" value="{{ $profile->document_number }}" name="document_number" id="document_number" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Fecha de Nacimiento</label>
                     <div class="col-sm-9">
                        <input type="description" value="{{ $profile->birthday }}" name="birthday" id="birthday" class="form-control" readonly>
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Dirección</label>
                     <div class="col-sm-9">
                        <input type="description" value="{{ $profile->address }}" name="address" id="address" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Género</label>
                     <div class="col-sm-9">
                        <select name="gender" id="gender">
                            <option value="M" <?php echo ($profile->gender=="M")?"selected":""?>>Masculino</option>
                            <option value="F" <?php echo ($profile->gender=="F")?"selected":""?>>Femenino</option>
                            <option value="O" <?php echo ($profile->gender=="O")?"selected":""?>>Otro/No Aplica</option>
                        </select>
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Teléfono</label>
                     <div class="col-sm-9">
                        <input type="description" value="{{ $profile->phone }}" name="phone" id="phone" class="form-control">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Celular</label>
                     <div class="col-sm-9">
                        <input type="description" value="{{ $profile->cellphone }}" name="cellphone" id="cellphone" class="form-control">
                     </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection