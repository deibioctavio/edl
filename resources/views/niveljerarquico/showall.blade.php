@extends('app')
 
@section('content')
@include('partials.niveles.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Nivel Jerarquico</h2>
            <h4>Listado de Niveles Jerarquicos Registrados</h4>    
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre Corto</th>
                            <th>Nombre Completo (Descripción)</th>
                            <th colspan="1">Vigencia</th>
                            <th colspan="2">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($niveles) < 1)
                            <tr>
                                <td colspan="5">No existen niveles jerarquicos registrados</td>
                            </tr>
                        @elseif (count($niveles) >= 1)
                            @foreach ($niveles as $n)
                                <tr>
                                    <td>{{ $n->id }}</td>
                                    <td>{{ $n->name }}</td>
                                    <td>{{ $n->description }}</td>
                                    <td>{{ $n->vigencia }}</td>
                                    <td><a href="<?php echo url('niveljerarquicoedit',[$n->id])?>">Actualizar</a></td>
                                    <td><a id="nivel-del" number="<?php echo $n['id']?>" href="<?php echo url('niveljerarquicodelete',[$n->id])?>">Eliminar</a></td>
                                    </td>
                                </tr>
                            @endforeach
                        @endif 
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection