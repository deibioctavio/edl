@extends('app')
 
@section('content')
@include('partials.configuracionevaluaciones.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Configuración</h2>
            <h4>Diligencie la información de configuración</h4>  
             
            <form 
                class="form-horizontal" 
                accept-charset="UTF-8" 
                action="{{route('configuracionevaluacionedit', [])}}" 
                method="POST" 
                id="register-frm"
            >
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" value="{{ $configuracion->id }}" name="id" class="form-control">
                <div class="form-group right"> 
                    <label class="col-sm-9">#Compromisos Laborales</label>
                    <div class="col-sm-3">
                        <input 
                            type="text" value="{{ $configuracion->n_compromisos_laborales }}" 
                            name="n_compromisos_laborales" 
                            class="form-control"
                        >
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Valor/Peso porcentual (%)</label>
                    <div class="col-sm-3">
                        <input 
                            type="text" value="{{ $configuracion->valor_compromisos_laborales }}" 
                            name="valor_compromisos_laborales" 
                            class="form-control" 
                            item="percen-value"
                        >
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">#Competencias Comportamentales</label>
                    <div class="col-sm-3">
                        <input 
                            type="text" value="{{ $configuracion->n_competencias_comportamentales }}" 
                            name="n_competencias_comportamentales" 
                            class="form-control"
                        >
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Valor/Peso porcentual (%)</label>
                    <div class="col-sm-3">
                        <input 
                            type="text" value="{{ $configuracion->valor_competencias_comportamentales }}" 
                            name="valor_competencias_comportamentales" 
                            class="form-control" 
                            item="percen-value"
                        >
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Vigencia</label>
                    <div class="col-sm-3"><b>{{$configuracion->vigencia}}</b></div>
                </div>
                
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection