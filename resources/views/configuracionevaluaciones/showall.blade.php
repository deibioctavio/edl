@extends('app')
 
@section('content')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Configuración Evaluaciones</h2>
            <h4>Listado de parametros de configiración de las evaluaciones</h4>    
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Vigencia(Año)</th>
                            <th>#Compromisos Laborales</th>
                            <th>Peso(%)</th>
                            <th>#Competencias Comportamentales</th>
                            <th>Peso(%)</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($configuraciones) < 1)
                            <tr>
                                <td colspan="5">No existen configuraciones registradas</td>
                            </tr>
                        @elseif (count($configuraciones) >= 1)
                            @foreach ($configuraciones as $c)
                                <tr>
                                    <td>{{ $c->vigencia }}</td>
                                    <td>{{ $c->n_compromisos_laborales }}</td>
                                    <td>{{ $c->valor_compromisos_laborales }}</td>
                                    <td>{{ $c->n_competencias_comportamentales }}</td>
                                    <td>{{ $c->valor_competencias_comportamentales }}</td>
                                    
                                    <td>
                                        <a 
                                            href="<?php echo url('configuracionevaluacionedit',[$c->id])?>"
                                        >Actualizar
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif 
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection