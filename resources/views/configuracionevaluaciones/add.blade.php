@extends('app')
 
@section('content')
@include('partials.configuracionevaluaciones.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Configuración</h2>
            <h4>Diligencie la información de configuración</h4>    
                            
            <form 
                    id="register-frm" 
                    class="form-horizontal" 
                    accept-charset="UTF-8" 
                    action="{{route('configuracionevaluacionadd', [])}}" 
                    method="POST"
            >
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group right"> 
                    <label class="col-sm-9">#Compromisos Laborales</label>
                    <div class="col-sm-3">
                        <input 
                            type="text" value="" 
                            name="n_compromisos_laborales" 
                            id="n_compromisos_laborales"
                            class="form-control"
                        >
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Valor/Peso porcentual (%)</label>
                    <div class="col-sm-3">
                        <input 
                            type="text" value="" 
                            name="valor_compromisos_laborales" 
                            id="valor_compromisos_laborales"
                            class="form-control"
                            item="percen-value"
                        >
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">#Competencias Comportamentales</label>
                    <div class="col-sm-3">
                        <input 
                            type="text" value="" 
                            name="n_competencias_comportamentales" 
                            id="n_competencias_comportamentales"
                            class="form-control"
                        >
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Valor/Peso porcentual (%)</label>
                    <div class="col-sm-3">
                        <input 
                            type="text" value="" 
                            name="valor_competencias_comportamentales" 
                            id="valor_competencias_comportamentales"
                            class="form-control"
                            item="percen-value"
                        >
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Vigencia</label>
                    <div class="col-sm-3" id="vigencia-container"><!--
                        <select name="vigencia" id="vigencia">
                        <?php
                            for($i = 2015; $i < date('Y') + 5; $i++ ){
                        ?>
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php  
                            }
                        ?>
                        </select>-->
                    </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection