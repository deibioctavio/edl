@extends('app')
 
@section('content')
@include('partials.cargo.regresar')
@include('partials.cargo.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Cargo</h2>
            <h4>Diligencie la información del Cargo</h4>
            @if (count($cargos) < 1)
                <div class="form-group right"> 
                        <div class="col-sm-12">
                            No existen Cargos Registrados
                        </div>
                </div>
                <div>                            
                        <input id="regresar" route="cargoshowall" type="button" value="Regresar" class="btn btn-rw btn-primary center-block">
                </div>
            @elseif (count($cargos) >= 1)
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('cargoadd', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione el Nivel Jerarquico</label>
                    <div class="col-sm-9">
                        <select name="nivel_jerarquico_id" id="nivel_jerarquico_id">
                        @foreach ($cargos as $c)
                            <option value="{{ $c->nivel_jerarquico_id }}">{{ $c->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre Corto (sin espacios)</label>
                    <div class="col-sm-9">
                        <input type="text" value="" name="name" class="form-control">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Nombre Completo</label>
                     <div class="col-sm-9">
                        <input type="description" value="" name="description" class="form-control">
                     </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Vigencia</label>
                    <div class="col-sm-3">
                        <select name="vigencia">
                        <?php
                            for($i = 2015; $i < date('Y') + 1; $i++ ){
                        ?>
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php  
                            }
                        ?>
                        </select>
                    </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            @endif 
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection