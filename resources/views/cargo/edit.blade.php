@extends('app')
 
@section('content')
@include('partials.cargo.regresar')
@include('partials.cargo.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Cargo</h2>
            <h4>Diligencie la información del Cargo</h4>
            @if (count($niveles) < 1)
                <div class="form-group right"> 
                        <div class="col-sm-12">
                            No existen cargos Registrados
                        </div>
                </div>
                <div>                            
                        <input id="regresar" route="cargoshowall" type="button" value="Regresar" class="btn btn-rw btn-primary center-block">
                </div>
            @elseif (count($niveles) >= 1)
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('cargoedit', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="id" class="form-control" value="{{ $cargos->id }}">
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione el Nivel Jerarquico</label>
                    <div class="col-sm-9">
                        <select name="nivel_jerarquico_id" id="nivel_jerarquico_id">
                        @foreach ($niveles as $n)
                            @if( $n->id === $cargos->nivel_jerarquico_id)
                                <option value="{{ $n->id }}" selected>{{ $n->name }}</option>
                            @else
                                <option value="{{ $n->id }}">{{ $n->name }}</option>
                            @endif 
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre Corto Del Cargo (sin espacios)</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control" value="{{ $cargos->name }}">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Nombre Completo Del Cargo</label>
                     <div class="col-sm-9">
                        <input type="description" name="description" class="form-control" value="{{ $cargos->description }}">
                     </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Vigencia</label>
                    <div class="col-sm-3">
                        <label class="col-sm-3"><?php echo $cargos->vigencia?></label>
                    </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            @endif 
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection