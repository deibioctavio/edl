@extends('app')
 
@section('content')
@include('partials.cargo.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Cargos</h2>
            <h4>Listado de Cargos Registrados</h4>    
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre del Nivel Jerarquico</th>
                            <th>Nombre Cargo</th>
                            <th>Nombre Completo (Descripción)</th>
                            <th colspan="1">Vigencia</th>
                            <th colspan="2">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($niveles) < 1)
                            <tr>
                                <td colspan="5">No existen niveles registrados</td>
                            </tr>
                        @elseif (count($niveles) >= 1)
                        <?php 
                            $i = 0;
                        ?>
                            @foreach ($niveles as $n)
                            <?php
                                $cargos = $nivelCargo[$n->id];

                                foreach ($cargos['cargos'] as $c) {
                                    $i++;
                            ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $n->name }}</td>
                                    <td>{{ $c['name'] }}</td>
                                    <td>{{ $c['description'] }}</td>
                                    <td>{{ $c['vigencia'] }}</td>
                                    <td><a href="<?php echo url('cargoedit',[$c['id']])?>">Actualizar</a></td>
                                    <td><a  id="cargo-del" number="<?php echo $c['id']?>" href="<?php echo url('cargodelete',[$c['id']])?>">Eliminar</a></td>
                                </tr>
                            <?php        
                                }
                            ?>    
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection