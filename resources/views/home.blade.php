@extends('app')
 
@section('content')
 <div class="container">
    <div class="row">
       <br />
            @include('partials.messages')
      <hr class="mt10 mb40">
        <h1>Bienvenidos a EDL</h1>
        <hr class="mt10 mb40 invisible">
        <p class="lead">
            Evaluación de Desempeño Laboral
        </p>
        <p>
            Proximamente aqu&iacute; encontraras informaci&oacute;n actualizada respecto a las actividades 
            de Evaluación del Desempeño Laboral de los empleados de carrera y en período de prueba conforme lo establecen los acuerdos 137 (Evaluación Tipo) y 138 (Evaluación Propia) de 2010 de la Comisión Nacional del Servicio Civil - CNSC.
        </p>

        <hr class="mt10 mb10">
        <!--
        @for ($i = 0; $i < 5; $i++)
            @include('partials.fileuploadajax',
                                                array(
                                                        "formId" => $i,
                                                        "uploadControllerRoute" => "addajax",
                                                        "uploadLinkName" => "Subir Archivo",
                                                        "hiddemForm" => true
                                                    )
                    )
        @endfor
        -->
    </div>
</div>
@endsection