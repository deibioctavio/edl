@extends('app')
 
@section('content')
@include('partials.conducta.regresar')
@include('partials.conducta.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Conductas</h2>
            <h4>Diligencie la información de la Conducta</h4>   
            @if (count($competencias) < 1)
                <div class="form-group right"> 
                        <div class="col-sm-12">
                            No existen cargos Registrados
                        </div>
                </div>
                <div>                            
                        <input id="regresar" route="conductashowall" type="button" value="Regresar" class="btn btn-rw btn-primary center-block">
                </div>
            @elseif (count($competencias) >= 1)
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('conductaedit', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden"  name="id" class="form-control" value="{{ $conducta->id }}">
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione la Competencia</label>
                    <div class="col-sm-9">
                        <select name="competencia_id" id="competencia_id">
                        @foreach ($competencias as $c)
                            @if( $c->id === $conducta->competencia_id)
                                <option value="{{ $c->id }}" selected>{{ $c->name }}</option>
                            @else
                                <option value="{{ $c->id }}">{{ $c->name }}</option>
                            @endif 
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre Corto (sin espacios)</label>
                    <div class="col-sm-9">
                        <input type="text" value="{{ $conducta->name }}" name="name" class="form-control">
                    </div>
                </div>
                <!--
                <div class="form-group right">
                    <label class="col-sm-3">Nombre Completo</label>
                     <div class="col-sm-9">
                        <input type="description" value="{{ $conducta->description }}" name="description" class="form-control">
                     </div>
                </div>-->
                <div class="form-group right"> 
                        <label class="col-sm-9">Vigencia</label>
                        <div class="col-sm-3">
                            <label class="col-sm-3"><?php echo $conducta->vigencia?></label>
                        </div>
                    </div>
                <div class="form-group right">
                    <label class="col-sm-3">Estado</label>
                     <div class="col-sm-9">
                        <select name="active">
                            <option value="0" <?php echo ($conducta->active == 0)?'selected':""?>>inactivo</option>
                            <option value="1" <?php echo ($conducta->active == 1)?'selected':""?>>activo</option>
                        </select>
                     </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            @endif 
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection