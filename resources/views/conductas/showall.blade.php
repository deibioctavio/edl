@extends('app')
 
@section('content')
@include('partials.conducta.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Conductas</h2>
            <h4>Listado de Conductas Registradas</h4>    
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre de la Competencia</th>
                            <th>Nombre de la Conducta</th>
                            <th colspan="1">Vigencia</th>
                            <th colspan="2">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($competencias) < 1)
                            <tr>
                                <td colspan="5">No existen conductas registrados</td>
                            </tr>
                        @elseif (count($competencias) >= 1)
                        <?php 
                            $i = 0;
                        ?>
                            @foreach ($competencias as $c)
                            <?php
                                $conductas = $competenciaConducta[$c->id];

                                foreach ($conductas['conductas'] as $p) {
                                    $i++;
                            ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $c->name }}</td>
                                    <td>{{ $p['name'] }}</td>
                                    <td>{{ $p['vigencia'] }}</td>
                                    <td><a href="<?php echo url('conductaedit',[$p['id']])?>">Actualizar</a></td>
                                    <td><a id="conducta-del" number="<?php echo $p['id']?>" href="<?php echo url('conductadelete',[$p['id']])?>">Eliminar</a></td>
                                </tr>
                            <?php        
                                }
                            ?>    
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection