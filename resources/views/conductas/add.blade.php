@extends('app')
 
@section('content')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Conductas</h2>
            <h4>Diligencie la información de la Conducta</h4>
            @if (count($competencias) < 1)
                <div class="form-group right"> 
                        <div class="col-sm-12">
                            No existen conductas Registradas
                        </div>
                </div>
                <div>                            
                        <input id="regresar" route="conductashowall" type="button" value="Regresar" class="btn btn-rw btn-primary center-block">
                </div>
            @elseif (count($competencias) >= 1)
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('conductaadd', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione la Competencia</label>
                    <div class="col-sm-9">
                        <select name="competencia_id" id="competencia_id">
                        @foreach ($competencias as $c)
                            <option value="{{ $c->id }}">{{ $c->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre Corto (sin espacios)</label>
                    <div class="col-sm-9">
                        <input type="text" value="" name="name" class="form-control">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Nombre Completo</label>
                     <div class="col-sm-9">
                        <input type="description" value="" name="description" class="form-control">
                     </div>
                </div>
                <div class="form-group right"> 
                        <label class="col-sm-9">Vigencia</label>
                        <div class="col-sm-3">
                            <select name="vigencia">
                                <?php
                                    for($i = 2015; $i < date('Y') + 1; $i++ ){
                                ?>
                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php  
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            @endif 
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection