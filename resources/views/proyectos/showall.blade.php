@extends('app')
 
@section('content')
@include('partials.proyectos.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Proyecto</h2>
            <h4>Listado de Proyectos Registrados</h4>    
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre del Área</th>
                            <th>Nombre Proyecto</th>
                            <th>Nombre Completo (Descripción)</th>
                            <th>Vigencia</th>
                            <th colspan="2">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($areas) < 1)
                            <tr>
                                <td colspan="5">No existen áreas registrados</td>
                            </tr>
                        @elseif (count($areas) >= 1)
                        <?php 
                            $i = 0;
                        ?>
                            @foreach ($areas as $a)
                            <?php
                                $proyectos = $areaProyecto[$a->id];
                                foreach ($proyectos['proyectos'] as $p) {
                                    $i++;
                            ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $a->name }}</td>
                                    <td>{{ $p['name'] }}</td>
                                    <td>{{ $p['description'] }}</td>
                                    <td>{{ $p['vigencia'] }}</td>
                                    <td>
                                    <a href="<?php echo url('proyectoedit',[$p['id']])?>">Actualizar</a></td>
                                    <td><a id="proyecto-del" number="<?php echo $p['id']?>" href="<?php echo url('proyectodelete',[$p['id']])?>">Eliminar</a></td>
                                </tr>
                            <?php        
                                }
                            ?>    
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection