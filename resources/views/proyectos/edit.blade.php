@extends('app')
 
@section('content')
@include('partials.proyectos.regresar')
@include('partials.proyectos.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Proyecto</h2>
            <h4>Diligencie la información del Proyecto</h4>
            @if (count($areas) < 1)
                <div class="form-group right"> 
                        <div class="col-sm-12">
                            No existen áreas Registradas
                        </div>
                </div>
                <div>                            
                        <input id="regresar" route="proyectoshowall" type="button" value="Regresar" class="btn btn-rw btn-primary center-block">
                </div>
            @elseif (count($areas) >= 1)
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('proyectoedit', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="id" class="form-control" value="{{ $proyecto->id }}">
                <div class="form-group right"> 
                    <label class="col-sm-3">Seleccione el Área</label>
                    <div class="col-sm-9">
                        <select name="area_id" id="area_id">
                        @foreach ($areas as $a)
                            @if( $a->id === $proyecto->area_id)
                                <option value="{{ $a->id }}" selected>{{ $a->name }}</option>
                            @else
                                <option value="{{ $a->id }}">{{ $a->name }}</option>
                            @endif 
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre Corto (sin espacios)</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" class="form-control" value="{{ $proyecto->name }}">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Nombre Completo</label>
                     <div class="col-sm-9">
                        <input type="description" name="description" class="form-control" value="{{ $proyecto->description }}">
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Estado</label>
                     <div class="col-sm-9">
                        <select name="active">
                            <option value="0" <?php echo ($proyecto->active == 0)?"selected":""?>>inactivo</option>
                            <option value="1" <?php echo ($proyecto->active == 1)?"selected":""?>>activo</option>
                        </select>
                     </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Vigencia</label>
                    <div class="col-sm-3">
                        <label class="col-sm-3"><?php echo $proyecto->vigencia?></label>
                    </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            @endif 
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection