@extends('app')
 
@section('content')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Competencias</h2>
            <h4>Diligencie la información de la Competencia</h4>    
                            
            <form class="form-horizontal" accept-charset="UTF-8" action="{{route('competenciaadd', [])}}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group right">
                        <label class="col-sm-3">Nombre Completo</label>
                         <div class="col-sm-9">
                            <input type="description" value="" name="description" class="form-control">
                         </div>
                    </div>
                    <div class="form-group right"> 
                        <label class="col-sm-9">Vigencia</label>
                        <div class="col-sm-3">
                            <select name="vigencia">
                                <?php
                                    for($i = 2015; $i < date('Y') + 1; $i++ ){
                                ?>
                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php  
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div>                            
                        <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                    </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection