@extends('app')
 
@section('content')
@include('partials.competencia.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Competencias</h2>
            <h4>Listado de Competencias Registradas</h4>    
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th colspan="1">Vigencia</th>
                            <th colspan="2">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($competencias) < 1)
                            <tr>
                                <td colspan="5">No existen competencias registradas</td>
                            </tr>
                        @elseif (count($competencias) >= 1)
                            @foreach ($competencias as $c)
                                <tr>
                                    <td>{{ $c->id }}</td>
                                    <td>{{ $c->name }}</td>
                                    <td>{{ $c->description }}</td>
                                    <td>{{ $c->vigencia }}</td>
                                    <td><a href="<?php echo url('competenciaedit',[$c->id])?>">Actualizar</a></td>
                                    <td><a id="competencia-del" number="<?php echo $c['id']?>" href="<?php echo url('competenciadelete',[$c->id])?>">Eliminar</a></td>
                                    </td>
                                </tr>
                            @endforeach
                        @endif 
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection