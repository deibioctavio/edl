@extends('app')
 
@section('content')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Competencia</h2>
            <h4>Diligencie la información de la Competencia</h4>    
                            
            <form class="form-horizontal" accept-charset="UTF-8" action="{{route('competenciaedit', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" value="{{ $competencia->id }}" name="id" class="form-control">
                <div class="form-group right"> 
                    <label class="col-sm-3">Nombre Corto (sin espacios)</label>
                    <div class="col-sm-9">
                        <input type="text" value="{{ $competencia->name }}" name="name" class="form-control">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Nombre Completo</label>
                     <div class="col-sm-9">
                        <input type="description" value="{{ $competencia->description }}" name="description" class="form-control">
                     </div>
                </div>
                <div class="form-group right"> 
                    <label class="col-sm-9">Vigencia</label>
                    <div class="col-sm-3">
                       <label class="col-sm-3"><?php echo $competencia->vigencia?></label>
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-3">Estado</label>
                     <div class="col-sm-9">
                        <select name="active">
                            <option value="0" <?php echo ($competencia->active == 0)?"selected":""?>>inactivo</option>
                            <option value="1" <?php echo ($competencia->active == 1)?"selected":""?>>activo</option>
                        </select>
                     </div>
                </div>
                <div>                            
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection