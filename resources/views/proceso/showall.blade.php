@extends('app')
 
@section('content')
@include('partials.areas.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Procesos</h2>
            <h4>Listado de Procesos Registrados</h4>    
            
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Identificador</th>
                            <th>Descripción</th>
                            <th>Vigencia</th>
                            <th>Area</th>
                            <th>Macroproceso</th>
                            <th colspan="2">Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($proceso) < 1)
                            <tr>
                                <td colspan="5">No existen Procesos registrados</td>
                            </tr>
                        @elseif (count($proceso) >= 1)
                            @foreach ($proceso as $p)
                                <tr>
                                    <td>{{ $p->id }}</td>
                                    <td>{{ $p->name }}</td>
                                    <td>{{ $p->identificator }}</td>
                                    <td>{{ $p->description }}</td>
                                    <td>{{ $p->vigencia }}</td>
                                    @foreach ($area as $a)
                                        @if($p->area_id == $a->id)
                                        <td>{{ $a->name }}</td>
                                        @endif
                                    @endforeach
                                    @foreach ($macroproceso as $m)
                                        @if($p->macroproceso_id == $m->id)
                                        <td>{{ $m->name }}</td>
                                        @endif
                                    @endforeach
                                    <td><a href="<?php echo url('procesoedit',[$p->id])?>">Actualizar</a></td>
                                    <td><a id="area-del" number="<?php echo $p['id']?>"  href="<?php echo url('procesodelete',[$p->id])?>">Eliminar</a></td>
                                    </td>
                                </tr>
                            @endforeach
                        @endif 
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div> 
    </div>
</div>
@endsection