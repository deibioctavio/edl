<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben tener al menos seis caracteres y concidir con la confirmación.',
    'reset' => 'Su contraseña ha sido reestablecida!',
    'sent' => 'Enviamos un correo electrónico con un enlace de reestablecimiento de contraseña!',
    'token' => 'Este token de restablecimiento de contraseña no es válida.',
    'user' => "No encontramos un usuario con esta dirección de correo electrónico.",

];
