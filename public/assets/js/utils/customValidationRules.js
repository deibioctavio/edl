$(document).ready(function(){

    jQuery.validator.addMethod("decimal_number", function (value, element) {
        return this.optional(element) || /^(\d+|\d+\.+\d{1,2})$/.test(value);
    },"Ingrese un número decimal valido. Emplee el punto (.) como separador de decimales y con máximo dos decimales");

    jQuery.validator.addMethod("dateGreaterThan", 

        function(value,element,params) {

            //console.log($(element));
            //console.log(params.anotherDate);
            
            var fecha1 = $(element).val();
            var fecha2 = params.anotherDate.val();

            //console.log(fecha1);
            //console.log(fecha2);

            if( fecha1.length == 0 || fecha2.length == 0 ){

                console.log("alguna fecha vacia");
                return false;
            }else{

                fecha1Array = fecha1.split("-");
                fecha2Array = fecha2.split("-");
                //console.log(fecha1Array);
                //console.log(fecha2Array);

                var fecha1DateObject = new Date(fecha1Array[0],fecha1Array[1],fecha1Array[2],0,0);
                var fecha2DateObject = new Date(fecha2Array[0],fecha2Array[1],fecha2Array[2],0,0);

                //console.log(fecha1DateObject.getTime());
                //console.log(fecha2DateObject.getTime());
            }
                
            return fecha1DateObject.getTime() <= fecha2DateObject.getTime()?false:true;
        }, 
        
        "Verifique las fechas"
    );

    jQuery.validator.addMethod("dateGreaterOrEqualThan", 

        function(value,element,params) {

            //console.log($(element));
            //console.log(params.anotherDate);
            
            var fecha1 = $(element).val();
            var fecha2 = params.anotherDate.val();

            //console.log(fecha1);
            //console.log(fecha2);

            if( fecha1.length == 0 || fecha2.length == 0 ){

                console.log("alguna fecha vacia");
                return false;
            }else{

                fecha1Array = fecha1.split("-");
                fecha2Array = fecha2.split("-");
                //console.log(fecha1Array);
                //console.log(fecha2Array);

                var fecha1DateObject = new Date(fecha1Array[0],fecha1Array[1],fecha1Array[2],0,0);
                var fecha2DateObject = new Date(fecha2Array[0],fecha2Array[1],fecha2Array[2],0,0);

                //console.log(fecha1DateObject.getTime());
                //console.log(fecha2DateObject.getTime());
            }
                
            return fecha1DateObject.getTime() < fecha2DateObject.getTime()?false:true;
        }, 
        
        "Verifique las fechas"
    );

    jQuery.validator.addMethod("dateLessThan", 

        function(value,element,params) {

            //console.log($(element));
            //console.log(params.anotherDate);
            
            var fecha1 = $(element).val();
            var fecha2 = params.anotherDate.val();

            //console.log(fecha1);
            //console.log(fecha2);

            if( fecha1.length == 0 || fecha2.length == 0 ){

                console.log("alguna fecha vacia");
                return false;
            }else{

                fecha1Array = fecha1.split("-");
                fecha2Array = fecha2.split("-");
                //console.log(fecha1Array);
                //console.log(fecha2Array);

                var fecha1DateObject = new Date(fecha1Array[0],fecha1Array[1],fecha1Array[2],0,0);
                var fecha2DateObject = new Date(fecha2Array[0],fecha2Array[1],fecha2Array[2],0,0);

                //console.log(fecha1DateObject.getTime());
                //console.log(fecha2DateObject.getTime());
            }
                
            return fecha1DateObject.getTime() >= fecha2DateObject.getTime()?false:true;
        }, 
        
        "Verifique las fechas"
    );

    jQuery.validator.addMethod("dateLessOrEqualThan", 

        function(value,element,params) {

            //console.log($(element));
            //console.log(params.anotherDate);
            
            var fecha1 = $(element).val();
            var fecha2 = params.anotherDate.val();

            //console.log(fecha1);
            //console.log(fecha2);

            if( fecha1.length == 0 || fecha2.length == 0 ){

                console.log("alguna fecha vacia");
                return false;
            }else{

                fecha1Array = fecha1.split("-");
                fecha2Array = fecha2.split("-");
                //console.log(fecha1Array);
                //console.log(fecha2Array);

                var fecha1DateObject = new Date(fecha1Array[0],fecha1Array[1],fecha1Array[2],0,0);
                var fecha2DateObject = new Date(fecha2Array[0],fecha2Array[1],fecha2Array[2],0,0);

                //console.log(fecha1DateObject.getTime());
                //console.log(fecha2DateObject.getTime());
            }
                
            return fecha1DateObject.getTime() > fecha2DateObject.getTime()?false:true;
        }, 
        
        "Verifique las fechas"
    );
});