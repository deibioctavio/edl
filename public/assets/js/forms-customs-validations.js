/**
 * @author Octavio
 */
jQuery.validator.addMethod("passcheck", 
		
	function() {
			return $('form#register-frm input#password2').val() == $('form#register-frm input#password').val();
	}, 
	
	"La contraseña ingresada no corresponde con la original"
);