-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 20, 2015 at 05:30 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `edl`
--

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `name`, `description`, `active`, `created_at`, `updated_at`) VALUES
(1, 'economico', 'Secretaría de Desarrollo Económico', 1, '0000-00-00 00:00:00', '2015-12-01 23:51:10'),
(2, 'social', 'Secretaría de Desarrollo Social', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'educacion', 'Secretaría de Educación', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'gobierno', 'Secretaría de Gobierno y Convivencia', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'infraestructura', 'Secretaría de Infraestructura', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'salud', 'Secretaría de Salud', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'tics', 'Secretaría de Las Tecnologías de la Información', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'transito', 'Secretaría de Transito y Transporte', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'bienes', 'Bienes y Suministros', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'control', 'Control Interno', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'dafi', 'Fortalecimiento Institucional', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'hacienda', 'Hacienda Municipal', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'juridico', 'Departamento Jurídico', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'planeacion', 'Departamento de Planeación', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'nuevaarea', 'LA nueva Área', 1, '2015-12-01 06:16:26', '2015-12-01 06:16:26'),
(18, 'deporteysalud', 'Secretaría Especial de Deporte y Salud', 1, '2015-12-01 16:19:26', '2015-12-01 16:19:26');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_profile`
--

CREATE TABLE `attribute_profile` (
  `attribute_id` int(10) unsigned NOT NULL,
  `profile_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `length` char(6) COLLATE utf8_unicode_ci NOT NULL,
  `default_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extra` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nullable` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cargos`
--

CREATE TABLE `cargos` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  `grade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `nivel_jerarquico_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cargos`
--

INSERT INTO `cargos` (`id`, `name`, `code`, `grade`, `description`, `active`, `nivel_jerarquico_id`, `created_at`, `updated_at`) VALUES
(1, 'tecnico02-200', 200, '02', 'Técnico', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'tecnico05-201', 201, '05', 'Técnico', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'tecnico08-202', 202, '08', 'Técnico', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'asistencial02-203', 203, '02', 'Asistencial', 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'asistencial05-204', 204, '05', 'Asistencial', 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'asistencial08-205', 205, '08', 'Asistencial', 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'profesional02-206', 206, '02', 'Profesional', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'profesional05-207', 207, '05', 'Profesional', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'profesional08-208', 208, '08', 'Profesional', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'profesional_especializado11-209', 209, '11', 'Profesional Especializado', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'profesional_especializado15-210', 210, '15', 'Profesional Especializado', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'profesional_especializado19-211', 211, '19', 'Profesional Especializado', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'profesional_especializado23-212', 212, '23', 'Profesional Especializado', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'profesional_maestria27-213', 213, '27', 'Profesional con Maestria', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'profesional_maestria29-214', 214, '29', 'Profesional con Maestria', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'profesional_maestria31-215', 215, '31', 'Profesional con Maestria', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'profesional_maestria33-216', 216, '33', 'Profesional con Maestria', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'profesional_maestria35-217', 217, '35', 'Profesional con Maestria', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'profesional_maestria37-218', 218, '37', 'Profesional con Maestria', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'profesional_maestria39-219', 219, '39', 'Profesional con Maestria', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'profesional_maestria41-220', 220, '41', 'Profesional con Maestria', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'profesional_doctorado43-221', 221, '43', 'Profesional con Doctorado/Postdocorado', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'profesional_doctorado46-222', 222, '46', 'Profesional con Doctorado/Postdocorado', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'profesional_doctorado49-223', 223, '49', 'Profesional con Doctorado/Postdocorado', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'profesional_doctorado52-224', 224, '52', 'Profesional con Doctorado/Postdocorado', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'profesional_doctorado55-225', 225, '55', 'Profesional con Doctorado/Postdocorado', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `compromisos`
--

CREATE TABLE `compromisos` (
  `id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `goal` int(11) NOT NULL,
  `obtained_percentage` int(11) NOT NULL,
  `comments` text COLLATE utf8_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `cargo_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `compromisos`
--

INSERT INTO `compromisos` (`id`, `description`, `goal`, `obtained_percentage`, `comments`, `active`, `cargo_id`, `created_at`, `updated_at`) VALUES
(1, 'tecnico02-200 Compromiso: 0 -  0', 100, 0, NULL, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'tecnico02-200 Compromiso: 1 -  1', 100, 0, NULL, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'tecnico02-200 Compromiso: 2 -  2', 100, 0, NULL, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'tecnico02-200 Compromiso: 3 -  3', 100, 0, NULL, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'tecnico05-201 Compromiso: 0 -  4', 100, 0, NULL, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'tecnico05-201 Compromiso: 1 -  5', 100, 0, NULL, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'tecnico05-201 Compromiso: 2 -  6', 100, 0, NULL, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'tecnico05-201 Compromiso: 3 -  7', 100, 0, NULL, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'tecnico05-201 Compromiso: 4 -  8', 100, 0, NULL, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'tecnico08-202 Compromiso: 0 -  9', 100, 0, NULL, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'tecnico08-202 Compromiso: 1 -  10', 100, 0, NULL, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'tecnico08-202 Compromiso: 2 -  11', 100, 0, NULL, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'asistencial02-203 Compromiso: 0 -  12', 100, 0, NULL, 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'asistencial02-203 Compromiso: 1 -  13', 100, 0, NULL, 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'asistencial02-203 Compromiso: 2 -  14', 100, 0, NULL, 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'asistencial05-204 Compromiso: 0 -  15', 100, 0, NULL, 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'asistencial05-204 Compromiso: 1 -  16', 100, 0, NULL, 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'asistencial05-204 Compromiso: 2 -  17', 100, 0, NULL, 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'asistencial08-205 Compromiso: 0 -  18', 100, 0, NULL, 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'asistencial08-205 Compromiso: 1 -  19', 100, 0, NULL, 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'asistencial08-205 Compromiso: 2 -  20', 100, 0, NULL, 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'asistencial08-205 Compromiso: 3 -  21', 100, 0, NULL, 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'asistencial08-205 Compromiso: 4 -  22', 100, 0, NULL, 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'profesional02-206 Compromiso: 0 -  23', 100, 0, NULL, 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'profesional02-206 Compromiso: 1 -  24', 100, 0, NULL, 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'profesional02-206 Compromiso: 2 -  25', 100, 0, NULL, 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'profesional02-206 Compromiso: 3 -  26', 100, 0, NULL, 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'profesional02-206 Compromiso: 4 -  27', 100, 0, NULL, 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'profesional05-207 Compromiso: 0 -  28', 100, 0, NULL, 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'profesional05-207 Compromiso: 1 -  29', 100, 0, NULL, 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'profesional05-207 Compromiso: 2 -  30', 100, 0, NULL, 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'profesional08-208 Compromiso: 0 -  31', 100, 0, NULL, 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'profesional08-208 Compromiso: 1 -  32', 100, 0, NULL, 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'profesional08-208 Compromiso: 2 -  33', 100, 0, NULL, 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'profesional08-208 Compromiso: 3 -  34', 100, 0, NULL, 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'profesional08-208 Compromiso: 4 -  35', 100, 0, NULL, 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'profesional08-208 Compromiso: 5 -  36', 100, 0, NULL, 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'profesional_especializado11-209 Compromiso: 0 -  37', 100, 0, NULL, 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'profesional_especializado11-209 Compromiso: 1 -  38', 100, 0, NULL, 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'profesional_especializado11-209 Compromiso: 2 -  39', 100, 0, NULL, 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'profesional_especializado11-209 Compromiso: 3 -  40', 100, 0, NULL, 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'profesional_especializado11-209 Compromiso: 4 -  41', 100, 0, NULL, 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'profesional_especializado11-209 Compromiso: 5 -  42', 100, 0, NULL, 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'profesional_especializado15-210 Compromiso: 0 -  43', 100, 0, NULL, 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'profesional_especializado15-210 Compromiso: 1 -  44', 100, 0, NULL, 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'profesional_especializado15-210 Compromiso: 2 -  45', 100, 0, NULL, 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'profesional_especializado19-211 Compromiso: 0 -  46', 100, 0, NULL, 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'profesional_especializado19-211 Compromiso: 1 -  47', 100, 0, NULL, 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'profesional_especializado19-211 Compromiso: 2 -  48', 100, 0, NULL, 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'profesional_especializado19-211 Compromiso: 3 -  49', 100, 0, NULL, 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'profesional_especializado19-211 Compromiso: 4 -  50', 100, 0, NULL, 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'profesional_especializado19-211 Compromiso: 5 -  51', 100, 0, NULL, 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'profesional_especializado23-212 Compromiso: 0 -  52', 100, 0, NULL, 1, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'profesional_especializado23-212 Compromiso: 1 -  53', 100, 0, NULL, 1, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'profesional_especializado23-212 Compromiso: 2 -  54', 100, 0, NULL, 1, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'profesional_maestria27-213 Compromiso: 0 -  55', 100, 0, NULL, 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'profesional_maestria27-213 Compromiso: 1 -  56', 100, 0, NULL, 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'profesional_maestria27-213 Compromiso: 2 -  57', 100, 0, NULL, 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'profesional_maestria27-213 Compromiso: 3 -  58', 100, 0, NULL, 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'profesional_maestria29-214 Compromiso: 0 -  59', 100, 0, NULL, 1, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'profesional_maestria29-214 Compromiso: 1 -  60', 100, 0, NULL, 1, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'profesional_maestria29-214 Compromiso: 2 -  61', 100, 0, NULL, 1, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'profesional_maestria29-214 Compromiso: 3 -  62', 100, 0, NULL, 1, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'profesional_maestria31-215 Compromiso: 0 -  63', 100, 0, NULL, 1, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'profesional_maestria31-215 Compromiso: 1 -  64', 100, 0, NULL, 1, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'profesional_maestria31-215 Compromiso: 2 -  65', 100, 0, NULL, 1, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'profesional_maestria33-216 Compromiso: 0 -  66', 100, 0, NULL, 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'profesional_maestria33-216 Compromiso: 1 -  67', 100, 0, NULL, 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'profesional_maestria33-216 Compromiso: 2 -  68', 100, 0, NULL, 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'profesional_maestria33-216 Compromiso: 3 -  69', 100, 0, NULL, 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'profesional_maestria33-216 Compromiso: 4 -  70', 100, 0, NULL, 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'profesional_maestria35-217 Compromiso: 0 -  71', 100, 0, NULL, 1, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'profesional_maestria35-217 Compromiso: 1 -  72', 100, 0, NULL, 1, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'profesional_maestria35-217 Compromiso: 2 -  73', 100, 0, NULL, 1, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'profesional_maestria37-218 Compromiso: 0 -  74', 100, 0, NULL, 1, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 'profesional_maestria37-218 Compromiso: 1 -  75', 100, 0, NULL, 1, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'profesional_maestria37-218 Compromiso: 2 -  76', 100, 0, NULL, 1, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'profesional_maestria39-219 Compromiso: 0 -  77', 100, 0, NULL, 1, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'profesional_maestria39-219 Compromiso: 1 -  78', 100, 0, NULL, 1, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'profesional_maestria39-219 Compromiso: 2 -  79', 100, 0, NULL, 1, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'profesional_maestria39-219 Compromiso: 3 -  80', 100, 0, NULL, 1, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'profesional_maestria39-219 Compromiso: 4 -  81', 100, 0, NULL, 1, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'profesional_maestria41-220 Compromiso: 0 -  82', 100, 0, NULL, 1, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'profesional_maestria41-220 Compromiso: 1 -  83', 100, 0, NULL, 1, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'profesional_maestria41-220 Compromiso: 2 -  84', 100, 0, NULL, 1, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'profesional_doctorado43-221 Compromiso: 0 -  85', 100, 0, NULL, 1, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'profesional_doctorado43-221 Compromiso: 1 -  86', 100, 0, NULL, 1, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'profesional_doctorado43-221 Compromiso: 2 -  87', 100, 0, NULL, 1, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'profesional_doctorado43-221 Compromiso: 3 -  88', 100, 0, NULL, 1, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'profesional_doctorado43-221 Compromiso: 4 -  89', 100, 0, NULL, 1, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'profesional_doctorado46-222 Compromiso: 0 -  90', 100, 0, NULL, 1, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'profesional_doctorado46-222 Compromiso: 1 -  91', 100, 0, NULL, 1, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 'profesional_doctorado46-222 Compromiso: 2 -  92', 100, 0, NULL, 1, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 'profesional_doctorado46-222 Compromiso: 3 -  93', 100, 0, NULL, 1, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'profesional_doctorado46-222 Compromiso: 4 -  94', 100, 0, NULL, 1, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'profesional_doctorado49-223 Compromiso: 0 -  95', 100, 0, NULL, 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'profesional_doctorado49-223 Compromiso: 1 -  96', 100, 0, NULL, 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'profesional_doctorado49-223 Compromiso: 2 -  97', 100, 0, NULL, 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 'profesional_doctorado49-223 Compromiso: 3 -  98', 100, 0, NULL, 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 'profesional_doctorado49-223 Compromiso: 4 -  99', 100, 0, NULL, 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 'profesional_doctorado49-223 Compromiso: 5 -  100', 100, 0, NULL, 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 'profesional_doctorado52-224 Compromiso: 0 -  101', 100, 0, NULL, 1, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 'profesional_doctorado52-224 Compromiso: 1 -  102', 100, 0, NULL, 1, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'profesional_doctorado52-224 Compromiso: 2 -  103', 100, 0, NULL, 1, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 'profesional_doctorado55-225 Compromiso: 0 -  104', 100, 0, NULL, 1, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'profesional_doctorado55-225 Compromiso: 1 -  105', 100, 0, NULL, 1, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 'profesional_doctorado55-225 Compromiso: 2 -  106', 100, 0, NULL, 1, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 'profesional_doctorado55-225 Compromiso: 3 -  107', 100, 0, NULL, 1, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `evidencias`
--

CREATE TABLE `evidencias` (
  `id` int(10) unsigned NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percentual_weight` int(11) NOT NULL,
  `file_id` text COLLATE utf8_unicode_ci NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `user_who_evaluates` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `compromiso_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fileentries`
--

CREATE TABLE `fileentries` (
  `id` int(10) unsigned NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filesize` bigint(20) NOT NULL,
  `mime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `metas`
--

CREATE TABLE `metas` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `proyecto_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=314 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `metas`
--

INSERT INTO `metas` (`id`, `name`, `description`, `active`, `proyecto_id`, `created_at`, `updated_at`) VALUES
(1, 'economicop0-0m0-0', 'economicop0-0 Meta: 0 -  0', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'economicop0-0m1-1', 'economicop0-0 Meta: 1 -  1', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'economicop0-0m2-2', 'economicop0-0 Meta: 2 -  2', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'economicop1-1m0-3', 'economicop1-1 Meta: 0 -  3', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'economicop1-1m1-4', 'economicop1-1 Meta: 1 -  4', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'economicop1-1m2-5', 'economicop1-1 Meta: 2 -  5', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'economicop1-1m3-6', 'economicop1-1 Meta: 3 -  6', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'economicop2-2m0-7', 'economicop2-2 Meta: 0 -  7', 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'economicop2-2m1-8', 'economicop2-2 Meta: 1 -  8', 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'economicop2-2m2-9', 'economicop2-2 Meta: 2 -  9', 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'economicop2-2m3-10', 'economicop2-2 Meta: 3 -  10', 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'economicop3-3m0-11', 'economicop3-3 Meta: 0 -  11', 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'economicop3-3m1-12', 'economicop3-3 Meta: 1 -  12', 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'economicop3-3m2-13', 'economicop3-3 Meta: 2 -  13', 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'economicop4-4m0-14', 'economicop4-4 Meta: 0 -  14', 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'economicop4-4m1-15', 'economicop4-4 Meta: 1 -  15', 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'economicop4-4m2-16', 'economicop4-4 Meta: 2 -  16', 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'economicop4-4m3-17', 'economicop4-4 Meta: 3 -  17', 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'socialp0-5m0-18', 'socialp0-5 Meta: 0 -  18', 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'socialp0-5m1-19', 'socialp0-5 Meta: 1 -  19', 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'socialp0-5m2-20', 'socialp0-5 Meta: 2 -  20', 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'socialp0-5m3-21', 'socialp0-5 Meta: 3 -  21', 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'socialp0-5m4-22', 'socialp0-5 Meta: 4 -  22', 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'socialp1-6m0-23', 'socialp1-6 Meta: 0 -  23', 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'socialp1-6m1-24', 'socialp1-6 Meta: 1 -  24', 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'socialp1-6m2-25', 'socialp1-6 Meta: 2 -  25', 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'socialp1-6m3-26', 'socialp1-6 Meta: 3 -  26', 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'socialp1-6m4-27', 'socialp1-6 Meta: 4 -  27', 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'socialp2-7m0-28', 'socialp2-7 Meta: 0 -  28', 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'socialp2-7m1-29', 'socialp2-7 Meta: 1 -  29', 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'socialp2-7m2-30', 'socialp2-7 Meta: 2 -  30', 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'socialp2-7m3-31', 'socialp2-7 Meta: 3 -  31', 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'socialp3-8m0-32', 'socialp3-8 Meta: 0 -  32', 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'socialp3-8m1-33', 'socialp3-8 Meta: 1 -  33', 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'socialp3-8m2-34', 'socialp3-8 Meta: 2 -  34', 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'educacionp0-9m0-35', 'educacionp0-9 Meta: 0 -  35', 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'educacionp0-9m1-36', 'educacionp0-9 Meta: 1 -  36', 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'educacionp0-9m2-37', 'educacionp0-9 Meta: 2 -  37', 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'educacionp0-9m3-38', 'educacionp0-9 Meta: 3 -  38', 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'educacionp0-9m4-39', 'educacionp0-9 Meta: 4 -  39', 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'educacionp1-10m0-40', 'educacionp1-10 Meta: 0 -  40', 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'educacionp1-10m1-41', 'educacionp1-10 Meta: 1 -  41', 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'educacionp1-10m2-42', 'educacionp1-10 Meta: 2 -  42', 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'educacionp1-10m3-43', 'educacionp1-10 Meta: 3 -  43', 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'educacionp1-10m4-44', 'educacionp1-10 Meta: 4 -  44', 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'educacionp2-11m0-45', 'educacionp2-11 Meta: 0 -  45', 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'educacionp2-11m1-46', 'educacionp2-11 Meta: 1 -  46', 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'educacionp2-11m2-47', 'educacionp2-11 Meta: 2 -  47', 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'educacionp2-11m3-48', 'educacionp2-11 Meta: 3 -  48', 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'educacionp2-11m4-49', 'educacionp2-11 Meta: 4 -  49', 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'educacionp2-11m5-50', 'educacionp2-11 Meta: 5 -  50', 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'educacionp3-12m0-51', 'educacionp3-12 Meta: 0 -  51', 1, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'educacionp3-12m1-52', 'educacionp3-12 Meta: 1 -  52', 1, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'educacionp3-12m2-53', 'educacionp3-12 Meta: 2 -  53', 1, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'educacionp3-12m3-54', 'educacionp3-12 Meta: 3 -  54', 1, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'educacionp3-12m4-55', 'educacionp3-12 Meta: 4 -  55', 1, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'gobiernop0-13m0-56', 'gobiernop0-13 Meta: 0 -  56', 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'gobiernop0-13m1-57', 'gobiernop0-13 Meta: 1 -  57', 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'gobiernop0-13m2-58', 'gobiernop0-13 Meta: 2 -  58', 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'gobiernop0-13m3-59', 'gobiernop0-13 Meta: 3 -  59', 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'gobiernop0-13m4-60', 'gobiernop0-13 Meta: 4 -  60', 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'gobiernop0-13m5-61', 'gobiernop0-13 Meta: 5 -  61', 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'gobiernop1-14m0-62', 'gobiernop1-14 Meta: 0 -  62', 1, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'gobiernop1-14m1-63', 'gobiernop1-14 Meta: 1 -  63', 1, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'gobiernop1-14m2-64', 'gobiernop1-14 Meta: 2 -  64', 1, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'gobiernop1-14m3-65', 'gobiernop1-14 Meta: 3 -  65', 1, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'gobiernop1-14m4-66', 'gobiernop1-14 Meta: 4 -  66', 1, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'gobiernop2-15m0-67', 'gobiernop2-15 Meta: 0 -  67', 1, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'gobiernop2-15m1-68', 'gobiernop2-15 Meta: 1 -  68', 1, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 'gobiernop2-15m2-69', 'gobiernop2-15 Meta: 2 -  69', 1, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'gobiernop2-15m3-70', 'gobiernop2-15 Meta: 3 -  70', 1, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'gobiernop2-15m4-71', 'gobiernop2-15 Meta: 4 -  71', 1, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'gobiernop2-15m5-72', 'gobiernop2-15 Meta: 5 -  72', 1, 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'gobiernop3-16m0-73', 'gobiernop3-16 Meta: 0 -  73', 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'gobiernop3-16m1-74', 'gobiernop3-16 Meta: 1 -  74', 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 'gobiernop3-16m2-75', 'gobiernop3-16 Meta: 2 -  75', 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'gobiernop3-16m3-76', 'gobiernop3-16 Meta: 3 -  76', 1, 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'infraestructurap0-17m0-77', 'infraestructurap0-17 Meta: 0 -  77', 1, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'infraestructurap0-17m1-78', 'infraestructurap0-17 Meta: 1 -  78', 1, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'infraestructurap0-17m2-79', 'infraestructurap0-17 Meta: 2 -  79', 1, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'infraestructurap1-18m0-80', 'infraestructurap1-18 Meta: 0 -  80', 1, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'infraestructurap1-18m1-81', 'infraestructurap1-18 Meta: 1 -  81', 1, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'infraestructurap1-18m2-82', 'infraestructurap1-18 Meta: 2 -  82', 1, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'infraestructurap1-18m3-83', 'infraestructurap1-18 Meta: 3 -  83', 1, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'infraestructurap2-19m0-84', 'infraestructurap2-19 Meta: 0 -  84', 1, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'infraestructurap2-19m1-85', 'infraestructurap2-19 Meta: 1 -  85', 1, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'infraestructurap2-19m2-86', 'infraestructurap2-19 Meta: 2 -  86', 1, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'infraestructurap2-19m3-87', 'infraestructurap2-19 Meta: 3 -  87', 1, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'infraestructurap2-19m4-88', 'infraestructurap2-19 Meta: 4 -  88', 1, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'infraestructurap3-20m0-89', 'infraestructurap3-20 Meta: 0 -  89', 1, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'infraestructurap3-20m1-90', 'infraestructurap3-20 Meta: 1 -  90', 1, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'infraestructurap3-20m2-91', 'infraestructurap3-20 Meta: 2 -  91', 1, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 'infraestructurap3-20m3-92', 'infraestructurap3-20 Meta: 3 -  92', 1, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 'infraestructurap3-20m4-93', 'infraestructurap3-20 Meta: 4 -  93', 1, 21, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'infraestructurap4-21m0-94', 'infraestructurap4-21 Meta: 0 -  94', 1, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'infraestructurap4-21m1-95', 'infraestructurap4-21 Meta: 1 -  95', 1, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'infraestructurap4-21m2-96', 'infraestructurap4-21 Meta: 2 -  96', 1, 22, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'infraestructurap5-22m0-97', 'infraestructurap5-22 Meta: 0 -  97', 1, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 'infraestructurap5-22m1-98', 'infraestructurap5-22 Meta: 1 -  98', 1, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 'infraestructurap5-22m2-99', 'infraestructurap5-22 Meta: 2 -  99', 1, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 'infraestructurap5-22m3-100', 'infraestructurap5-22 Meta: 3 -  100', 1, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 'saludp0-23m0-101', 'saludp0-23 Meta: 0 -  101', 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 'saludp0-23m1-102', 'saludp0-23 Meta: 1 -  102', 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'saludp0-23m2-103', 'saludp0-23 Meta: 2 -  103', 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 'saludp0-23m3-104', 'saludp0-23 Meta: 3 -  104', 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'saludp0-23m4-105', 'saludp0-23 Meta: 4 -  105', 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 'saludp0-23m5-106', 'saludp0-23 Meta: 5 -  106', 1, 24, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 'saludp1-24m0-107', 'saludp1-24 Meta: 0 -  107', 1, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 'saludp1-24m1-108', 'saludp1-24 Meta: 1 -  108', 1, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 'saludp1-24m2-109', 'saludp1-24 Meta: 2 -  109', 1, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 'saludp1-24m3-110', 'saludp1-24 Meta: 3 -  110', 1, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 'saludp1-24m4-111', 'saludp1-24 Meta: 4 -  111', 1, 25, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 'saludp2-25m0-112', 'saludp2-25 Meta: 0 -  112', 1, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 'saludp2-25m1-113', 'saludp2-25 Meta: 1 -  113', 1, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 'saludp2-25m2-114', 'saludp2-25 Meta: 2 -  114', 1, 26, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 'saludp3-26m0-115', 'saludp3-26 Meta: 0 -  115', 1, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 'saludp3-26m1-116', 'saludp3-26 Meta: 1 -  116', 1, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 'saludp3-26m2-117', 'saludp3-26 Meta: 2 -  117', 1, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 'saludp3-26m3-118', 'saludp3-26 Meta: 3 -  118', 1, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 'saludp3-26m4-119', 'saludp3-26 Meta: 4 -  119', 1, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 'saludp4-27m0-120', 'saludp4-27 Meta: 0 -  120', 1, 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 'saludp4-27m1-121', 'saludp4-27 Meta: 1 -  121', 1, 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 'saludp4-27m2-122', 'saludp4-27 Meta: 2 -  122', 1, 28, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 'ticsp0-28m0-123', 'ticsp0-28 Meta: 0 -  123', 1, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 'ticsp0-28m1-124', 'ticsp0-28 Meta: 1 -  124', 1, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 'ticsp0-28m2-125', 'ticsp0-28 Meta: 2 -  125', 1, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 'ticsp0-28m3-126', 'ticsp0-28 Meta: 3 -  126', 1, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 'ticsp0-28m4-127', 'ticsp0-28 Meta: 4 -  127', 1, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 'ticsp0-28m5-128', 'ticsp0-28 Meta: 5 -  128', 1, 29, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 'ticsp1-29m0-129', 'ticsp1-29 Meta: 0 -  129', 1, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 'ticsp1-29m1-130', 'ticsp1-29 Meta: 1 -  130', 1, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 'ticsp1-29m2-131', 'ticsp1-29 Meta: 2 -  131', 1, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 'ticsp1-29m3-132', 'ticsp1-29 Meta: 3 -  132', 1, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 'ticsp1-29m4-133', 'ticsp1-29 Meta: 4 -  133', 1, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 'ticsp1-29m5-134', 'ticsp1-29 Meta: 5 -  134', 1, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 'ticsp2-30m0-135', 'ticsp2-30 Meta: 0 -  135', 1, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 'ticsp2-30m1-136', 'ticsp2-30 Meta: 1 -  136', 1, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 'ticsp2-30m2-137', 'ticsp2-30 Meta: 2 -  137', 1, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 'ticsp2-30m3-138', 'ticsp2-30 Meta: 3 -  138', 1, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 'ticsp2-30m4-139', 'ticsp2-30 Meta: 4 -  139', 1, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 'ticsp3-31m0-140', 'ticsp3-31 Meta: 0 -  140', 1, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 'ticsp3-31m1-141', 'ticsp3-31 Meta: 1 -  141', 1, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 'ticsp3-31m2-142', 'ticsp3-31 Meta: 2 -  142', 1, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 'ticsp3-31m3-143', 'ticsp3-31 Meta: 3 -  143', 1, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 'ticsp3-31m4-144', 'ticsp3-31 Meta: 4 -  144', 1, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, 'ticsp3-31m5-145', 'ticsp3-31 Meta: 5 -  145', 1, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 'transitop0-32m0-146', 'transitop0-32 Meta: 0 -  146', 1, 33, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 'transitop0-32m1-147', 'transitop0-32 Meta: 1 -  147', 1, 33, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, 'transitop0-32m2-148', 'transitop0-32 Meta: 2 -  148', 1, 33, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 'transitop1-33m0-149', 'transitop1-33 Meta: 0 -  149', 1, 34, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 'transitop1-33m1-150', 'transitop1-33 Meta: 1 -  150', 1, 34, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 'transitop1-33m2-151', 'transitop1-33 Meta: 2 -  151', 1, 34, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 'transitop2-34m0-152', 'transitop2-34 Meta: 0 -  152', 1, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 'transitop2-34m1-153', 'transitop2-34 Meta: 1 -  153', 1, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 'transitop2-34m2-154', 'transitop2-34 Meta: 2 -  154', 1, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 'transitop3-35m0-155', 'transitop3-35 Meta: 0 -  155', 1, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 'transitop3-35m1-156', 'transitop3-35 Meta: 1 -  156', 1, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 'transitop3-35m2-157', 'transitop3-35 Meta: 2 -  157', 1, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 'transitop3-35m3-158', 'transitop3-35 Meta: 3 -  158', 1, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(160, 'transitop3-35m4-159', 'transitop3-35 Meta: 4 -  159', 1, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(161, 'transitop4-36m0-160', 'transitop4-36 Meta: 0 -  160', 1, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(162, 'transitop4-36m1-161', 'transitop4-36 Meta: 1 -  161', 1, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(163, 'transitop4-36m2-162', 'transitop4-36 Meta: 2 -  162', 1, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(164, 'transitop4-36m3-163', 'transitop4-36 Meta: 3 -  163', 1, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(165, 'transitop4-36m4-164', 'transitop4-36 Meta: 4 -  164', 1, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, 'bienesp0-37m0-165', 'bienesp0-37 Meta: 0 -  165', 1, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, 'bienesp0-37m1-166', 'bienesp0-37 Meta: 1 -  166', 1, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, 'bienesp0-37m2-167', 'bienesp0-37 Meta: 2 -  167', 1, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, 'bienesp0-37m3-168', 'bienesp0-37 Meta: 3 -  168', 1, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, 'bienesp0-37m4-169', 'bienesp0-37 Meta: 4 -  169', 1, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, 'bienesp0-37m5-170', 'bienesp0-37 Meta: 5 -  170', 1, 38, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(172, 'bienesp1-38m0-171', 'bienesp1-38 Meta: 0 -  171', 1, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, 'bienesp1-38m1-172', 'bienesp1-38 Meta: 1 -  172', 1, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(174, 'bienesp1-38m2-173', 'bienesp1-38 Meta: 2 -  173', 1, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(175, 'bienesp1-38m3-174', 'bienesp1-38 Meta: 3 -  174', 1, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(176, 'bienesp1-38m4-175', 'bienesp1-38 Meta: 4 -  175', 1, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(177, 'bienesp1-38m5-176', 'bienesp1-38 Meta: 5 -  176', 1, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(178, 'bienesp2-39m0-177', 'bienesp2-39 Meta: 0 -  177', 1, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(179, 'bienesp2-39m1-178', 'bienesp2-39 Meta: 1 -  178', 1, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(180, 'bienesp2-39m2-179', 'bienesp2-39 Meta: 2 -  179', 1, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(181, 'bienesp2-39m3-180', 'bienesp2-39 Meta: 3 -  180', 1, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(182, 'bienesp2-39m4-181', 'bienesp2-39 Meta: 4 -  181', 1, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(183, 'bienesp2-39m5-182', 'bienesp2-39 Meta: 5 -  182', 1, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, 'bienesp3-40m0-183', 'bienesp3-40 Meta: 0 -  183', 1, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, 'bienesp3-40m1-184', 'bienesp3-40 Meta: 1 -  184', 1, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, 'bienesp3-40m2-185', 'bienesp3-40 Meta: 2 -  185', 1, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, 'bienesp3-40m3-186', 'bienesp3-40 Meta: 3 -  186', 1, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(188, 'bienesp4-41m0-187', 'bienesp4-41 Meta: 0 -  187', 1, 42, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, 'bienesp4-41m1-188', 'bienesp4-41 Meta: 1 -  188', 1, 42, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, 'bienesp4-41m2-189', 'bienesp4-41 Meta: 2 -  189', 1, 42, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(191, 'bienesp4-41m3-190', 'bienesp4-41 Meta: 3 -  190', 1, 42, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(192, 'bienesp5-42m0-191', 'bienesp5-42 Meta: 0 -  191', 1, 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(193, 'bienesp5-42m1-192', 'bienesp5-42 Meta: 1 -  192', 1, 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(194, 'bienesp5-42m2-193', 'bienesp5-42 Meta: 2 -  193', 1, 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(195, 'bienesp5-42m3-194', 'bienesp5-42 Meta: 3 -  194', 1, 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(196, 'bienesp5-42m4-195', 'bienesp5-42 Meta: 4 -  195', 1, 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(197, 'controlp0-43m0-196', 'controlp0-43 Meta: 0 -  196', 1, 44, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(198, 'controlp0-43m1-197', 'controlp0-43 Meta: 1 -  197', 1, 44, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(199, 'controlp0-43m2-198', 'controlp0-43 Meta: 2 -  198', 1, 44, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(200, 'controlp1-44m0-199', 'controlp1-44 Meta: 0 -  199', 1, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(201, 'controlp1-44m1-200', 'controlp1-44 Meta: 1 -  200', 1, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(202, 'controlp1-44m2-201', 'controlp1-44 Meta: 2 -  201', 1, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(203, 'controlp1-44m3-202', 'controlp1-44 Meta: 3 -  202', 1, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(204, 'controlp2-45m0-203', 'controlp2-45 Meta: 0 -  203', 1, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(205, 'controlp2-45m1-204', 'controlp2-45 Meta: 1 -  204', 1, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(206, 'controlp2-45m2-205', 'controlp2-45 Meta: 2 -  205', 1, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(207, 'controlp3-46m0-206', 'controlp3-46 Meta: 0 -  206', 1, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, 'controlp3-46m1-207', 'controlp3-46 Meta: 1 -  207', 1, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(209, 'controlp3-46m2-208', 'controlp3-46 Meta: 2 -  208', 1, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(210, 'controlp3-46m3-209', 'controlp3-46 Meta: 3 -  209', 1, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(211, 'controlp3-46m4-210', 'controlp3-46 Meta: 4 -  210', 1, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(212, 'controlp3-46m5-211', 'controlp3-46 Meta: 5 -  211', 1, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(213, 'controlp4-47m0-212', 'controlp4-47 Meta: 0 -  212', 1, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(214, 'controlp4-47m1-213', 'controlp4-47 Meta: 1 -  213', 1, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(215, 'controlp4-47m2-214', 'controlp4-47 Meta: 2 -  214', 1, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(216, 'controlp4-47m3-215', 'controlp4-47 Meta: 3 -  215', 1, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(217, 'controlp4-47m4-216', 'controlp4-47 Meta: 4 -  216', 1, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(218, 'controlp4-47m5-217', 'controlp4-47 Meta: 5 -  217', 1, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(219, 'dafip0-48m0-218', 'dafip0-48 Meta: 0 -  218', 1, 49, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(220, 'dafip0-48m1-219', 'dafip0-48 Meta: 1 -  219', 1, 49, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(221, 'dafip0-48m2-220', 'dafip0-48 Meta: 2 -  220', 1, 49, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(222, 'dafip0-48m3-221', 'dafip0-48 Meta: 3 -  221', 1, 49, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(223, 'dafip1-49m0-222', 'dafip1-49 Meta: 0 -  222', 1, 50, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(224, 'dafip1-49m1-223', 'dafip1-49 Meta: 1 -  223', 1, 50, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(225, 'dafip1-49m2-224', 'dafip1-49 Meta: 2 -  224', 1, 50, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(226, 'dafip2-50m0-225', 'dafip2-50 Meta: 0 -  225', 1, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(227, 'dafip2-50m1-226', 'dafip2-50 Meta: 1 -  226', 1, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(228, 'dafip2-50m2-227', 'dafip2-50 Meta: 2 -  227', 1, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(229, 'dafip2-50m3-228', 'dafip2-50 Meta: 3 -  228', 1, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(230, 'dafip2-50m4-229', 'dafip2-50 Meta: 4 -  229', 1, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(231, 'dafip3-51m0-230', 'dafip3-51 Meta: 0 -  230', 1, 52, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(232, 'dafip3-51m1-231', 'dafip3-51 Meta: 1 -  231', 1, 52, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(233, 'dafip3-51m2-232', 'dafip3-51 Meta: 2 -  232', 1, 52, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(234, 'dafip3-51m3-233', 'dafip3-51 Meta: 3 -  233', 1, 52, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(235, 'dafip3-51m4-234', 'dafip3-51 Meta: 4 -  234', 1, 52, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(236, 'dafip3-51m5-235', 'dafip3-51 Meta: 5 -  235', 1, 52, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(237, 'dafip4-52m0-236', 'dafip4-52 Meta: 0 -  236', 1, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(238, 'dafip4-52m1-237', 'dafip4-52 Meta: 1 -  237', 1, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(239, 'dafip4-52m2-238', 'dafip4-52 Meta: 2 -  238', 1, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(240, 'dafip4-52m3-239', 'dafip4-52 Meta: 3 -  239', 1, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(241, 'haciendap0-53m0-240', 'haciendap0-53 Meta: 0 -  240', 1, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(242, 'haciendap0-53m1-241', 'haciendap0-53 Meta: 1 -  241', 1, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(243, 'haciendap0-53m2-242', 'haciendap0-53 Meta: 2 -  242', 1, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(244, 'haciendap0-53m3-243', 'haciendap0-53 Meta: 3 -  243', 1, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(245, 'haciendap0-53m4-244', 'haciendap0-53 Meta: 4 -  244', 1, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(246, 'haciendap0-53m5-245', 'haciendap0-53 Meta: 5 -  245', 1, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, 'haciendap1-54m0-246', 'haciendap1-54 Meta: 0 -  246', 1, 55, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, 'haciendap1-54m1-247', 'haciendap1-54 Meta: 1 -  247', 1, 55, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, 'haciendap1-54m2-248', 'haciendap1-54 Meta: 2 -  248', 1, 55, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, 'haciendap1-54m3-249', 'haciendap1-54 Meta: 3 -  249', 1, 55, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, 'haciendap1-54m4-250', 'haciendap1-54 Meta: 4 -  250', 1, 55, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, 'haciendap1-54m5-251', 'haciendap1-54 Meta: 5 -  251', 1, 55, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(253, 'haciendap2-55m0-252', 'haciendap2-55 Meta: 0 -  252', 1, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, 'haciendap2-55m1-253', 'haciendap2-55 Meta: 1 -  253', 1, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, 'haciendap2-55m2-254', 'haciendap2-55 Meta: 2 -  254', 1, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(256, 'haciendap2-55m3-255', 'haciendap2-55 Meta: 3 -  255', 1, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, 'haciendap2-55m4-256', 'haciendap2-55 Meta: 4 -  256', 1, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, 'haciendap2-55m5-257', 'haciendap2-55 Meta: 5 -  257', 1, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, 'haciendap3-56m0-258', 'haciendap3-56 Meta: 0 -  258', 1, 57, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, 'haciendap3-56m1-259', 'haciendap3-56 Meta: 1 -  259', 1, 57, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, 'haciendap3-56m2-260', 'haciendap3-56 Meta: 2 -  260', 1, 57, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, 'haciendap3-56m3-261', 'haciendap3-56 Meta: 3 -  261', 1, 57, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, 'haciendap3-56m4-262', 'haciendap3-56 Meta: 4 -  262', 1, 57, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, 'haciendap3-56m5-263', 'haciendap3-56 Meta: 5 -  263', 1, 57, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, 'haciendap4-57m0-264', 'haciendap4-57 Meta: 0 -  264', 1, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, 'haciendap4-57m1-265', 'haciendap4-57 Meta: 1 -  265', 1, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(267, 'haciendap4-57m2-266', 'haciendap4-57 Meta: 2 -  266', 1, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, 'haciendap4-57m3-267', 'haciendap4-57 Meta: 3 -  267', 1, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, 'haciendap4-57m4-268', 'haciendap4-57 Meta: 4 -  268', 1, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, 'haciendap5-58m0-269', 'haciendap5-58 Meta: 0 -  269', 1, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, 'haciendap5-58m1-270', 'haciendap5-58 Meta: 1 -  270', 1, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, 'haciendap5-58m2-271', 'haciendap5-58 Meta: 2 -  271', 1, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, 'juridicop0-59m0-272', 'juridicop0-59 Meta: 0 -  272', 1, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, 'juridicop0-59m1-273', 'juridicop0-59 Meta: 1 -  273', 1, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, 'juridicop0-59m2-274', 'juridicop0-59 Meta: 2 -  274', 1, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, 'juridicop0-59m3-275', 'juridicop0-59 Meta: 3 -  275', 1, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, 'juridicop0-59m4-276', 'juridicop0-59 Meta: 4 -  276', 1, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, 'juridicop0-59m5-277', 'juridicop0-59 Meta: 5 -  277', 1, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, 'juridicop1-60m0-278', 'juridicop1-60 Meta: 0 -  278', 1, 61, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, 'juridicop1-60m1-279', 'juridicop1-60 Meta: 1 -  279', 1, 61, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, 'juridicop1-60m2-280', 'juridicop1-60 Meta: 2 -  280', 1, 61, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, 'juridicop1-60m3-281', 'juridicop1-60 Meta: 3 -  281', 1, 61, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, 'juridicop2-61m0-282', 'juridicop2-61 Meta: 0 -  282', 1, 62, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, 'juridicop2-61m1-283', 'juridicop2-61 Meta: 1 -  283', 1, 62, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(285, 'juridicop2-61m2-284', 'juridicop2-61 Meta: 2 -  284', 1, 62, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(286, 'juridicop2-61m3-285', 'juridicop2-61 Meta: 3 -  285', 1, 62, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(287, 'juridicop2-61m4-286', 'juridicop2-61 Meta: 4 -  286', 1, 62, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(288, 'planeacionp0-62m0-287', 'planeacionp0-62 Meta: 0 -  287', 1, 63, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(289, 'planeacionp0-62m1-288', 'planeacionp0-62 Meta: 1 -  288', 1, 63, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(290, 'planeacionp0-62m2-289', 'planeacionp0-62 Meta: 2 -  289', 1, 63, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(291, 'planeacionp0-62m3-290', 'planeacionp0-62 Meta: 3 -  290', 1, 63, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(292, 'planeacionp0-62m4-291', 'planeacionp0-62 Meta: 4 -  291', 1, 63, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(293, 'planeacionp0-62m5-292', 'planeacionp0-62 Meta: 5 -  292', 1, 63, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(294, 'planeacionp1-63m0-293', 'planeacionp1-63 Meta: 0 -  293', 1, 64, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(295, 'planeacionp1-63m1-294', 'planeacionp1-63 Meta: 1 -  294', 1, 64, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(296, 'planeacionp1-63m2-295', 'planeacionp1-63 Meta: 2 -  295', 1, 64, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(297, 'planeacionp1-63m3-296', 'planeacionp1-63 Meta: 3 -  296', 1, 64, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(298, 'planeacionp1-63m4-297', 'planeacionp1-63 Meta: 4 -  297', 1, 64, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(299, 'planeacionp2-64m0-298', 'planeacionp2-64 Meta: 0 -  298', 1, 65, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(300, 'planeacionp2-64m1-299', 'planeacionp2-64 Meta: 1 -  299', 1, 65, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(301, 'planeacionp2-64m2-300', 'planeacionp2-64 Meta: 2 -  300', 1, 65, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(302, 'planeacionp2-64m3-301', 'planeacionp2-64 Meta: 3 -  301', 1, 65, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(303, 'planeacionp2-64m4-302', 'planeacionp2-64 Meta: 4 -  302', 1, 65, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(304, 'planeacionp2-64m5-303', 'planeacionp2-64 Meta: 5 -  303', 1, 65, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(305, 'planeacionp3-65m0-304', 'planeacionp3-65 Meta: 0 -  304', 1, 66, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(306, 'planeacionp3-65m1-305', 'planeacionp3-65 Meta: 1 -  305', 1, 66, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(307, 'planeacionp3-65m2-306', 'planeacionp3-65 Meta: 2 -  306', 1, 66, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(308, 'planeacionp3-65m3-307', 'planeacionp3-65 Meta: 3 -  307', 1, 66, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(309, 'planeacionp3-65m4-308', 'planeacionp3-65 Meta: 4 -  308', 1, 66, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(310, 'planeacionp3-65m5-309', 'planeacionp3-65 Meta: 5 -  309', 1, 66, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(311, 'planeacionp4-66m0-310', 'planeacionp4-66 Meta: 0 -  310', 1, 67, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(312, 'planeacionp4-66m1-311', 'planeacionp4-66 Meta: 1 -  311', 1, 67, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(313, 'planeacionp4-66m2-312', 'planeacionp4-66 Meta: 2 -  312', 1, 67, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_09_14_132439_create_permissions_table', 1),
('2015_09_14_160309_create_permission_user_table', 1),
('2015_09_14_161220_create_roles_table', 1),
('2015_09_14_172044_create_profiles_table', 1),
('2015_09_15_111658_create_role_user_table', 1),
('2015_09_15_112026_create_permission_role_table', 1),
('2015_09_15_113140_create_attributes_table', 1),
('2015_09_15_113852_create_attribute_profile_table', 1),
('2015_11_25_113347_create_permission_route_table', 1),
('2015_11_28_172201_create_areas_table', 1),
('2015_11_28_172202_create_proyectos_table', 1),
('2015_11_28_172203_create_metas_table', 1),
('2015_11_29_113402_create_nivel_jerarquico_table', 1),
('2015_11_29_114345_create_cargos_table', 1),
('2015_11_29_114651_create_compromisos_table', 1),
('2015_11_29_114652_create_evidencias_table', 1),
('2015_11_30_212652_create_fileentries_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nivel_jerarquico`
--

CREATE TABLE `nivel_jerarquico` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nivel_jerarquico`
--

INSERT INTO `nivel_jerarquico` (`id`, `name`, `description`, `active`, `created_at`, `updated_at`) VALUES
(1, 'profesional', 'Profesional', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'tecnico', 'Técnico', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'asistencial', 'Asistencial', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(2, 2, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(3, 3, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(4, 4, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(5, 5, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(6, 6, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(7, 7, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(8, 8, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(9, 9, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(10, 10, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(11, 11, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(12, 12, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(13, 13, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(14, 14, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(15, 15, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(16, 16, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(17, 17, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(18, 18, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(19, 19, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(20, 20, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(21, 4, 4, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(22, 5, 4, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(23, 9, 4, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(24, 10, 4, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(25, 11, 4, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(26, 15, 4, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(27, 16, 4, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(28, 17, 4, '2015-12-01 05:46:06', '2015-12-01 05:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `permission_route`
--

CREATE TABLE `permission_route` (
  `id` int(10) unsigned NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_visible` tinyint(1) NOT NULL DEFAULT '1',
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permission_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_route`
--

INSERT INTO `permission_route` (`id`, `slug`, `route`, `menu_visible`, `description`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 'area.add', 'areaadd', 1, 'Adicionar Área', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'area.edit', 'areaedit', 1, 'Editar Área', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'area.delete', 'areadelete', 1, 'Eliminar Área', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'area.show', 'areashow', 1, 'Ver Área', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'area.showall', 'areashowall', 1, 'Ver Todas las Áreas', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'proyecto.add', 'proyectoadd', 1, 'Adicionar Proyecto', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'proyecto.edit', 'proyectoedit', 1, 'Editar Proyecto', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'proyecto.delete', 'proyectodelete', 1, 'Eliminar Proyecto', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'proyecto.showbyarea', 'proyectoshowbyarea', 1, 'Ver Proyectos por Área', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'proyecto.showall', 'proyectoshowall', 1, 'Ver Todos los Proyectos', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'proyecto.show', 'proyectoshow', 1, 'Ver Proyecto', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'meta.add', 'metaadd', 1, 'Adicionar Meta', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'meta.edit', 'metaedit', 1, 'Editar Meta', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'meta.delete', 'metadelete', 1, 'Eliminar Meta', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'meta.show', 'metashow', 1, 'Ver Meta', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'meta.showallbyproyecto', 'metashowallbyproyecto', 1, 'Ver Metas por Proyecto', 16, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'meta.showall', 'metashowall', 1, 'Ver Todas las Metas', 17, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'perfil.update', 'perfilupdate', 1, 'Editar Información Perfil', 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'role.add', 'roleadd', 1, 'Asignar Rol a Usuario', 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'role.delete', 'roledelete', 1, 'Remover Rol a Usuario', 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) unsigned NOT NULL,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `model`, `created_at`, `updated_at`) VALUES
(1, 'Adicionar Área', 'area.add', 'Permiso de Adicionar Área', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Editar Área', 'area.edit', 'Permiso de Editar Área', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Eliminar Área', 'area.delete', 'Permiso de Eliminar Área (soft)', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Ver Área', 'area.show', 'Permiso de Ver Área (Información)', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Ver Todas las Áreas', 'area.showall', 'Permiso de Ver Todas las Áreas', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Adicionar Proyecto', 'proyecto.add', 'Permiso de Adicionar Proyecto', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Editar Proyecto', 'proyecto.edit', 'Permiso de Editar Proyecto', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Eliminar Proyecto', 'proyecto.delete', 'Permiso de Eliminar Proyecto', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Ver Proyectos por Área', 'proyecto.showbyarea', 'Permiso de Ver Proyectos por Área', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Ver Todos los Proyectos', 'proyecto.showall', 'Permiso Ver Todos los Proyectos', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Ver Proyecto', 'proyecto.show', 'Permiso de Ver Proyecto', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Adicionar Meta', 'meta.add', 'Permiso de Adicionar Meta', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Editar Meta', 'meta.edit', 'Permiso de Editar Meta', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Eliminar Meta', 'meta.delete', 'Permiso de Eliminar Meta (soft)', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Ver Meta', 'meta.show', 'Permiso de Ver Meta', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Ver Metas por Proyecto', 'meta.showallbyproyecto', 'Permiso de Ver Metas por Proyecto', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Ver Todas las Metas', 'meta.showall', 'Permiso de Ver Todas Metas', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'Editar Información Perfil', 'perfil.update', 'Permiso de Editar Información Perfil', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'Asignar Rol a Usuario', 'role.add', 'Permiso de Asignar Rol a Usuario', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'Remover Rol a Usuario', 'role.delete', 'Permiso de Remover Rol a Usuario', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `document_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `gender` enum('M','F') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'M',
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `name`, `lastname`, `document_type`, `document_number`, `birthday`, `address`, `gender`, `phone`, `active`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 'Sin Apellido', 'CC', '000000000000000', '2015-12-01', 'Sin dirección', 'M', '(+57)(6) 7000000', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Otro Administrador', 'Sin Apellido', 'CC', '000000000000001', '2015-12-01', 'Sin dirección', 'M', '(+57)(6) 7000000', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Usuario Evaluador', 'Sin Apellido', 'CC', '000000000000002', '2015-12-01', 'Sin dirección', 'M', '(+57)(6) 7000000', 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Usuario Funcionario', 'Sin Apellido', 'CC', '000000000000003', '2015-12-01', 'Sin dirección', 'M', '(+57)(6) 7000000', 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `proyectos`
--

CREATE TABLE `proyectos` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `area_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `proyectos`
--

INSERT INTO `proyectos` (`id`, `name`, `description`, `active`, `area_id`, `created_at`, `updated_at`) VALUES
(1, 'economicop0-0', 'Área: economico Proyecto: 0 -  0', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'economicop1-1', 'Área: economico Proyecto: 1 -  1', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'economicop2-2', 'Área: economico Proyecto: 2 -  2', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'economicop3-3', 'Área: economico Proyecto: 3 -  3', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'economicop4-4', 'Área: economico Proyecto: 4 -  4', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'socialp0-5', 'Área: social Proyecto: 0 -  5', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'socialp1-6', 'Área: social Proyecto: 1 -  6', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'socialp2-7', 'Área: social Proyecto: 2 -  7', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'socialp3-8', 'Área: social Proyecto: 3 -  8', 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'educacionp0-9', 'Área: educacion Proyecto: 0 -  9', 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'educacionp1-10', 'Área: educacion Proyecto: 1 -  10', 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'educacionp2-11', 'Área: educacion Proyecto: 2 -  11', 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'educacionp3-12', 'Área: educacion Proyecto: 3 -  12', 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'gobiernop0-13', 'Área: gobierno Proyecto: 0 -  13', 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'gobiernop1-14', 'Área: gobierno Proyecto: 1 -  14', 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'gobiernop2-15', 'Área: gobierno Proyecto: 2 -  15', 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'gobiernop3-16', 'Área: gobierno Proyecto: 3 -  16', 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'infraestructurap0-17', 'Área: infraestructura Proyecto: 0 -  17', 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'infraestructurap1-18', 'Área: infraestructura Proyecto: 1 -  18', 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'infraestructurap2-19', 'Área: infraestructura Proyecto: 2 -  19', 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'infraestructurap3-20', 'Área: infraestructura Proyecto: 3 -  20', 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'infraestructurap4-21', 'Área: infraestructura Proyecto: 4 -  21', 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'infraestructurap5-22', 'Área: infraestructura Proyecto: 5 -  22', 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'saludp0-23', 'Área: salud Proyecto: 0 -  23', 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'saludp1-24', 'Área: salud Proyecto: 1 -  24', 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'saludp2-25', 'Área: salud Proyecto: 2 -  25', 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'saludp3-26', 'Área: salud Proyecto: 3 -  26', 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'saludp4-27', 'Área: salud Proyecto: 4 -  27', 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'ticsp0-28', 'Área: tics Proyecto: 0 -  28', 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'ticsp1-29', 'Área: tics Proyecto: 1 -  29', 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'ticsp2-30', 'Área: tics Proyecto: 2 -  30', 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'ticsp3-31', 'Área: tics Proyecto: 3 -  31', 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'transitop0-32', 'Área: transito Proyecto: 0 -  32', 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'transitop1-33', 'Área: transito Proyecto: 1 -  33', 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'transitop2-34', 'Área: transito Proyecto: 2 -  34', 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'transitop3-35', 'Área: transito Proyecto: 3 -  35', 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'transitop4-36', 'Área: transito Proyecto: 4 -  36', 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'bienesp0-37', 'Área: bienes Proyecto: 0 -  37', 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'bienesp1-38', 'Área: bienes Proyecto: 1 -  38', 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'bienesp2-39', 'Área: bienes Proyecto: 2 -  39', 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'bienesp3-40', 'Área: bienes Proyecto: 3 -  40', 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'bienesp4-41', 'Área: bienes Proyecto: 4 -  41', 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'bienesp5-42', 'Área: bienes Proyecto: 5 -  42', 1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'controlp0-43', 'Área: control Proyecto: 0 -  43', 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'controlp1-44', 'Área: control Proyecto: 1 -  44', 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'controlp2-45', 'Área: control Proyecto: 2 -  45', 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'controlp3-46', 'Área: control Proyecto: 3 -  46', 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'controlp4-47', 'Área: control Proyecto: 4 -  47', 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'dafip0-48', 'Área: dafi Proyecto: 0 -  48', 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'dafip1-49', 'Área: dafi Proyecto: 1 -  49', 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 'dafip2-50', 'Área: dafi Proyecto: 2 -  50', 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 'dafip3-51', 'Área: dafi Proyecto: 3 -  51', 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 'dafip4-52', 'Área: dafi Proyecto: 4 -  52', 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 'haciendap0-53', 'Área: hacienda Proyecto: 0 -  53', 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 'haciendap1-54', 'Área: hacienda Proyecto: 1 -  54', 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 'haciendap2-55', 'Área: hacienda Proyecto: 2 -  55', 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 'haciendap3-56', 'Área: hacienda Proyecto: 3 -  56', 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 'haciendap4-57', 'Área: hacienda Proyecto: 4 -  57', 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 'haciendap5-58', 'Área: hacienda Proyecto: 5 -  58', 1, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 'juridicop0-59', 'Área: juridico Proyecto: 0 -  59', 1, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 'juridicop1-60', 'Área: juridico Proyecto: 1 -  60', 1, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'juridicop2-61', 'Área: juridico Proyecto: 2 -  61', 1, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'planeacionp0-62', 'Área: planeacion Proyecto: 0 -  62', 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 'planeacionp1-63', 'Área: planeacion Proyecto: 1 -  63', 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 'planeacionp2-64', 'Área: planeacion Proyecto: 2 -  64', 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'planeacionp3-65', 'Área: planeacion Proyecto: 3 -  65', 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'planeacionp4-66', 'Área: planeacion Proyecto: 4 -  66', 1, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(2, 4, 1, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(3, 3, 4, '2015-12-01 05:46:06', '2015-12-01 05:46:06'),
(4, 4, 6, '2015-12-01 05:46:06', '2015-12-01 05:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `level`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'Administrador aplicación', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Evaluador', 'evaluador', 'Rol de Evaluador - EDL', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Funcionario', 'funcionario', 'Rol funcionario Evaluado', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Asistente', 'asistente', 'Rol de Asistente evaluador/admin', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'deibioctavio', 'deibioctavio@gmail.com', '$2y$10$Lf.MMdI1zAVit8rqIxQL2.Qcgeae5tDblvE23MYCRPUO7Xd8FFKo6', 'hWNCksD9soRNTSUatY2uesGCTjbChaSuclDC7pVRnITD6zJIUcuaKnsIMPCb', '0000-00-00 00:00:00', '2015-12-16 21:51:03'),
(2, 'administradoredl', 'administradoredl@armenia.gov.co', '$2y$10$SG3Zr8L.WOSZvttH.4QrSOcwOIAklIy.VqQ6lvI3mH31nfIzJc1jC', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'evaluadoredl', 'evaluadoredl@armenia.gov.co', '$2y$10$YgWpSqzb6Nu88RiZmlSFF.U2EBRcJI2EWsO8Klltj8O5J7ljYhtxm', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'funcionarioedl', 'funcionarioedl@armenia.gov.co', '$2y$10$9vvGORKp3RF6B4PGn1xMCO.zHnO/YsMakpz.7m63wAP1zxbdtp.jO', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'user1448948765', 'user1448948765@armenia.gov.co', '$2y$10$bKND/Ksasmqg4fiLGEq0h.Ls/E6kaRnzPe3Q8H19bNDtEfLdbAQq6', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'asistenteedl', 'asistenteedl@armenia.gov.co', '$2y$10$S/.xHOpSSZbfxMeM4AHxtegLQ8LNVr90jXbAzJWauusJdliKaMaBG', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_profile`
--
ALTER TABLE `attribute_profile`
  ADD KEY `attribute_profile_attribute_id_index` (`attribute_id`),
  ADD KEY `attribute_profile_profile_id_index` (`profile_id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cargos_nivel_jerarquico_id_index` (`nivel_jerarquico_id`);

--
-- Indexes for table `compromisos`
--
ALTER TABLE `compromisos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `compromisos_cargo_id_index` (`cargo_id`);

--
-- Indexes for table `evidencias`
--
ALTER TABLE `evidencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `evidencias_user_who_evaluates_foreign` (`user_who_evaluates`),
  ADD KEY `evidencias_compromiso_id_index` (`compromiso_id`);

--
-- Indexes for table `fileentries`
--
ALTER TABLE `fileentries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metas`
--
ALTER TABLE `metas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `metas_proyecto_id_index` (`proyecto_id`);

--
-- Indexes for table `nivel_jerarquico`
--
ALTER TABLE `nivel_jerarquico`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `permission_route`
--
ALTER TABLE `permission_route`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_route_id_index` (`id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_index` (`permission_id`),
  ADD KEY `permission_user_user_id_index` (`user_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profiles_user_id_unique` (`user_id`);

--
-- Indexes for table `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proyectos_area_id_index` (`area_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `compromisos`
--
ALTER TABLE `compromisos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `evidencias`
--
ALTER TABLE `evidencias`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fileentries`
--
ALTER TABLE `fileentries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `metas`
--
ALTER TABLE `metas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=314;
--
-- AUTO_INCREMENT for table `nivel_jerarquico`
--
ALTER TABLE `nivel_jerarquico`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `permission_route`
--
ALTER TABLE `permission_route`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `attribute_profile`
--
ALTER TABLE `attribute_profile`
  ADD CONSTRAINT `attribute_profile_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attribute_profile_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cargos`
--
ALTER TABLE `cargos`
  ADD CONSTRAINT `cargos_nivel_jerarquico_id_foreign` FOREIGN KEY (`nivel_jerarquico_id`) REFERENCES `nivel_jerarquico` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `compromisos`
--
ALTER TABLE `compromisos`
  ADD CONSTRAINT `compromisos_cargo_id_foreign` FOREIGN KEY (`cargo_id`) REFERENCES `cargos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `evidencias`
--
ALTER TABLE `evidencias`
  ADD CONSTRAINT `evidencias_compromiso_id_foreign` FOREIGN KEY (`compromiso_id`) REFERENCES `compromisos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `evidencias_user_who_evaluates_foreign` FOREIGN KEY (`user_who_evaluates`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `metas`
--
ALTER TABLE `metas`
  ADD CONSTRAINT `metas_proyecto_id_foreign` FOREIGN KEY (`proyecto_id`) REFERENCES `proyectos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `proyectos`
--
ALTER TABLE `proyectos`
  ADD CONSTRAINT `proyectos_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
