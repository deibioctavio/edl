<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionEvaluacion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'configuracion_evaluaciones';

    protected $fillable = [
    							'n_compromisos_laborales',
    							'n_competencias_comportamentales',
    							'valor_compromisos_laborales',
    							'valor_competencias_comportamentales',
    							'vigencia'
    						];
}
