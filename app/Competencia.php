<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Competencia extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'competencias';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','active','vigencia'];


    public function conductasAsociadas() {
        
       return $this->hasMany('App\ConductaAsociada');
    }
}
