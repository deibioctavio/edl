<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompromisosComportamentales extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'compromisos_comportamentales';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    						'evaluacion_id',
    						'competencia_id',
    						'conducta_asociada_id',
    						'procentaje_cumpimiento_pactado',
    						'procentaje_cumpimiento_obtenido',
    						'periodo',
    						'comments',
    						'active'
    					];

    public function evaluacion() {
        
       return $this->belongsTo('App\Evaluacion');
    }

    public function evidencias() {
        
       return $this->hasMany('App\Evidencia');
    }
}
