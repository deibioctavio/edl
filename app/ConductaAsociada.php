<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ConductaAsociada extends Model
{
        /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'conductas_asociadas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','active','vigencia'];

    public function competencia() {
        
       return $this->belongsTo('App\Competencia');
    }
}
