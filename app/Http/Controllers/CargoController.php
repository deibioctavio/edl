<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\NivelJerarquico;
use App\Cargo;

class CargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nivelCargo = array();
        $niveles = NivelJerarquico::get();
        foreach ($niveles as $n) {
            $cargos = Cargo::where('nivel_jerarquico_id',$n->id)->get();
            $nivelCargo[$n->id]['cargos'] = $cargos->toArray();
        }
        return \View::make('cargo.showall',array('nivelCargo' => $nivelCargo,'niveles'=>$niveles));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $niveles = NivelJerarquico::get();
        $cargos = Cargo::get();
        return \View::make('cargo.add',array('niveles'=>$niveles,'cargos'=>$cargos));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cargos = new Cargo();
        
        $cargos->name = $request->input('name');

        $cargos->description = $request->input('description');

        $cargos->nivel_jerarquico_id = $request->input('nivel_jerarquico_id');

        $cargos->vigencia = $request->input('vigencia');
        
        if(!$cargos->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('cargoadd');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('cargoshowall');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $niveles = NivelJerarquico::get();
        $cargos = Cargo::where('id',$id)->first();
        return \View::make('cargo.edit',array('niveles'=>$niveles,'cargos'=>$cargos));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');

        $cargos = Cargo::find($id);

        $nivel_jerarquico_id = $request->input('nivel_jerarquico_id');
        $cargos->name = $request->input('name');
        $cargos->description = $request->input('description');
        $cargos->nivel_jerarquico_id = $nivel_jerarquico_id;
        
        if(!$cargos->save()){
            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();
        }else{
            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cargos = Cargo::find($id);
        $cargos->delete();
        return redirect('cargoshowall');
    }
    public function cargoslistjson($nivelId){
        $cargos = Cargo::where('nivel_jerarquico_id',$nivelId)->get()->toArray();
        echo json_encode($cargos);
    }
}
