<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Config\Repository;
use App\Area;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $areas = Area::get();
        return \View::make('areas.showall',array('areas' => $areas));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return \View::make('areas.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $area = new Area();
        $area->name = $request->input('name');
        $area->description = $request->input('description');
        $area->vigencia = $request->input('vigencia');

        
        if(!$area->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('areaadd');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('areashowall');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id = NULL)
    {
        if($id = NULL){
            return redirect('areashoall');
        }
    }

    public function edit($id)
    {
        $area = Area::where('id',$id)->first();
        return \View::make('areas.edit',array('area' => $area));
    }  

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $area = Area::find($id);
        $area->name = $request->input('name');
        $area->description = $request->input('description');
        $area->active = $request->input('active');
        
        if(!$area->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        $area = Area::find($id);
        $area->delete();
        return redirect('areashowall');
    }

    public function getByInitialCharacter($letter)
    {
        $areas = Area::where('name', 'like', $letter."%")->get();
        return \View::make('areas.getinitial',array('areas' => $areas));
    }
}
