<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cargo;
use App\NivelJerarquico;
use App\Compromiso;

class CompromisoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nivelCargoCompromiso = array();
        $niveles = NivelJerarquico::get();
        $cargos = Cargo::get();
        $compromiso = Compromiso::get();

        $i = 0;

        foreach ($niveles as $n) {

             $cargosTmp = Cargo::where('nivel_jerarquico_id',$n->id)->get();

             foreach ($cargosTmp as $c) {
  

                $compromisoTmp = Compromiso::where('cargo_id',$c->id)->get();

                foreach ($compromisoTmp as $m) {
                    $nivelCargoCompromiso[$i]['nivel_jerarquico_id'] = $n->id;
                    $nivelCargoCompromiso[$i]['nivel_name'] = $n->name;
                    $nivelCargoCompromiso[$i]['cargo_id'] = $c->id;
                    $nivelCargoCompromiso[$i]['cargo_name'] = $c->name;
                    $nivelCargoCompromiso[$i]['compromiso_id'] = $m->id;
                    $nivelCargoCompromiso[$i]['compromiso_description'] = $m->description;
                    $nivelCargoCompromiso[$i]['compromiso_vigencia'] = $m->vigencia;
                    $i++;
                }
            }
        }

        /*echo "<pre>";
        print_r($areaProyectoMeta);
        echo "</pre>";*/

        return \View::make('compromiso.showall',
                                array(
                                        'niveles' => $niveles,
                                        'cargos' => $cargos,
                                        'compromiso' => $compromiso,
                                        'nivelCargoCompromiso' => $nivelCargoCompromiso,
                                        'isReportView'=>true
                                    )
                            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $niveles = NivelJerarquico::get();
        $cargos= Cargo::get();

        return \View::make('compromiso.add',array('niveles'=>$niveles,'cargos'=>$cargos));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $compromiso = new Compromiso();
        $compromiso->description = $request->input('description');
        $compromiso->cargo_id = $request->input('cargo_id');
        $compromiso->vigencia = $request->input('vigencia');
        
        if(!$compromiso->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('compromisoadd');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('compromisoshowall');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $compromiso = Compromiso::where('id',$id)->first();
        $cargo = $compromiso->cargo;
        $nivel = $cargo->nivelJerarquico;

        $cargos = $nivel->cargos;
        $niveles = NivelJerarquico::get();

        return \View::make('compromiso.edit',array('nivel'=>$nivel,'niveles'=>$niveles,'cargo'=>$cargo,'cargos'=>$cargos,'compromiso'=>$compromiso));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');

        $compromiso = Compromiso::find($id);

        $cargo_id = $request->input('cargo_id');
        $compromiso->description = $request->input('description');
        $compromiso->cargo_id = $cargo_id;
        
        if(!$compromiso->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $compromiso = Compromiso::find($id);
        $compromiso->delete();
        return redirect('compromisoshowall');
    }
}