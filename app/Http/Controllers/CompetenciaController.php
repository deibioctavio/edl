<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Config\Repository;
use App\Competencia;

class CompetenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $competencias = Competencia::get();
        return \View::make('competencias.showall',array('competencias' => $competencias));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \View::make('competencias.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $competencia = new Competencia();
        $competencia->description = $request->input('description');
        $competencia->vigencia = $request->input('vigencia');
        
        if(!$competencia->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('competenciashowall');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('competenciashowall');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id = NULL){
            return redirect('competenciashowall');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $competencia = Competencia::where('id',$id)->first();
        return \View::make('competencias.edit',array('competencia' => $competencia));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $competencia = Competencia::find($id);
        $competencia->name = $request->input('name');
        $competencia->description = $request->input('description');
        $competencia->active = $request->input('active');
        
        if(!$competencia->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $competencia = Competencia::find($id);
        $competencia->delete();
        return redirect('competenciashowall');
    }
}
