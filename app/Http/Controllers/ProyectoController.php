<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Area;
use App\Proyecto;
use App\Meta;

class ProyectoController extends Controller
{

    public function index()
    {
        $areaProyecto = array();
        $areas = Area::get();

        foreach ($areas as $a) {
            $proyectos = Proyecto::where('area_id',$a->id)->get();
            $areaProyecto[$a->id]['proyectos'] = $proyectos->toArray();
        }

        return \View::make('proyectos.showall',array('areaProyecto' => $areaProyecto,'areas'=>$areas));
    }


    public function create()
    {
        $areas = Area::get();
        return \View::make('proyectos.add',array('areas'=>$areas));
    }


    public function store(Request $request)
    {
        $proyecto = new Proyecto();
        $proyecto->name = $request->input('name');
        $proyecto->description = $request->input('description');
        $proyecto->area_id = $request->input('area_id');
        $proyecto->vigencia = $request->input('vigencia');
        
        if(!$proyecto->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('proyectoadd');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('proyectoshowall');
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $areas = Area::get();
        $proyecto = Proyecto::where('id',$id)->first();
        return \View::make('proyectos.edit',array('areas'=>$areas,'proyecto'=>$proyecto));
    }


    public function update(Request $request)
    {
        $id = $request->input('id');

        $proyecto = Proyecto::find($id);

        $area_id = $request->input('area_id');
        $proyecto->name = $request->input('name');
        $proyecto->description = $request->input('description');
        $proyecto->active = $request->input('active');
        $proyecto->area_id = $area_id;
        
        if(!$proyecto->save()){
            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            return redirect()->back();
        }else{
            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        $proyecto = Proyecto::find($id);
        $proyecto->delete();
        return redirect('proyectoshowall');
    }

    public function proyectoslistjson($areaId){
        $proyectos = Proyecto::where('area_id',$areaId)->get()->toArray();
        echo json_encode($proyectos);
    }

    public function getMetasByProyectoId($id){
        return Meta::where('proyecto_id',$id)->get();
    }

    public function getMetasByProyectoIdAjax($id){
        return response()->json($this->getMetasByProyectoId($id)); 
    }
}
