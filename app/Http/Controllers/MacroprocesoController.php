<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Macroproceso;

class MacroprocesoController extends Controller
{

    public function index()
    {
        $macroproceso = Macroproceso::get();
        return \View::make('macroproceso.showall',array('macroproceso' => $macroproceso));        
    }


    public function create()
    {
        return \View::make('macroproceso.add');
    }


    public function store(Request $request)
    {
        $macroproceso = new Macroproceso();
        $macroproceso->name = $request->input('name');
        $macroproceso->description = $request->input('description');
        $macroproceso->active = $request->input('active');
        $macroproceso->vigencia = $request->input('vigencia');
        $macroproceso->color = $request->input('color');
        
        if(!$macroproceso->save()){
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('macroprocesoadd');
        }else{
            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('macroprocesoshowall');
        }
    }

    public function show($id)
    {
        if($id = NULL){
            return redirect('macroprocesoshowall');
        }
    }


    public function edit($id)
    {
        $macroproceso = Macroproceso::where('id',$id)->first();
        return \View::make('macroproceso.edit',array('macroproceso' => $macroproceso));        
    }


    public function update(Request $request)
    {
        $id = $request->input('id');
        $macroproceso = Macroproceso::find($id);
        $macroproceso->name = $request->input('name');
        $macroproceso->description = $request->input('description');
        $macroproceso->active = $request->input('active');
        $macroproceso->vigencia = $request->input('vigencia');
        $macroproceso->color = $request->input('color');
        
        if(!$macroproceso->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        $macroproceso = Macroproceso::find($id);
        $macroproceso->delete();
        return redirect('macroprocesoshowall');
    }
}
