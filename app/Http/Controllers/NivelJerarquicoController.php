<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\NivelJerarquico;


class NivelJerarquicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $niveles = NivelJerarquico::get();
        return \View::make('niveljerarquico.showall',array('niveles' => $niveles));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return \View::make('niveljerarquico.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $niveljerarquico = new NivelJerarquico();
        $niveljerarquico->name = $request->input('name');
        $niveljerarquico->description = $request->input('description');
        $niveljerarquico->vigencia = $request->input('vigencia');

        if(!$niveljerarquico->save()){

            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('niveljerarquicoadd');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('niveljerarquicoshowall');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id = NULL){
            return redirect('niveljerarquicoshowall');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $niveljerarquico = NivelJerarquico::where('id',$id)->first();
        return \View::make('niveljerarquico.edit',array('niveljerarquico' => $niveljerarquico));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $niveljerarquico = NivelJerarquico::find($id);
        $niveljerarquico->name = $request->input('name');
        $niveljerarquico->description = $request->input('description');
        $niveljerarquico->active = $request->input('active');
        
        if(!$niveljerarquico->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $niveljerarquico = NivelJerarquico::find($id);
        $niveljerarquico->delete();
        return redirect('niveljerarquicoshowall');
    }
}
