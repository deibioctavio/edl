<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TipoEvaluacion;
use App\Evaluacion;
use App\Proyecto;
use App\Compromiso;
use Auth;
use DB;
use Log;
use App\CompromisosLaboralesEvaluacion;
use App\Evidencia;
use App\ConductaAsociada;
use App\ConfiguracionEvaluacion;
use App\CompromisosComportamentales;
use App\Competencias;

use Illuminate\Auth\AuthManager;

class EvaluacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //Shows Evaluations by evaluador
    public function index()
    {
        /*$evaluaciones = Evaluacion::where('usuario_evaluador1_id',Auth::user()->id)
                                    ->orWhere('usuario_evaluador2_id', Auth::user()->id)
                                    ->get();*/

        //$profile = Profile::where('document_number','like',"%{$id}%")->get();

        $evaluaciones = DB::table('evaluaciones')
            ->join('tipo_evaluaciones', 'evaluaciones.tipo_evaluacion_id', '=', 'tipo_evaluaciones.id')
            ->join('profiles', 'profiles.user_id', '=', 'evaluaciones.usuario_evaluado_id')
            ->where('evaluaciones.usuario_evaluador1_id',Auth::user()->id)
            ->orWhere('evaluaciones.usuario_evaluador2_id', Auth::user()->id)
            ->select('evaluaciones.*', 'tipo_evaluaciones.name as tipo_evaluacion', 'profiles.name as nombre','profiles.lastname as apellido')
            ->get();

        return \View::make('evaluaciones.showall',
                                array(
                                        'evaluaciones' => $evaluaciones,
                                        'isReportView' => true
                                    )
                            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $tiposEvaluaciones = TipoEvaluacion::get();
        $vigencias = DB::select('SELECT vigencia FROM configuracion_evaluaciones');
        //Log::error(print_r($vigencias,TRUE));
        return \View::make('evaluaciones.add',
                                                array(
                                                        'tiposEvaluaciones' => $tiposEvaluaciones,
                                                        'vigencias' => $vigencias,
                                                        'isReportView' => true
                                                    )
                                            );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $evaluacion = new Evaluacion();
        $evaluacion->tipo_evaluacion_id = $request->input('tipo_evaluacion_id');
        $evaluacion->vigencia = $request->input('vigencia');
        $evaluacion->dias_evaluados = $request->input('dias_evaluados');
        $evaluacion->porcentaje_evaluado = $request->input('porcentaje_evaluado');

        $evaluacion->usuario_evaluado_id = $request->input('usuario_evaluado_id');
        $evaluacion->usuario_evaluado_cargo_id = $request->input('usuario_evaluado_cargo_id');
        $evaluacion->usuario_evaluado_area_id = $request->input('usuario_evaluado_area_id');
        $evaluacion->usuario_evaluador1_id = $request->input('usuario_evaluador1_id');
        $evaluacion->usuario_evaluador2_id = $request->input('usuario_evaluador2_id');
        $evaluacion->usuario_diligencia_id = Auth::user()->id;
        $evaluacion->periodo_evaluado_inicio = $request->input('periodo_evaluado_inicio');
        $evaluacion->periodo_evaluado_fin = $request->input('periodo_evaluado_fin');
        $evaluacion->fecha_fijacion_compromiso = $request->input('fecha_fijacion_compromiso');
        $evaluacion->proposito = $request->input('proposito');

        if(!$evaluacion->save()){

            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('evaluacionadd');

        }else{

            $evaluacionId = $evaluacion->id;

            $configuracionEvaluacion = ConfiguracionEvaluacion::where('active',1)->where('vigencia',$evaluacion->vigencia)->first();

            $conductasAsociadas = ConductaAsociada::where('active',1)->where('vigencia',$evaluacion->vigencia)->get();

            $porcentajeIndividual = 0.00;

            if( count($conductasAsociadas) > 0 ){

                $porcentajeIndividual = $configuracionEvaluacion->valor_competencias_comportamentales/count($conductasAsociadas);
            }

            $insertArray = array();

            $created_at = date('Y-m-d H:i:s');

            foreach ($conductasAsociadas as $ca) {

                $rowArray = array(

                                    'evaluacion_id' => $evaluacionId,
                                    'competencia_id' => $ca->competencia_id,
                                    'conducta_id' => $ca->id,
                                    'procentaje_cumpimiento_pactado' => $porcentajeIndividual,
                                    'usuario_id_create' => $evaluacion->usuario_diligencia_id,
                                    'created_at' => $created_at,
                );

                $insertArray[] = $rowArray;
            }

            CompromisosComportamentales::insert($insertArray);

            return redirect('evaluaciones/showallbyevaluador');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //Shows all evaluations
    public function showall(){

    }

    public function compromiso($id){

        $evaluacion = DB::table('evaluaciones')
            ->join('tipo_evaluaciones', 'evaluaciones.tipo_evaluacion_id', '=', 'tipo_evaluaciones.id')
            ->join('profiles AS p', 'p.user_id', '=', 'evaluaciones.usuario_evaluado_id')
            ->join('profiles AS p1', 'p1.user_id', '=', 'evaluaciones.usuario_evaluador1_id')
            ->join('profiles AS p2', 'p2.user_id', '=', 'evaluaciones.usuario_evaluador2_id')
            ->join('configuracion_evaluaciones AS ce', 'ce.vigencia', '=', 'evaluaciones.vigencia')
            ->where('evaluaciones.id',$id)
            ->select(
                        'evaluaciones.*',
                        'tipo_evaluaciones.name as tipo_evaluacion',
                        'p.name as nombre','p.lastname as apellido','p.document_number','p.area_id','p.cargo_id',
                        'p1.name as e1nombre','p1.lastname as e1apellido','p1.document_number as e1document_number',
                        'p2.name as e2nombre','p2.lastname as e2apellido','p2.document_number as e2document_number',
                        'ce.n_compromisos_laborales','ce.n_competencias_comportamentales'
                        )
            ->first();

        $proyectos = Proyecto::where('area_id',$evaluacion->area_id)->get();
        $compromisos = Compromiso::where('cargo_id',$evaluacion->cargo_id)->get();

        return \View::make('evaluaciones.compromisosadd',
                                array(
                                        'evaluacion' => $evaluacion,
                                        'proyectos' => $proyectos,
                                        'compromisos' => $compromisos,
                                        'isReportView' => true
                                    )
                            );
    }

    public function compromisoStore(Request $request){

        /*echo "<pre>";
        print_r($request->all());
        echo "</pre>";*/

        $evaluacion_id = $request->input('evaluacion_id');
        $usuario_id_create = Auth::user()->id;

        $proyectos = $request->input('proyecto');
        $metas = $request->input('meta');
        $compromiso = $request->input('compromiso');
        $condicion_resultado = $request->input('condicion_resultado');
        $procentaje_cumpimiento_pactado = $request->input('procentaje_cumpimiento_pactado');

        $insertArray = array();

        for( $i = 0; $i < count($proyectos); $i++ ){

            $rowArray = array(
                                'evaluacion_id' => $evaluacion_id,
                                'usuario_id_create' => $usuario_id_create,
                                'proyecto_id' => $proyectos[$i],
                                'meta_id' => $metas[$i],
                                'compromiso_id' => $compromiso[$i],
                                'condicion_resultado' => $condicion_resultado[$i],
                                'procentaje_cumpimiento_pactado' => $procentaje_cumpimiento_pactado[$i],
                                'created_at' => date('Y-m-d H:i:s'),
            );

            $insertArray[] = $rowArray;
        }

        CompromisosLaboralesEvaluacion::insert($insertArray);

        return redirect('evaluaciones/showallbyevaluador');
    }

    public function getCompromisosLaboralesByEvaluacionId( $id ){

        return CompromisosLaboralesEvaluacion::where('evaluacion_id', $id)->get();
    }

    public function getCompromisosLaboralesByEvaluacionIdAjax( $id ){

        return response()->json($this->getCompromisosLaboralesByEvaluacionId($id));
    }

    public function showCompromisosByEvaluacionId( $id ){

        $evaluacion = DB::table('evaluaciones')
            ->join('tipo_evaluaciones', 'evaluaciones.tipo_evaluacion_id', '=', 'tipo_evaluaciones.id')
            ->join('profiles AS p', 'p.user_id', '=', 'evaluaciones.usuario_evaluado_id')
            ->join('profiles AS p1', 'p1.user_id', '=', 'evaluaciones.usuario_evaluador1_id')
            ->join('profiles AS p2', 'p2.user_id', '=', 'evaluaciones.usuario_evaluador2_id')
            ->join('configuracion_evaluaciones AS ce', 'ce.vigencia', '=', 'evaluaciones.vigencia')
            ->where('evaluaciones.id',$id)
            ->select(
                        'evaluaciones.*',
                        'tipo_evaluaciones.name as tipo_evaluacion',
                        'p.name as nombre','p.lastname as apellido','p.document_number','p.area_id','p.cargo_id',
                        'p1.name as e1nombre','p1.lastname as e1apellido','p1.document_number as e1document_number',
                        'p2.name as e2nombre','p2.lastname as e2apellido','p2.document_number as e2document_number',
                        'ce.n_compromisos_laborales','ce.n_competencias_comportamentales'
                        )
            ->first();

        $proyectos = Proyecto::where('area_id',$evaluacion->area_id)->get();
        $compromisos = Compromiso::where('cargo_id',$evaluacion->cargo_id)->get();

        //$compromisosLaborales = $this->getCompromisosLaboralesByEvaluacionId($id);

        $compromisosLaborales = DB::table('compromisos_laborales_evaluacion')
            ->join('proyectos', 'compromisos_laborales_evaluacion.proyecto_id', '=', 'proyectos.id')
            ->join('metas', 'compromisos_laborales_evaluacion.meta_id', '=', 'metas.id')
            ->join('compromisos', 'compromisos_laborales_evaluacion.compromiso_id', '=', 'compromisos.id')
            ->where('compromisos_laborales_evaluacion.evaluacion_id',$id)
            ->select(
                        'compromisos_laborales_evaluacion.*',
                        'proyectos.name as proyecto_name',
                        'metas.name as meta_name',
                        'metas.description as meta_description',
                        'compromisos.description as compromiso_name'
                    )->get();

        $evidenciacompromisosLaborales = array();

        foreach ($compromisosLaborales as $cL) {

             $evidenciacompromisosLaborales[$cL->id] = Evidencia::where('compromiso_id',$cL->id)
                                                        ->where('type','laboral')
                                                        ->get();
        }

        return \View::make('evaluaciones.compromisosshow',
                                array(
                                        'evaluacion' => $evaluacion,
                                        'proyectos' => $proyectos,
                                        'compromisos' => $compromisos,
                                        'compromisosLaborales' => $compromisosLaborales,
                                        'evidenciacompromisosLaborales' => $evidenciacompromisosLaborales,
                                        'isReportView' => true
                                    )
                            );
    }

    public function showCompetenciasByEvaluacionId( $id ){

        $evaluacion = DB::table('evaluaciones')
            ->join('tipo_evaluaciones', 'evaluaciones.tipo_evaluacion_id', '=', 'tipo_evaluaciones.id')
            ->join('profiles AS p', 'p.user_id', '=', 'evaluaciones.usuario_evaluado_id')
            ->join('profiles AS p1', 'p1.user_id', '=', 'evaluaciones.usuario_evaluador1_id')
            ->join('profiles AS p2', 'p2.user_id', '=', 'evaluaciones.usuario_evaluador2_id')
            ->join('configuracion_evaluaciones AS ce', 'ce.vigencia', '=', 'evaluaciones.vigencia')
            ->where('evaluaciones.id',$id)
            ->select(
                        'evaluaciones.*',
                        'tipo_evaluaciones.name as tipo_evaluacion',
                        'p.name as nombre','p.lastname as apellido','p.document_number','p.area_id','p.cargo_id',
                        'p1.name as e1nombre','p1.lastname as e1apellido','p1.document_number as e1document_number',
                        'p2.name as e2nombre','p2.lastname as e2apellido','p2.document_number as e2document_number',
                        'ce.n_compromisos_laborales','ce.n_competencias_comportamentales'
                        )
            ->first();

        $competenciasComportamentales = DB::table('competencias AS C')
            ->join('conductas_asociadas AS CA', 'C.id', '=', 'CA.competencia_id')
            //->join('conductas_asociadas', 'compromisos_comportamentales.conducta_id', '=', 'conductas_asociadas.id')
            //->where('compromisos_comportamentales.evaluacion_id',$id)
            ->orderBy('competencia_name','ASC')
            ->orderBy('conducta_nombre','ASC')
            ->select(
                        'C.name AS competencia_name',
                        'CA.name AS conducta_nombre',
                        'CA.id AS conducta_id'
                    )->get();

        return \View::make('evaluaciones.conductasadd',
                                array(
                                        'evaluacion' => $evaluacion,
                                        'competenciasComportamentales' => $competenciasComportamentales,
                                        'isReportView' => true
                                    )
                            );
    }

    public function compromisosEvidenciadefineByCompromisoId($id,$evaluacionId){

        $evaluacion = DB::table('evaluaciones')
            ->join('tipo_evaluaciones', 'evaluaciones.tipo_evaluacion_id', '=', 'tipo_evaluaciones.id')
            ->join('profiles AS p', 'p.user_id', '=', 'evaluaciones.usuario_evaluado_id')
            ->join('profiles AS p1', 'p1.user_id', '=', 'evaluaciones.usuario_evaluador1_id')
            ->join('profiles AS p2', 'p2.user_id', '=', 'evaluaciones.usuario_evaluador2_id')
            ->join('configuracion_evaluaciones AS ce', 'ce.vigencia', '=', 'evaluaciones.vigencia')
            ->where('evaluaciones.id',$evaluacionId)
            ->select(
                        'evaluaciones.*',
                        'tipo_evaluaciones.name as tipo_evaluacion',
                        'p.name as nombre','p.lastname as apellido','p.document_number','p.area_id','p.cargo_id',
                        'p1.name as e1nombre','p1.lastname as e1apellido','p1.document_number as e1document_number',
                        'p2.name as e2nombre','p2.lastname as e2apellido','p2.document_number as e2document_number',
                        'ce.n_compromisos_laborales','ce.n_competencias_comportamentales'
                        )
            ->first();

        $proyectos = Proyecto::where('area_id',$evaluacion->area_id)->get();
        $compromisos = Compromiso::where('cargo_id',$evaluacion->cargo_id)->get();

        $configuracionEvaluacion = ConfiguracionEvaluacion::where('vigencia',$evaluacion->vigencia)->where('active',1)->first();

        //$compromisosLaborales = $this->getCompromisosLaboralesByEvaluacionId($id);

        $compromisosLaborales = DB::table('compromisos_laborales_evaluacion')
            ->join('proyectos', 'compromisos_laborales_evaluacion.proyecto_id', '=', 'proyectos.id')
            ->join('metas', 'compromisos_laborales_evaluacion.meta_id', '=', 'metas.id')
            ->join('compromisos', 'compromisos_laborales_evaluacion.compromiso_id', '=', 'compromisos.id')
            ->where('compromisos_laborales_evaluacion.evaluacion_id',$evaluacionId)
            ->where('compromisos_laborales_evaluacion.id',$id)
            ->select(
                        'compromisos_laborales_evaluacion.*',
                        'proyectos.name as proyecto_name',
                        'metas.name as meta_name',
                        'metas.description as meta_description',
                        'compromisos.description as compromiso_name'
                    )->get();

        $evidenciacompromisosLaborales = array();

        foreach ($compromisosLaborales as $cL) {

             $evidenciacompromisosLaborales[$cL->id] = Evidencia::where('compromiso_id',$cL->id)
                                                        ->where('type','laboral')
                                                        ->get();
        }

        return \View::make('evaluaciones.compromisosevidenciaadd',
                                array(
                                        'evaluacion' => $evaluacion,
                                        'proyectos' => $proyectos,
                                        'compromisos' => $compromisos,
                                        'compromisosLaborales' => $compromisosLaborales,
                                        'configuracionEvaluacion' => $configuracionEvaluacion,
                                        'isReportView' => true
                                    )
                            );

    }

    public function compromisosEvidenciadefineByCompromisoIdStore(Request $request)
    {
        /*echo "<pre>";
        print_r($request->all());
        echo "</pre>";*/

        $evaluacion_id = $request->input('evaluacion_id');
        $create_user_id = Auth::user()->id;
        $compromiso_id = $request->input('compromiso_id');

        $description = $request->input('description');
        $percentual_weight = $request->input('percentual_weight');
        $comments = $request->input('comments');

        $insertArray = array();

        for( $i = 0; $i < count($description); $i++ ){

            $rowArray = array(

                                'description' => $description[$i],
                                'percentual_weight' => $percentual_weight[$i],
                                'comments' => $comments[$i],
                                'create_user_id' => $create_user_id,
                                'compromiso_id' => $compromiso_id,
                                'created_at' => date('Y-m-d H:i:s'),
            );

            $insertArray[] = $rowArray;
        }

        Evidencia::insert($insertArray);

        return redirect('/evaluaciones/showcompromisolaboralesbyevaluacion/'.$evaluacion_id);
    }

    public function conductasAdd(Request $request){
      //dd($request->all());
    }

}
