<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Area;
use App\Proyecto;
use App\Meta;

class MetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areaProyectoMeta = array();
        $areas = Area::get();
        $proyectos = Proyecto::get();
        $metas = Meta::get();

        $i = 0;

        foreach ($areas as $a) {

             $proyectoTmp = Proyecto::where('area_id',$a->id)->get();

             foreach ($proyectoTmp as $p) {
  

                $metasTmp = Meta::where('proyecto_id',$p->id)->get();

                foreach ($metasTmp as $m) {
                    $areaProyectoMeta[$i]['area_id'] = $a->id;
                    $areaProyectoMeta[$i]['area_name'] = $a->name;
                    $areaProyectoMeta[$i]['proyecto_id'] = $p->id;
                    $areaProyectoMeta[$i]['proyecto_name'] = $p->name;
                    $areaProyectoMeta[$i]['meta_id'] = $m->id;
                    $areaProyectoMeta[$i]['meta_name'] = $m->name;
                    $areaProyectoMeta[$i]['meta_description'] = $m->description;
                    $areaProyectoMeta[$i]['meta_vigencia'] = $m->vigencia;
                    $i++;
                }
            }
        }

        /*echo "<pre>";
        print_r($areaProyectoMeta);
        echo "</pre>";*/

        return \View::make('metas.showall',
                                array(
                                        'areas' => $areas,
                                        'proyectos' => $proyectos,
                                        'metas' => $metas,
                                        'areaProyectoMeta' => $areaProyectoMeta,
                                        'isReportView'=>true
                                    )
                            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $areas = Area::get();
        $proyectos = Proyecto::get();

        return \View::make('metas.add',array('areas'=>$areas,'proyectos'=>$proyectos));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $meta = new Meta();
        $meta->name = $request->input('name');
        $meta->description = $request->input('description');
        $meta->proyecto_id = $request->input('proyecto_id');
        $meta->vigencia = $request->input('vigencia');
        
        if(!$meta->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('metaadd');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('metashowall');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meta = Meta::where('id',$id)->first();
        $proyecto = $meta->proyecto;
        $area = $proyecto->area;

        $proyectos = $area->proyectos;
        $areas = Area::get();

        return \View::make('metas.edit',array('area'=>$area,'areas'=>$areas,'proyecto'=>$proyecto,'proyectos'=>$proyectos,'meta'=>$meta));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');

        $meta = Meta::find($id);

        $proyecto_id = $request->input('proyecto_id');
        $meta->name = $request->input('name');
        $meta->description = $request->input('description');
        $meta->active = $request->input('active');
        $meta->proyecto_id = $proyecto_id;
        
        if(!$meta->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meta = Meta::find($id);
        $meta->delete();
        return redirect('metashowall');
    }
}
