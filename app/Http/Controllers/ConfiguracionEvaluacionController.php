<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ConfiguracionEvaluacion;
use Log;
use DB;

class ConfiguracionEvaluacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configuraciones = ConfiguracionEvaluacion::get();
        return \View::make('configuracionevaluaciones.showall',
                            array(
                                    'configuraciones'=>$configuraciones)
                            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \View::make('configuracionevaluaciones.add',array());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $configuracion = new ConfiguracionEvaluacion();
        $configuracion ->n_compromisos_laborales = $request->input('n_compromisos_laborales');
        $configuracion ->n_competencias_comportamentales = $request->input('n_competencias_comportamentales');
        $configuracion ->valor_compromisos_laborales = $request->input('valor_compromisos_laborales');
        $configuracion ->valor_competencias_comportamentales = $request->input('valor_competencias_comportamentales');
        $configuracion ->vigencia = $request->input('vigencia');

        if(!$configuracion->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('configuracionevaluacionadd');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('configuracionevaluacionshowall');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id = NULL){
            return redirect('configuracionevaluacionshowall');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $configuracion = ConfiguracionEvaluacion::where('id',$id)->first();
        return \View::make('configuracionevaluaciones.edit',array('configuracion' => $configuracion));    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $configuracion = ConfiguracionEvaluacion::find($id);
        $configuracion ->n_compromisos_laborales = $request->input('n_compromisos_laborales');
        $configuracion ->valor_compromisos_laborales = $request->input('valor_compromisos_laborales');
        $configuracion ->n_competencias_comportamentales = $request->input('n_competencias_comportamentales');
        $configuracion ->valor_competencias_comportamentales = $request->input('valor_competencias_comportamentales');
        
        if(!$configuracion->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $configuracion = ConfiguracionEvaluacion::find($id);
        $configuracion->delete();
        return redirect('configuracionevaluacionshowall');
    }

    public function vigenciaExistAjax(Request $request){
        
        //Log::error(printf($request->input('vigencia'),TRUE));
        return response()->json('false');
    }

    public function vigenciaExist(Request $request){ 
        
        $configuracion = ConfiguracionEvaluacion::where('vigencia',$request->input('vigencia'))->first();
        //Log::error($configuracion==NULL?"ES NULO":"NO ES NULO");
        
        echo $configuracion==NULL?"true":"false";
    }

    public function getValidVigencias(){

        $sql = "";
        $sql .= "SELECT valor as vigencia FROM vigencias_referencias WHERE valor "; 
        $sql .= "NOT IN (SELECT vigencia FROM configuracion_evaluaciones)";

        $vigencias =  DB::select($sql);

        return response()->json($vigencias);
    }
}
