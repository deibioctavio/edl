<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Competencia;
use App\ConductaAsociada;

class ConductaAsociadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $competenciaConducta = array();
        $competencias = Competencia::get();
        

        foreach ($competencias as $c) {

            $conductas = ConductaAsociada::where('competencia_id',$c->id)->get();
            $competenciaConducta[$c->id]['conductas'] = $conductas->toArray();

        }

        return \View::make('conductas.showall',array('competenciaConducta' => $competenciaConducta,'competencias'=>$competencias,'isReportView'=>true));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $competencias = Competencia::get();
        return \View::make('conductas.add',array('competencias'=>$competencias));
    
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	
        $conductas = new ConductaAsociada();
        $conductas->name = $request->input('name');
        $conductas->description = $request->input('description');
        $conductas->competencia_id = $request->input('competencia_id');
        $conductas->vigencia = $request->input('vigencia');
        
        if(!$conductas->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('conductasadd');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('conductashowall');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $competencias = Competencia::get();
        $conductas = ConductaAsociada::where('id',$id)->first();
        return \View::make('conductas.edit',array('competencias'=>$competencias,'conducta'=>$conductas));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');

        $conductas = ConductaAsociada::find($id);

        $competencia_id = $request->input('competencia_id');
        $conductas->name = $request->input('name');
        $conductas->description = $request->input('description');
        $conductas->active = $request->input('active');
        $conductas->competencia_id = $competencia_id;
        
        if(!$conductas->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            //return Redirect::route('areaedit', array("id" => $id));
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $conducta = ConductaAsociada::find($id);
        $conducta->delete();
        return redirect('conductashowall');
    }
}
