<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Actividad;
use App\Proceso;

class ActividadController extends Controller
{


    public function index()
    {   
        $actividad = Actividad::get();
        $proceso = Proceso::get();
        return \View::make('actividad.showall',array('actividad' => $actividad,'proceso'=>$proceso));
    }


    public function create()
    {
        $proceso = Proceso::get();
        return \View::make('actividad.add',array('proceso'=>$proceso));
    }

    public function store(Request $request)
    {
        $actividad = new Actividad();
        $actividad->name = $request->input('name');
        $actividad->description = $request->input('description');
        $actividad->active = $request->input('active');
        $actividad->vigencia = $request->input('vigencia');
        $actividad->proceso_id = $request->input('proceso_id');

        
        if(!$actividad->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('actividadadd');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('actividadshowall');
        }
    }

    public function show($id)
    {
        if($id = NULL){
            return redirect('actividadshowall');
        }
    }

    public function edit($id)
    {
        $actividad = Actividad::where('id',$id)->first();
        $proceso = Proceso::get();
        return \View::make('actividad.edit',array('actividad' => $actividad,'proceso'=>$proceso));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        $actividad = Actividad::find($id);
        $actividad->name = $request->input('name');
        $actividad->description = $request->input('description');
        $actividad->active = $request->input('active');
        $actividad->vigencia = $request->input('vigencia');
        $actividad->proceso_id = $request->input('proceso_id');
        
        if(!$actividad->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        $actividad = Actividad::find($id);
        $actividad->delete();
        return redirect('actividadshowall');
    }
}
