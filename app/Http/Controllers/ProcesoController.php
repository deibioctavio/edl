<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Macroproceso;
use App\Proceso;
use App\Area;


class ProcesoController extends Controller
{

    public function index()
    {
        $macroproceso = Macroproceso::get();
        $area = Area::get();
        $proceso = Proceso::get();

        return \View::make('proceso.showall',array('proceso' => $proceso,'macroproceso'=>$macroproceso,'area'=>$area));        
    }


    public function create()
    {
        $area = Area::get();
        $macroproceso = Macroproceso::get();
        return \View::make('proceso.add',array('area' => $area,'macroproceso'=>$macroproceso));
    }


    public function store(Request $request)
    {
        $proceso = new Proceso();
        $proceso->name = $request->input('name');
        $proceso->identificator = $request->input('identificator');
        $proceso->description = $request->input('description');
        $proceso->active = $request->input('active');
        $proceso->vigencia = $request->input('vigencia');
        $proceso->area_id = $request->input('area_id');
        $proceso->macroproceso_id = $request->input('macroproceso_id');
        
        if(!$proceso->save()){
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('procesoadd');
        }else{
            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('procesoshowall');
        }
    }


    public function show($id)
    {
        if($id = NULL){
            return redirect('procesoshowall');
        }
    }


    public function edit($id)
    {
        $proceso = Proceso::where('id',$id)->first();
        return \View::make('proceso.edit',array('proceso' => $proceso)); 
    }

    public function update(Request $request)
    {
        $id = $request->input('id');

        $proceso = new Proceso();

        $proceso->name = $request->input('name');
        $proceso->identificador = $request->input('identificador');
        $proceso->description = $request->input('description');
        $proceso->active = $request->input('active');
        $proceso->vigencia = $request->input('vigencia');
        $proceso->area_id = $request->input('area_id');
        $proceso->macroproceso_id = $request->input('macroproceso_id');
        $proceso->color = $request->input('color');
        
        if(!$proceso->save()){
            $request->session()->flash('flash_error_message', 'Ocurrió un error actualizando el registro');
            return redirect()->back();
        }else{
            $request->session()->flash('flash_success_message', 'registro modificado correctamente');
            return redirect()->back();
        }
    }


    public function destroy($id)
    {
        $proceso = Proceso::find($id);
        $proceso->delete();
        return redirect('procesoshowall');
    }
}
