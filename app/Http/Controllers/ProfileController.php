<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Config\Repository;
use Bican\Roles\Models\Permission;
use Auth;
use App\User;
use App\Profile;
use App\Area;
use App\NIvelJerarquico;
use App\Cargo;
use DB;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $profile = Profile::where('user_id',Auth::user()->id)->first();
        
        if( count($profile) < 1){
            return redirect('profileadd');
        }else{
            return redirect('profileedit');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $areas = Area::get();
        $niveles = NIvelJerarquico::get();

        return \View::make('profiles.add',
                                array(
                                        'user' => Auth::user(),
                                        'areas' => $areas,
                                        'niveles' => $niveles
                                    )
                            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $profile = new Profile();
        //['name','lastname','document_type','document_number','birthday','address','gender','phone','cellphone']
        
        $profile->area_id = $request->input('area_id');
        $profile->cargo_id = $request->input('cargo_id');
        $profile->name = $request->input('name');
        $profile->lastname = $request->input('lastname');
        $profile->document_type = $request->input('document_type');
        $profile->document_number = $request->input('document_number');
        $profile->birthday = $request->input('birthday');
        $profile->address = $request->input('address');
        $profile->gender = $request->input('gender');
        $profile->phone = $request->input('phone');
        $profile->cellphone = $request->input('cellphone'); 
        $profile->user_id = Auth::user()->id;      
        
        if(!$profile->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('profileedit');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show()
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id = null)
    {
        $profile_id = ($id == null)?Auth::user()->id:$id;

        $profile = Profile::where('user_id',$profile_id)->first();
        $pid = (int)$profile->cargo_id;
        $areas = Area::get();
        $niveles = NIvelJerarquico::get();
        $tmp = Cargo::where('id',$pid)->first();
        $nivelSelected = $tmp->nivel_jerarquico_id;
        $cargos = Cargo::get();

        return \View::make('profiles.edit',
                                array(
                                        'user' => Auth::user(),
                                        'profile'=>$profile,
                                        'areas' => $areas,
                                        'niveles' => $niveles,
                                        'cargos' => $cargos,
                                        'selectedNivel' => $nivelSelected,
                                        'selectedCargo' =>  $pid
                                    )
                            );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {
        $profile = Profile::where('user_id',Auth::user()->id)->first();
        //['name','lastname','document_type','document_number','birthday','address','gender','phone','cellphone']
        
        $profile->area_id = $request->input('area_id');
        $profile->cargo_id = $request->input('cargo_id');
        $profile->name = $request->input('name');
        $profile->lastname = $request->input('lastname');
        $profile->document_type = $request->input('document_type');
        $profile->document_number = $request->input('document_number');
        $profile->birthday = $request->input('birthday');
        $profile->address = $request->input('address');
        $profile->gender = $request->input('gender');
        $profile->phone = $request->input('phone');
        $profile->cellphone = $request->input('cellphone');        
        
        if(!$profile->save()){
            
            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('profileedit');

        }else{

            $request->session()->flash('flash_success_message', 'registro adicionado correctamente');
            return redirect('profileedit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getFullProfileDataByUserId( Request $request){
        $id = $request->input('id');
        //$profile = Profile::where('document_number','like',"%{$id}%")->get();
 
        $profile = DB::table('profiles')
            ->join('cargos', 'profiles.cargo_id', '=', 'cargos.id')
            ->join('areas', 'profiles.area_id', '=', 'areas.id')
            ->where('profiles.document_number','like',"%{$id}%")
            ->select('profiles.*', 'cargos.name as cargo', 'areas.name as area')
            ->get();

        $area = array();
        $cargo = array();
        $data = array('profile'=>$profile);
        return response()->json($data);
    }
}
