<?php

//Default GET Calling

Route::get('/', function () {
   return redirect('home');
});
/*
//Numbers Only
Route::get('home/{id}', function($id) {
	return view("home")->with("user_id",$id);
})->where('id', '[0-9]+');
*/
//Home Route
/*
Route::get('home', function() {
	return view('home');
	//return "Welcome home";
});*/

//Home Route with Controller

Route::get('home', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);

//Route::get('profile/{fakeid}', ['as' => 'profile', 'uses' => 'ProfileController@show']);

Route::get('areaadd', ['as' => 'areaadd', 'uses' => 'AreaController@create','middleware' => 'role:admin']);
Route::post('areaadd', ['as' => 'areaadd', 'uses' => 'AreaController@store','middleware' => 'role:admin']);
Route::get('areaedit/{id}', ['as' => 'areaedit', 'uses' => 'AreaController@edit','middleware' => 'role:admin']);
Route::post('areaedit', ['as' => 'areaedit', 'uses' => 'AreaController@update','middleware' => 'role:admin']);
Route::get('areadelete/{id}', ['as' => 'areadelete', 'uses' => 'AreaController@destroy','middleware' => 'role:admin']);
Route::get('areashow/{id}', ['as' => 'areashow', 'uses' => 'AreaController@show']);
Route::get('areashowall', ['as' => 'areashowall', 'uses' => 'AreaController@index']);
Route::get('areagetinitial/{letter}', ['as' => 'areagetinitial', 'uses' => 'AreaController@getByInitialCharacter']);


Route::get('proyectoadd', ['as' => 'proyectoadd', 'uses' => 'ProyectoController@create','middleware' => 'role:admin']);
Route::post('proyectoadd', ['as' => 'proyectoadd', 'uses' => 'ProyectoController@store','middleware' => 'role:admin']);
Route::get('proyectoedit/{id}', ['as' => 'proyectoedit', 'uses' => 'ProyectoController@edit','middleware' => 'role:admin']);
Route::post('proyectoedit', ['as' => 'proyectoedit', 'uses' => 'ProyectoController@update','middleware' => 'role:admin']);
Route::get('proyectodelete/{id}', ['as' => 'proyectodelete', 'uses' => 'ProyectoController@destroy','middleware' => 'role:admin']);
Route::get('proyectoshowall', ['as' => 'proyectoshowall', 'uses' => 'ProyectoController@index']);
Route::get('proyectoshowbyarea', ['as' => 'proyectoshowbyarea', 'uses' => 'ProyectoController@index']);
Route::get('proyectoshow', ['as' => 'proyectoshow', 'uses' => 'ProyectoController@index']);
Route::get('proyectolistjson/{areaId}', ['as' => 'proyectolistjson', 'uses' => 'ProyectoController@proyectoslistjson']);

Route::get('metashowall', ['as' => 'metashowall', 'uses' => 'MetaController@index']);
Route::get('metaadd', ['as' => 'metaadd', 'uses' => 'MetaController@create','middleware' => 'role:admin']);
Route::post('metaadd', ['as' => 'metaadd', 'uses' => 'MetaController@store','middleware' => 'role:admin']);
Route::get('metaedit/{id}', ['as' => 'metaedit', 'uses' => 'MetaController@edit','middleware' => 'role:admin']);
Route::post('metaedit', ['as' => 'metaedit', 'uses' => 'MetaController@update','middleware' => 'role:admin']);
Route::get('metadelete/{id}', ['as' => 'metadelete', 'uses' => 'MetaController@destroy','middleware' => 'role:admin']);
Route::get('metashow', ['as' => 'metashow', 'uses' => 'MetaController@show']);
Route::get('metashowallbyproyecto', ['as' => 'metashowallbyproyecto', 'uses' => 'MetaController@show']);

Route::get('competenciaadd', ['as' => 'competenciaadd', 'uses' => 'CompetenciaController@create','middleware' => 'role:admin']);
Route::post('competenciaadd', ['as' => 'competenciaadd', 'uses' => 'CompetenciaController@store','middleware' => 'role:admin']);
Route::get('competenciaedit/{id}', ['as' => 'competenciaedit', 'uses' => 'CompetenciaController@edit','middleware' => 'role:admin']);
Route::post('competenciaedit', ['as' => 'competenciaedit', 'uses' => 'CompetenciaController@update','middleware' => 'role:admin']);
Route::get('competenciadelete/{id}', ['as' => 'competenciadelete', 'uses' => 'CompetenciaController@destroy','middleware' => 'role:admin']);
Route::get('competenciashowall', ['as' => 'competenciashowall', 'uses' => 'CompetenciaController@index']);
Route::get('competenciashow', ['as' => 'competenciasshow', 'uses' => 'CompetenciaController@index']);

Route::get('conductaadd', ['as' => 'conductaadd', 'uses' => 'ConductaAsociadaController@create','middleware' => 'role:admin']);
Route::post('conductaadd', ['as' => 'conductaadd', 'uses' => 'ConductaAsociadaController@store','middleware' => 'role:admin']);
Route::get('conductaedit/{id}', ['as' => 'conductaedit', 'uses' => 'ConductaAsociadaController@edit','middleware' => 'role:admin']);
Route::post('conductaedit', ['as' => 'conductaedit', 'uses' => 'ConductaAsociadaController@update','middleware' => 'role:admin']);
Route::get('conductadelete/{id}', ['as' => 'conductadelete', 'uses' => 'ConductaAsociadaController@destroy','middleware' => 'role:admin']);
Route::get('conductashowall', ['as' => 'conductashowall', 'uses' => 'ConductaAsociadaController@index']);
Route::get('conductashow', ['as' => 'conductashow', 'uses' => 'ConductaAsociadaController@index']);

Route::get('cargoadd', ['as' => 'cargoadd', 'uses' => 'CargoController@create','middleware' => 'role:admin']);
Route::post('cargoadd', ['as' => 'cargoadd', 'uses' => 'CargoController@store','middleware' => 'role:admin']);
Route::get('cargoedit/{id}', ['as' => 'cargoedit', 'uses' => 'CargoController@edit','middleware' => 'role:admin']);
Route::post('cargoedit', ['as' => 'cargoedit', 'uses' => 'CargoController@update','middleware' => 'role:admin']);
Route::get('cargodelete/{id}', ['as' => 'cargodelete', 'uses' => 'CargoController@destroy','middleware' => 'role:admin']);
Route::get('cargoshowall', ['as' => 'cargoshowall', 'uses' => 'CargoController@index']);
Route::get('cargoshow', ['as' => 'cargoshow', 'uses' => 'CargoController@index']);

Route::get('profile', ['as' => 'profile', 'uses' => 'ProfileController@index']);
Route::get('profileadd', ['as' => 'profileadd', 'uses' => 'ProfileController@create']);
Route::post('profileadd', ['as' => 'profileadd', 'uses' => 'ProfileController@store']);
Route::get('profileedit', ['as' => 'profileedit', 'uses' => 'ProfileController@edit']);
Route::post('profileedit', ['as' => 'profileedit', 'uses' => 'ProfileController@update']);
Route::get('profileshow', ['as' => 'profileshow', 'uses' => 'ProfileController@show']);
Route::post('getbyuserid', ['as' => 'getbyuserid', 'uses' => 'ProfileController@getFullProfileDataByUserId']);


Route::get('compromisoadd', ['as' => 'compromisoadd', 'uses' => 'CompromisoController@create','middleware' => 'role:admin']);
Route::post('compromisoadd', ['as' => 'compromisoadd', 'uses' => 'CompromisoController@store','middleware' => 'role:admin']);
Route::get('compromisoedit/{id}', ['as' => 'compromisoedit', 'uses' => 'CompromisoController@edit','middleware' => 'role:admin']);
Route::post('compromisoedit', ['as' => 'compromisoedit', 'uses' => 'CompromisoController@update','middleware' => 'role:admin']);
Route::get('compromisodelete/{id}', ['as' => 'compromisodelete', 'uses' => 'CompromisoController@destroy','middleware' => 'role:admin']);
Route::get('compromisoshowall', ['as' => 'compromisoshowall', 'uses' => 'CompromisoController@index']);
Route::get('proyectoshowbyarea', ['as' => 'proyectoshowbyarea', 'uses' => 'CompromisoController@index']);
Route::get('cargoslistjson/{nivelId}', ['as' => 'cargoslistjson', 'uses' => 'CargoController@cargoslistjson']);

Route::get('niveljerarquicoadd', ['as' => 'niveljerarquicoadd', 'uses' => 'NivelJerarquicoController@create','middleware' => 'role:admin']);
Route::post('niveljerarquicoadd', ['as' => 'niveljerarquicoadd', 'uses' => 'NivelJerarquicoController@store','middleware' => 'role:admin']);
Route::get('niveljerarquicoedit/{id}', ['as' => 'niveljerarquicoedit', 'uses' => 'NivelJerarquicoController@edit','middleware' => 'role:admin']);
Route::post('niveljerarquicoedit', ['as' => 'niveljerarquicoedit', 'uses' => 'NivelJerarquicoController@update','middleware' => 'role:admin']);
Route::get('niveljerarquicodelete/{id}', ['as' => 'niveljerarquicodelete', 'uses' => 'NivelJerarquicoController@destroy','middleware' => 'role:admin']);
Route::get('niveljerarquicoshowall', ['as' => 'niveljerarquicoshowall', 'uses' => 'NivelJerarquicoController@index']);

Route::get('configuracionevaluacionadd', ['as' => 'configuracionevaluacionadd', 'uses' => 'ConfiguracionEvaluacionController@create','middleware' => 'role:admin']);
Route::post('configuracionevaluacionadd', ['as' => 'configuracionevaluacionadd', 'uses' => 'ConfiguracionEvaluacionController@store','middleware' => 'role:admin']);
Route::get('configuracionevaluacionedit/{id}', ['as' => 'configuracionevaluacionedit', 'uses' => 'ConfiguracionEvaluacionController@edit','middleware' => 'role:admin']);
Route::post('configuracionevaluacionedit', ['as' => 'configuracionevaluacionedit', 'uses' => 'ConfiguracionEvaluacionController@update','middleware' => 'role:admin']);
Route::get('configuracionevaluaciondelete/{id}', ['as' => 'configuracionevaluaciondelete', 'uses' => 'ConfiguracionEvaluacionController@destroy','middleware' => 'role:admin']);
Route::get('configuracionevaluacionshowall', ['as' => 'configuracionevaluacionshowall', 'uses' => 'ConfiguracionEvaluacionController@index']);
Route::post('configuracionvigenciaexistajax', ['as' => 'configuracionvigenciaexistajax', 'uses' => 'ConfiguracionEvaluacionController@vigenciaExistAjax','middleware' => 'role:admin']);

Route::post('configuracionevaluacion/validatevigencia', ['as' => 'configuracionvigenciaexist', 'uses' => 'ConfiguracionEvaluacionController@vigenciaExist']);

Route::post('configuracionevaluacion/getvalidvigencia', ['as' => 'configuracionvalidvigencia', 'uses' => 'ConfiguracionEvaluacionController@getValidVigencias']);

Route::get('userroleshowall', ['as' => 'userroleshowall', 'uses' => 'UserRoleController@index','middleware' => 'role:admin']);
Route::get('userroleadd', ['as' => 'userroleadd', 'uses' => 'UserRoleController@create','middleware' => 'role:admin']);
Route::post('userroleadd', ['as' => 'userroleadd', 'uses' => 'UserRoleController@store','middleware' => 'role:admin']);
Route::get('userroleedit/{id}', ['as' => 'userroleedit', 'uses' => 'UserRoleController@edit','middleware' => 'role:admin']);
Route::post('userroleedit', ['as' => 'userroleedit', 'uses' => 'UserRoleController@update','middleware' => 'role:admin']);
Route::get('userroledelete', ['as' => 'userroledelete', 'uses' => 'UserRoleController@show','middleware' => 'role:admin']);

Route::get('fileentry', 'FileEntryController@index');
Route::get('fileentry/get/{filename}', ['as' => 'getentry', 'uses' => 'FileEntryController@get']);
Route::post('fileentry/add',['as' => 'addentry', 'uses' => 'FileEntryController@add']);
Route::post('fileentry/addajax',['as' => 'addentryajax', 'uses' => 'FileEntryController@addAjax']);

Route::get('evaluacionadd', ['as' => 'evaluacionadd', 'uses' => 'EvaluacionController@create','middleware' => 'role:admin|evaluador']);
Route::post('evaluacionadd', ['as' => 'evaluacionadd', 'uses' => 'EvaluacionController@store','middleware' => 'role:admin|evaluador']);

Route::get('evaluaciones/showallbyevaluador',
			[
				'as' => 'evaluacionshowallbyevaluador',
				'uses' => 'EvaluacionController@index','middleware' => 'role:admin|evaluador'
			]);

Route::get('evaluaciones/showcompromisolaboralesbyevaluacion/{id}',
			[
				'as' => 'evaluacionshowcompromisolaboralesbyevaluacion',
				'uses' => 'EvaluacionController@showCompromisosByEvaluacionId','middleware' => 'role:admin|evaluador'
			]);

Route::get('evaluaciones/showcompromisocomportamentalesbyevaluacion/{id}',
			[
				'as' => 'evaluacionshowcompromisocomportamentalesbyevaluacion',
				'uses' => 'EvaluacionController@showCompetenciasByEvaluacionId','middleware' => 'role:admin|evaluador'
			]);



Route::get('evaluacionshowall', ['as' => 'evaluacionshowall', 'uses' => 'EvaluacionController@showall','middleware' => 'role:admin']);
Route::get('evaluacionaddcompromiso/{id}', ['as' => 'evaluacionaddcompromiso', 'uses' => 'EvaluacionController@compromiso','middleware' => 'role:admin|evaluador']);
Route::post('evaluacionaddcompromiso', ['as' => 'evaluacionaddcompromiso', 'uses' => 'EvaluacionController@compromisoStore','middleware' => 'role:admin|evaluador']);

Route::get('evaluaciones/getcompromisoslaboralesbyevaluacionidajax/{id}',
			[
				'as' => 'compromisoslaboralesbyevaluacionidajax',
				'uses' => 'EvaluacionController@getCompromisosLaboralesByEvaluacionIdAjax',
				'middleware' => 'role:admin|evaluador'
			]);

Route::get('evaluaciones/getmetasbyproyectoidajax/{id}',
			[
				'as' => 'evaluacionmetasbyproyectoidajax',
				'uses' => 'ProyectoController@getMetasByProyectoIdAjax',
				'middleware' => 'role:admin|evaluador'
			]);

Route::get('evaluaciones/compromisos/evidenciadefine/{id}/{evaluacionId}',
			[
				'as' => 'evaluacionescompromisosevidenciadefine',
				'uses' => 'EvaluacionController@compromisosEvidenciadefineByCompromisoId',
				'middleware' => 'role:admin|evaluador'
			]);

Route::post('evaluaciones/compromisos/evidenciadefine',
			[
				'as' => 'evaluacionescompromisosevidenciadefinesave',
				'uses' => 'EvaluacionController@compromisosEvidenciadefineByCompromisoIdStore',
				'middleware' => 'role:admin|evaluador'
			]);

Route::post('evaluacion/conductasadd', ['as' => 'evaluacion/conductasadd', 'uses' => 'EvaluacionController@conductasAdd']);

//macroproceso route
Route::get('macroprocesoadd', ['as' => 'macroprocesoadd', 'uses' => 'MacroprocesoController@create','middleware' => 'role:admin']);
Route::post('macroprocesoadd', ['as' => 'macroprocesoadd', 'uses' => 'MacroprocesoController@store','middleware' => 'role:admin']);
Route::get('macroprocesoedit/{id}', ['as' => 'macroprocesoedit', 'uses' => 'MacroprocesoController@edit','middleware' => 'role:admin']);
Route::post('macroprocesoedit', ['as' => 'macroprocesoedit', 'uses' => 'MacroprocesoController@update','middleware' => 'role:admin']);
Route::get('macroprocesodelete/{id}', ['as' => 'macroprocesodelete', 'uses' => 'MacroprocesoController@destroy','middleware' => 'role:admin']);
Route::get('macroprocesoshowall', ['as' => 'macroprocesoshowall', 'uses' => 'MacroprocesoController@index']);

//proceso route
Route::get('procesoadd', ['as' => 'procesoadd', 'uses' => 'ProcesoController@create','middleware' => 'role:admin']);
Route::post('procesoadd', ['as' => 'procesoadd', 'uses' => 'ProcesoController@store','middleware' => 'role:admin']);
Route::get('procesoedit/{id}', ['as' => 'procesoedit', 'uses' => 'ProcesoController@edit','middleware' => 'role:admin']);
Route::post('procesoedit', ['as' => 'procesoedit', 'uses' => 'ProcesoController@update','middleware' => 'role:admin']);
Route::get('procesodelete/{id}', ['as' => 'procesodelete', 'uses' => 'ProcesoController@destroy','middleware' => 'role:admin']);
Route::get('procesoshowall', ['as' => 'procesoshowall', 'uses' => 'ProcesoController@index']);

//actividad route
Route::get('actividadadd', ['as' => 'actividadadd', 'uses' => 'ActividadController@create','middleware' => 'role:admin']);
Route::post('actividadadd', ['as' => 'actividadadd', 'uses' => 'ActividadController@store','middleware' => 'role:admin']);
Route::get('actividadedit/{id}', ['as' => 'actividadedit', 'uses' => 'ActividadController@edit','middleware' => 'role:admin']);
Route::post('actividadedit', ['as' => 'actividadedit', 'uses' => 'ActividadController@update','middleware' => 'role:admin']);
Route::get('actividaddelete/{id}', ['as' => 'actividaddelete', 'uses' => 'ActividadController@destroy','middleware' => 'role:admin']);
Route::get('actividadshowall', ['as' => 'actividadshowall', 'uses' => 'ActividadController@index']);
