<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeProfile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attribute_profile';
}