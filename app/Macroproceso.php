<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Macroproceso extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'macroprocesos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','active','vigencia','color'];

    public function proceso() {
        
        return $this->hasMany('App\Proceso');
    }
}
