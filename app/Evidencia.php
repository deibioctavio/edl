<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evidencia extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'evidencias';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                            'description',
                            'percentual_weight',
                            'last_file_evidence_added',
                            'verification_date',
                            'last_verification_user_id',
                            'comments',
                            'active'
                        ];

    public function proyecto() {
        
        return $this->belongsTo('App\Proyecto');
    }
}
