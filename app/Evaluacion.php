<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluacion extends Model
{
    //evaluaciones
    protected $table = 'evaluaciones';

    protected $fillable = [
    							'tipo',
    							'vigencia',
    							'dias_evaluados',
    							'porcentaje_evaluado',
    							'usuario_evaluado_id',
                                'usuario_evaluado_cargo_id',
                                'usuario_evaluado_area_id',
    							'usuario_evaluador1_id', //Jefe Directo
    							'usuario_evaluador2_id', // Funcionario de LNR para casos de comisión
    							'periodo_evaluado_inicio',
    							'periodo_evaluado_fin',
    							'fecha_fijacion_compromiso',
    							'proposito',
    							'aceptacion_evaluacion', //SI,NO Boleano
    							'usuario_aceptacion_evaluacion_id',
    							'fecha_aceptacion_evaluacion',
    							'observacion_aceptacion_evaluacion'
    						];
    						
    public function tipoEvaluacion() {
        
        return $this->belongsTo('App\TipoEvaluacion');
    }
}
