<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compromiso extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'compromisos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description','goal','obtained_percentage','comments','active','vigencia'];

    public function cargo() {
        
       return $this->belongsTo('App\Cargo');
    }

    public function evidencias() {
        
       return $this->hasMany('App\Evidencia');
    }
}
