<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'proyectos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','active','vigencia'];

    public function area() {
        
        return $this->belongsTo('App\Area');
    }

    public function metas() {
        
        return $this->hasMany('App\Meta');
    }
}
