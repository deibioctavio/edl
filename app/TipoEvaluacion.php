<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEvaluacion extends Model
{
    protected $table = 'tipo_evaluaciones';

    protected $fillable = ['name','description','dias_evaluacion','porcentaje_evaluado','active'];

    public function evaluaciones() {
        
        return $this->hasMany('App\Evaluacion');
    }
}
