<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    protected $fillable = [
                            'name','lastname','document_type','document_number',
                            'birthday','address','gender','phone','cellphone',
                            'area_id','cargo_id','is_proccess_member'
                        ];

    public function user() {
        
       return $this->belongsTo('App\User');
    }
}
