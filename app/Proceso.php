<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proceso extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'procesos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','identificador','description','active','vigencia','area_id','macroproceso_id'];

    public function macroproceso() {
        
        return $this->belongsTo('App\Macroproceso');
    }
    public function actividad() {
        
        return $this->hasMany('App\Actividad');
    }
    public function area() {
        
        return $this->belongsTo('App\Area');
    }
}
