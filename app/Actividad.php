<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'actividades';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','active','vigencia','proceso_id'];

    public function proceso() {
        
        return $this->belongsToMany('App\Proceso');
    }
}
