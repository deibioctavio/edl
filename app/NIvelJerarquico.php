<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NivelJerarquico extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nivel_jerarquico';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','active','vigencia'];

    public function cargos() {
        
        return $this->hasMany('App\Cargo');
    }
}
