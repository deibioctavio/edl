<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompromisosLaboralesEvaluacion extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'compromisos_laborales_evaluacion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    						'evaluacion_id',
    						'proyecto_id',
    						'meta_id',
    						'compromiso_id',
    						'condicion_resultado',
    						'procentaje_cumpimiento_pactado',
    						'procentaje_cumpimiento_obtenido',
    						'periodo',
    						'comments',
    						'active'
    					];

    public function evaluacion() {
        
       return $this->belongsTo('App\Evaluacion');
    }

    public function evidencias() {
        
       return $this->hasMany('App\Evidencia');
    }
}
