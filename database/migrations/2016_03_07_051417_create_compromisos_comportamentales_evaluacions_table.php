<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompromisosComportamentalesEvaluacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compromisos_comportamentales', function (Blueprint $table) {
            $table->increments('id')->unsigned()->index();
            $table->integer('evaluacion_id')->unsigned()->index();
            $table->integer('competencia_id')->unsigned()->index();
            $table->integer('conducta_id')->unsigned()->index();
            $table->decimal('procentaje_cumpimiento_pactado',20,17)->nullable()->unsigned();
            $table->decimal('procentaje_cumpimiento_obtenido',20,17)->nullable()->unsigned();
            $table->string('detalle_calificacion_obtenida',50)->nullable();
            $table->integer('periodo')->nullable()->unsigned()->index();
            $table->text('comments')->nullable();
            $table->boolean('active')->default(1);
            $table->integer('usuario_id_create')->unsigned()->index();
            $table->integer('usuario_id_last_update')->unsigned()->nullable()->index();
            $table->timestamps();
            $table->foreign('evaluacion_id')->references('id')->on('evaluaciones')->onDelete('cascade');;
            $table->foreign('competencia_id')->references('id')->on('competencias');
            $table->foreign('conducta_id')->references('id')->on('conductas_asociadas');
            $table->foreign('usuario_id_create')->references('id')->on('users');
            $table->foreign('usuario_id_last_update')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('compromisos_comportamentales');
    }
}
