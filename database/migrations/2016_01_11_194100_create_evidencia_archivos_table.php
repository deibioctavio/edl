<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvidenciaArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evidencia_archivos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('evidencia_id')->unsigned()->index();
            $table->integer('fileentries_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('evidencia_id')->references('id')->on('evidencias')->onDelete('cascade');
            $table->foreign('fileentries_id')->references('id')->on('fileentries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evidencia_archivos');
    }
}
