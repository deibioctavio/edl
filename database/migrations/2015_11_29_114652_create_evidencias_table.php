<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvidenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evidencias', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('description');
            $table->integer('percentual_weight')->unsigned();
            $table->text('comments')->nullable();
            $table->integer('last_verification_user_id')->unsigned()->nullable()->index();
            $table->integer('create_user_id')->unsigned()->index();
            $table->boolean('active')->default(1);
            $table->timestamp('last_date_file_evidence_added')->nullable();
            $table->timestamp('verification_date')->nullable();
            $table->integer('compromiso_id')->unsigned()->index();
            $table->string('type')->default('laboral');; //laboral|comportamental

            $table->timestamps();

            $table->foreign('compromiso_id')->references('id')->on('compromisos_laborales_evaluacion')->onDelete('cascade');
            $table->foreign('last_verification_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('create_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evidencias');
        Schema::drop('compromisos_laborales_evaluacion');
        Schema::drop('cargos');
        Schema::drop('nivel_jerarquico');
    }
}
