<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompromisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('compromisos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->text('description')->nullable();
            $table->integer('goal');
            $table->integer('obtained_percentage');
            $table->text('comments')->nullable();
            $table->boolean('active')->default(1);
            $table->integer('vigencia')->default(2016);
            $table->integer('cargo_id')->unsigned()->index();
            $table->foreign('cargo_id')->references('id')->on('cargos')->onDelete('cascade');
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('compromisos');
    }
}
