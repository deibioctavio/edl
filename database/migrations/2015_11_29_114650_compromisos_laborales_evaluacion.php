<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompromisosLaboralesEvaluacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compromisos_laborales_evaluacion', function (Blueprint $table) {
            $table->increments('id')->unsigned()->index();
            $table->integer('evaluacion_id')->unsigned()->index();
            $table->integer('proyecto_id')->unsigned()->index();
            $table->integer('meta_id')->unsigned()->index();
            $table->integer('compromiso_id')->unsigned()->index();
            $table->text('condicion_resultado')->nullable();
            $table->decimal('procentaje_cumpimiento_pactado',5,2)->nullable()->unsigned();
            $table->decimal('procentaje_cumpimiento_obtenido',5,2)->nullable()->unsigned();
            $table->integer('periodo')->nullable()->unsigned()->index();
            $table->text('comments')->nullable();
            $table->boolean('active')->default(1);
            $table->integer('usuario_id_create')->unsigned()->index();
            $table->integer('usuario_id_last_update')->unsigned()->nullable()->index();
            $table->timestamps();
            $table->foreign('evaluacion_id')->references('id')->on('evaluaciones')->onDelete('cascade');;
            $table->foreign('proyecto_id')->references('id')->on('proyectos');
            $table->foreign('meta_id')->references('id')->on('metas');
            $table->foreign('compromiso_id')->references('id')->on('compromisos');
            $table->foreign('usuario_id_create')->references('id')->on('users');
            $table->foreign('usuario_id_last_update')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('compromisos_laborales_evaluacion');
    }
}
