<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracionEvaluacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('configuracion_evaluaciones', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('n_compromisos_laborales')->unsigned();
            $table->integer('n_competencias_comportamentales')->unsigned();
            $table->decimal('valor_compromisos_laborales',5,2)->unsigned();
            $table->decimal('valor_competencias_comportamentales',5,2)->unsigned();
            $table->integer('vigencia')->unique()->unsigned();
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::drop('configuracion_evaluaciones');
    }
}
