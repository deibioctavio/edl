<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluaciones', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('tipo_evaluacion_id')->unsigned()->index();
            $table->integer('vigencia')->unsigned()->default(2016);
            $table->integer('dias_evaluados')->unsigned();
            $table->decimal('porcentaje_evaluado',5,2)->unsigned();
            $table->integer('usuario_evaluado_id')->unsigned()->index();
            $table->integer('usuario_evaluado_cargo_id')->unsigned()->index();
            $table->integer('usuario_evaluado_area_id')->unsigned()->index();
            $table->integer('usuario_evaluador1_id')->unsigned()->index();
            $table->integer('usuario_evaluador2_id')->nullable()->unsigned()->index();
            $table->integer('usuario_diligencia_id')->unsigned()->index(); //Usuario que crea la evaluación
            $table->date('periodo_evaluado_inicio');
            $table->date('periodo_evaluado_fin');
            $table->date('fecha_fijacion_compromiso'); //Automática cuando se guarda la evaluación
            $table->text('proposito')->nullable();
            $table->boolean('aceptacion_evaluacion')->nullable(); //Por defecto No hasta que el funcionario acepte
            $table->integer('usuario_aceptacion_evaluacion_id')->nullable()->unsigned()->index();
            $table->timestamp('fecha_aceptacion_evaluacion')->nullable();
            $table->text('observacion_aceptacion_evaluacion')->nullable();
            $table->timestamps();
            $table->foreign('tipo_evaluacion_id')->references('id')->on('tipo_evaluaciones');
            $table->foreign('usuario_evaluado_id')->references('id')->on('users');
            $table->foreign('usuario_evaluado_cargo_id')->references('id')->on('cargos');
            $table->foreign('usuario_evaluado_area_id')->references('id')->on('areas');
            $table->foreign('usuario_evaluador1_id')->references('id')->on('users');
            $table->foreign('usuario_evaluador2_id')->references('id')->on('users');
            $table->foreign('usuario_diligencia_id')->references('id')->on('users');
            $table->foreign('usuario_aceptacion_evaluacion_id')->references('id')->on('users');
        });


        /*
            
        $table->integer('permission_id')->unsigned()->index();
        $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');
        
        'tipo',
        'vigencia',
        'dias_evaluados',
        'porcentaje_evaluado',
        'usuario_evaluado_id',
        'usuario_evaluador1_id', //Jefe Directo
        'usuario_evaluador2_id', // Funcionario de LNR para casos de comisión
        'periodo_evaluado_inicio',
        'periodo_evaluado_fin',
        'fecha_fijacion_compromiso',
        'proposito',
        'aceptacion_evaluacion',
        'usuario_aceptacion_evaluacion_id',
        'fecha_aceptacion_evaluacion',
        'observacion_aceptacion_evaluacion'*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evaluaciones');
    }
}
