<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionRouteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_route', function (Blueprint $table) {
            $table->increments('id')->unsigned()->index();
            $table->string('slug');
            $table->string('route');
            $table->string('menu_slug');
            $table->boolean('menu_visible')->default(1);
            $table->string('description')->nullable();
            $table->string('group');
            $table->integer('permission_id')->unsigned();
            $table->timestamps();
        });//
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permission_route');
    }
}
