<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AreaTableSeeder extends Seeder {
	
	public function run() {
		
		DB::table('areas')->delete();

        DB::table('areas')->insert([
            'id' => 1,
            'name' => 'alcalde',
            'description' => 'Despacho del Alcalde',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 24,
            'name' => 'economico',
            'description' => 'Secretaría de Desarrollo Económico',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 22,
            'name' => 'social',
            'description' => 'Secretaría de Desarrollo Social',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 25,
            'name' => 'educacion',
            'description' => 'Secretaría de Educación',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 21,
            'name' => 'gobierno',
            'description' => 'Secretaría de Gobierno y Convivencia',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 26,
            'name' => 'infraestructura',
            'description' => 'Secretaría de Infraestructura',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 23,
            'name' => 'salud',
            'description' => 'Secretaría de Salud',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 28,
            'name' => 'tics',
            'description' => 'Secretaría de Tecnologías de la Información y Las Comunicaciones',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 27,
            'name' => 'transito',
            'description' => 'Secretaría de Transito y Transporte',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 34,
            'name' => 'bienes',
            'description' => 'Departamento Administrativo de Bienes y Suministros',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 36,
            'name' => 'control',
            'description' => 'Departamento Administrativo de Control Interno',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 31,
            'name' => 'dafi',
            'description' => 'Departamento Administrativo de Fortalecimiento Institucional',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 33,
            'name' => 'hacienda',
            'description' => 'Departamento Administrativo de Hacienda Municipal',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 32,
            'name' => 'juridico',
            'description' => 'Departamento Administrativo Jurídico',
            'vigencia' => 2016
        ]);

        DB::table('areas')->insert([
            'id' => 35,
            'name' => 'planeacion',
            'description' => 'Departamento Administrativo de Planeación',
            'vigencia' => 2016
        ]);
	}
}
?>