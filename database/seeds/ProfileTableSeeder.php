<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProfileTableSeeder extends Seeder {
	
	public function run() {
		
		DB::table('profiles')->delete();

        DB::table('profiles')->insert([
            'name' => 'Administrador',
            'lastname' => 'Sin Apellido',
            'document_type' => 'CC',
            'document_number' => '110001100011100',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 1,
            'area_id' => 21,
            'cargo_id' => 11,
        ]);

        DB::table('profiles')->insert([
            'name' => 'Pedro Nel',
            'lastname' => 'Salazar',
            'document_type' => 'CC',
            'document_number' => '220002202022200',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 2,
            'area_id' => 22,
            'cargo_id' => 8,
        ]);

        DB::table('profiles')->insert([
            'name' => 'María Antonia',
            'lastname' => 'Gutierrez',
            'document_type' => 'CC',
            'document_number' => '300333300003330',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'F',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 3,
            'area_id' => 23,
            'cargo_id' => 16,
        ]);

        DB::table('profiles')->insert([
            'name' => 'Edgar',
            'lastname' => 'Vivar',
            'document_type' => 'CC',
            'document_number' => '444400044004440',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 4,
            'area_id' => 24,
            'cargo_id' => 9,
        ]);

        DB::table('profiles')->insert([
            'name' => 'Nélson',
            'lastname' => 'Alvarez',
            'document_type' => 'CC',
            'document_number' => '5500055505050505',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 5,
            'area_id' => 25,
            'cargo_id' => 22,
        ]);

        DB::table('profiles')->insert([
            'name' => 'Ivan Dario',
            'lastname' => 'Gonzalez',
            'document_type' => 'CC',
            'document_number' => '6660000660066',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 6,
            'area_id' => 26,
            'cargo_id' => 23,
        ]);

        DB::table('profiles')->insert([
            'name' => 'Martha Liliana',
            'lastname' => 'Quiroz P.',
            'document_type' => 'CC',
            'document_number' => '7770007070707070',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'F',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 7,
            'area_id' => 27,
            'cargo_id' => 8,
        ]);

        DB::table('profiles')->insert([
            'name' => 'Leonel',
            'lastname' => 'Peña',
            'document_type' => 'CC',
            'document_number' => '8000808880000',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 8,
            'area_id' => 28,
            'cargo_id' => 21,
        ]);

        DB::table('profiles')->insert([
            'name' => 'Carolina',
            'lastname' => 'Muñoz',
            'document_type' => 'CC',
            'document_number' => '9090999000909',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'F',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 9,
            'area_id' => 31,
            'cargo_id' => 13,
        ]);

        DB::table('profiles')->insert([
            'name' => 'Oscar',
            'lastname' => 'Agudelo',
            'document_type' => 'CC',
            'document_number' => '990066611105532',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 10,
            'area_id' => 32,
            'cargo_id' => 11,
        ]);

        DB::table('profiles')->insert([
            'name' => 'Oscar',
            'lastname' => 'Agudelo',
            'document_type' => 'CC',
            'document_number' => '22266644001155',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 11,
            'area_id' => 33,
            'cargo_id' => 17,
        ]);
	}
}
?>