<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Bican\Roles\Models\Permission;

class PermissionRouteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('permission_route')->delete();
        $permissions = Permission::get();

        if($permissions){

            $permissionAttributes = array();
            
            $i = 0;
            foreach ($permissions as $p) {
                $permissionAttributes[$i]['route']        = str_replace(".", "", $p->slug); 
                $permissionAttributes[$i]['slug']         = $p->slug; 
                $permissionAttributes[$i]['description']  = $p->name;
                $permissionAttributes[$i]['permission_id']  = $p->id;
                $permissionAttributes[$i]['menu_visible']  = 0;
                $permissionAttributes[$i]['group']  = $p ->group;
                $i++;
            }

            DB::table('permission_route')->insert($permissionAttributes);

            DB::table('permission_route')
            ->where('slug', 'like','%.showall')
            ->update(['menu_slug' => 'admin','menu_visible' => 1]);

            DB::table('permission_route')
            ->where('slug', 'like','%.add')
            ->update(['menu_slug' => 'admin','menu_visible' => 1]);
        }
    }
}
