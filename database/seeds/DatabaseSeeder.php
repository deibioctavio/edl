<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    // php artisan db:seed
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(PermissionRouteTableSeeder::class);
        $this->call(AreaTableSeeder::class);
        $this->call(ProyectoTableSeeder::class);
        $this->call(MetaTableSeeder::class);
        $this->call(NivelJerarquicoTableSeeder::class);
        $this->call(CargoTableSeeder::class);
        $this->call(ProfileTableSeeder::class);
        $this->call(CompromisoTableSeeder::class);
        $this->call(CompetenciaTableSeeder::class);
        $this->call(ConductaAsociadaTableSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(ConfiguracionEvaluaciones::class);
        $this->call(TipoEvaluacionTableSeeder::class);
        $this->call(VigenciasReferenciasTableSeeder::class);
        $this->call(MacroprocesoTableSeeder::class);
        $this->call(ProcesoTableSeeder::class);
        $this->call(ActividadTableSeeder::class);

        Model::reguard();
    }
}

