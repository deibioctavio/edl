<?php 
use Illuminate\Log\Writer;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Proyecto;


class MetaTableSeeder extends Seeder {
	
	public function run() {
		
		DB::table('metas')->delete();

        $proyectos = Proyecto::get();

        if($proyectos){

            $i = 0;

            Log::info("HAVE Proyecto",array("count"=>count($proyectos)));
            
            foreach ($proyectos as $p) {

                $totalMetas = rand(3,6);
                $metasAttr = array();

                for( $j = 0;  $j < $totalMetas; $j++ ){

                    $metasAttr[$j]['name']        = "{$p->name}m{$j}-{$i}";
                    $metasAttr[$j]['description']  = "{$p->name} Meta: {$j} -  {$i}";
                    $metasAttr[$j]['proyecto_id']         = $p->id;
                    $metasAttr[$j]['vigencia']         = 2016;
                    $i++;
                }

                DB::table('metas')->insert($metasAttr);
                //Log::info("Area {$a->name} Proyectos ARRAY: ",array("regs"=>print_r($proyectosAttr,TRUE)));
            }
        }
	}
}
?>