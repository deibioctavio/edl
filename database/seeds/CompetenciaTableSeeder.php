<?php

use Illuminate\Database\Seeder;

class CompetenciaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('competencias')->delete();

        DB::table('competencias')->insert([
            'id' => 1,
            'name' => 'Respeto',
            'description' => 'Condición clave para un ambiente positivo y más productivo',
            'vigencia' => 2016
        ]);

        DB::table('competencias')->insert([
            'id' => 2,
            'name' => 'Sentido de Pertenencia',
            'description' => 'Queremos que quieran trabajar aquí',
            'vigencia' => 2016
        ]);

        DB::table('competencias')->insert([
            'id' => 3,
            'name' => 'Cooperación y trabajo en equipo',
            'description' => 'Factor clave para obtener mejores resultados',
            'vigencia' => 2016
        ]);

        DB::table('competencias')->insert([
            'id' => 4,
            'name' => 'Orientación al usuario',
            'description' => 'La atención al usuario no es un proceso es una actitud',
            'vigencia' => 2016
        ]);
    }
}
