<?php

use Illuminate\Database\Seeder;
use App\Cargo;

class CompromisoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('compromisos')->delete();

        $cargos = Cargo::get();

        if($cargos){

            $i = 0;

            //Log::info("HAVE cargos",array("count"=>count($cargos)));
            
            foreach ($cargos as $c) {

                $totalCompromisos = rand(3,6);
                $compromisosAttr = array();

                for( $j = 0;  $j < $totalCompromisos; $j++ ){

                    $compromisosAttr[$j]['description']  = "{$c->name} Compromiso: {$j} -  {$i}";
                    $compromisosAttr[$j]['goal']         = 100;
                    $compromisosAttr[$j]['cargo_id']         = $c->id;
                    $compromisosAttr[$j]['vigencia']         = 2016;
                    $i++;
                }

                DB::table('compromisos')->insert($compromisosAttr);
                //Log::info("Area {$a->name} Proyectos ARRAY: ",array("regs"=>print_r($proyectosAttr,TRUE)));
            }
        }
    }
}
