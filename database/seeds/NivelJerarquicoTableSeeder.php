<?php

use Illuminate\Database\Seeder;

class NivelJerarquicoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /*
$table->increments('id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
    */
    public function run()
    {
        DB::table('nivel_jerarquico')->delete();

        DB::table('nivel_jerarquico')->insert([
            'id' => 1,
            'name' => 'profesional',
            'description' => 'Profesional',
            'vigencia' => 2016
        ]);

         DB::table('nivel_jerarquico')->insert([
            'id' => 2,
            'name' => 'directivo',
            'description' => 'Directivo',
            'vigencia' => 2016
          ]);

          DB::table('nivel_jerarquico')->insert([
            'id' => 3,
            'name' => 'asesor',
            'description' => 'Asesor',
            'vigencia' => 2016
          ]);

          DB::table('nivel_jerarquico')->insert([
            'id' => 4,
            'name' => 'tecnico',
            'description' => 'Técnico',
            'vigencia' => 2016
          ]);

          DB::table('nivel_jerarquico')->insert([
            'id' => 5,
            'name' => 'asistencia',
            'description' => 'Asistencia',
            'vigencia' => 2016
          ]);

          DB::table('nivel_jerarquico')->insert([
              'id' => 6,
              'name' => 'profesionalpersonasacargo',
              'description' => 'Profesional con Personas a Cargo',
              'vigencia' => 2016
          ]);
    }



}
