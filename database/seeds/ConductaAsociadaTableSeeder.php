<?php
use Illuminate\Log\Writer;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Competencia;
use App\ConductaAsociada;

class ConductaAsociadaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('conductas_asociadas')->delete();

        DB::table('conductas_asociadas')->insert([
            'name' => 'Atiende a los usuarios internos y externos con tono de voz cálido y cortés a través de un lenguaje adecuado y sencillo, generando  confianza y simpatía en ellos.',
            'vigencia' => 2016,
            'competencia_id' => 1
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Se dirige a las personas con las cuales se relaciona  refiriéndose a ellos por su nombre o distinción (señor, señora, joven, niño, niña), demostrando hacia éstos un gran respeto.',
            'vigencia' => 2016,
            'competencia_id' => 1
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Establece contacto visual y verbal con los usuarios internos y externos,   manifestando así el interés por brindar una buena atención.',
            'vigencia' => 2016,
            'competencia_id' => 1
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Se dirige al personal  a su cargo de forma respetuosa, llamándoles por su nombre y en un tono de voz adecuada, generando confianza y buscando establecer diálogo.',
            'vigencia' => 2016,
            'competencia_id' => 1
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Demuestra respeto hacia los compañeros de trabajo, evitando vociferar, proferir insultos, regaños,   escuchar música en alto volumen, discutir acaloradamente o  practicar rituales en el sitio de trabajo.',
            'vigencia' => 2016,
            'competencia_id' => 1
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Facilita el desarrollo de las actividades laborales de sus compañeros de trabajo,  evitando distraerlos con charlas extensas,   conformación de corrillos u otras acciones   que  alteren  la tranquilidad del sitio de trabajo.',
            'vigencia' => 2016,
            'competencia_id' => 1
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Evita atender vendedores, recibir visitas prolongadas de funcionarios o de  terceros que sean ajenas a su labor,  y que interfieran en el desarrollo normal de las actividades suyas o de sus compañeros de trabajo.',
            'vigencia' => 2016,
            'competencia_id' => 1
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Gestiona los equipos de trabajo, útiles de oficina y demás  recursos necesarios para el buen desempeño de los funcionarios a su cargo, promoviendo el orden y generando un ambiente de trabajo sano y seguro.',
            'vigencia' => 2016,
            'competencia_id' => 1
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Muestra prudencia por los diálogos ajenos, evitando interrumpirlos o escucharlos; igualmente  evita retirarse de una conversación sin que su interlocutor haya terminado.',
            'vigencia' => 2016,
            'competencia_id' => 1
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Conserva los modales básicos de educación tales como:  saludar y despedirse,  hacer  peticiones con amabilidad, tomar y entregar objetos y documentos con sutileza y sin brusquedad.',
            'vigencia' => 2016,
            'competencia_id' => 1
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Respeta  el tiempo de los demás, asistiendo puntualmente a las reuniones y a  otras actividades laborales a las que sea convocado y a las que por su responsabilidad deba convocar.',
            'vigencia' => 2016,
            'competencia_id' => 1
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Realiza los llamados de atención de forma respetuosa y de manera personal,  evitando hacer públicas situaciones individuales de los colaboradores a su cargo.',
            'vigencia' => 2016,
            'competencia_id' => 1
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Demuestra interés por colaborar con sus compañeros de trabajo,  brindando en todo momento su apoyo  y sus conocimientos.',
            'vigencia' => 2016,
            'competencia_id' => 2
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Actualiza sus conocimientos y competencias laborales, asistiendo a las actividades programadas  en el Plan Institucional de Capacitación, siempre que sean pertinentes a las funciones que desempeña y acordes a su perfil académico.',
            'vigencia' => 2016,
            'competencia_id' => 2
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Participa activamente  en la ejecución  de  los programas y  proyectos tendientes al cumplimiento de los objetivos y metas de la dependencia o  la entidad, comprometiendo sus capacidades y saber en el logro de éstos.',
            'vigencia' => 2016,
            'competencia_id' => 2
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Establece metas de trabajo individual  y promueve en el personal a su cargo  el desarrollo de logros motivándolos para  alcanzar altos niveles de desempeño laboral.',
            'vigencia' => 2016,
            'competencia_id' => 2
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Acepta las consecuencias de sus acciones, decisiones u omisiones, sin buscar excusas o tratar de inculpar a otros de las actuaciones inadecuadas.',
            'vigencia' => 2016,
            'competencia_id' => 2
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Mantiene  con confidencialidad, prudencia y discreción,  la información a su cargo, evitando poner en evidencia asuntos institucionales de carácter reservado o  antes que éstos sean oficiales.',
            'vigencia' => 2016,
            'competencia_id' => 2
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Demuestra responsabilidad y seriedad con sus obligaciones, organizando su trabajo, cumpliendo las fechas de entregas de informes y atendiendo con buena disposición las tareas asignadas.',
            'vigencia' => 2016,
            'competencia_id' => 2
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Asume las responsabilidades propias de su cargo y rol de Líder,  evitando delegar en su equipo de colaboradores,  asuntos que son de su competencia.',
            'vigencia' => 2016,
            'competencia_id' => 2
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Demuestra  identidad con la organización evitando incurrir dentro o fuera de las instalaciones en conductas o situaciones que deterioren el buen nombre o prestigio  de la entidad.',
            'vigencia' => 2016,
            'competencia_id' => 2
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Acoge y respeta las posiciones formales  adoptadas  por la entidad, anteponiendo la conveniencia  institucional frente a cualquier interés  personal o particular.',
            'vigencia' => 2016,
            'competencia_id' => 2
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Proyecta una imagen positiva de la institución con una adecuada presentación personal  y evitando incurrir en conductas que deterioren el buen nombre y la reputación de la entidad.',
            'vigencia' => 2016,
            'competencia_id' => 2
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Como Líder se involucra  y participa  activamente de las actividades desarrolladas por la entidad, motivando con su actuar  el sentido de pertenencia en las personas a su cargo.',
            'vigencia' => 2016,
            'competencia_id' => 2
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Utiliza una comunicación directa con los demás, cara a cara, observando sus gestos, movimientos, postura corporal, tono de voz, que permita una retroalimentación asertiva entre las partes.',
            'vigencia' => 2016,
            'competencia_id' => 3
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Expresa en forma clara y concreta lo que piensa, siente o necesita, adecuando las expresiones y lenguaje a las características de su  interlocutor,  logrando así una comunicación efectiva ',
            'vigencia' => 2016,
            'competencia_id' => 3
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Se muestra emocionalmente equilibrada al expresar sus opiniones y hacer valer sus derechos, sentimientos y necesidades, generando asertividad en la comunicación.',
            'vigencia' => 2016,
            'competencia_id' => 3
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Toma en cuenta  el conocimiento, las ideas, propuestas, emociones y opiniones de sus subalternos, a fin de crear un ambiente propicio de entendimiento y comprensión, para el desarrollo del trabajo',
            'vigencia' => 2016,
            'competencia_id' => 3
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Atiende los requerimientos y necesidades de los usuarios  con equidad y justicia,  sin distingo de su condición política, religiosa, raza o sexo.',
            'vigencia' => 2016,
            'competencia_id' => 3
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Realiza la entrega del puesto de trabajo, dedicando el tiempo necesario para la adecuada comprensión  y teniendo disposición para retroalimentar las inquietudes posteriores de quien recibe, generando continuidad de los procesos.',
            'vigencia' => 2016,
            'competencia_id' => 3
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Acata  las instrucciones de sus superiores inmediatos y jerárquicos, reconociendo los niveles de autoridad y actuando conforme a los procedimientos establecidos.',
            'vigencia' => 2016,
            'competencia_id' => 3
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Guarda justicia y equidad para asignar responsabilidades,  siendo objetivo  al momento de distribuir tareas entre su equipo de trabajo.',
            'vigencia' => 2016,
            'competencia_id' => 3
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Aporta todas sus capacidades y conocimientos  sin esperar algún beneficio adicional, desarrollando hábitos de mejora continua que fortalezcan el Sistema de Gestión de Calidad.',
            'vigencia' => 2016,
            'competencia_id' => 3
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Cumple con los horarios de trabajo establecidos en la realización de actividades laborales internas o externas, informando oportunamente las ausencias o situaciones que interrumpan la jornada laboral.',
            'vigencia' => 2016,
            'competencia_id' => 3
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Demuestra  buen ejemplo,  reconociendo los errores y ofreciendo disculpas cuando se equivoca; acatando sugerencias y recomendaciones frente a actitudes y comportamientos reprobables.',
            'vigencia' => 2016,
            'competencia_id' => 3
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Demuestra comportamiento ejemplar en sus actos en el contexto laboral,  cumpliendo con las  normas  de convivencia y convirtiéndose en un referente para sus colaboradores.',
            'vigencia' => 2016,
            'competencia_id' => 3
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Atiende las solicitudes de los usuarios  con dedicación y paciencia aún en acontecimientos difíciles, demostrando  un gran interés por resolver sus consultas, quejas o reclamos.',
            'vigencia' => 2016,
            'competencia_id' => 4
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Comprende las necesidades y solicitudes de los usuarios, interactuando e indagando puntualmente respecto a sus requerimientos.',
            'vigencia' => 2016,
            'competencia_id' => 4
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Muestra disposición para acoger las sugerencias y recomendaciones de los usuarios y establece mecanismos para conocer su nivel de satisfacción.',
            'vigencia' => 2016,
            'competencia_id' => 4
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Como responsable de la dependencia o área atiende de forma oportuna  las solicitudes de los  usuarios internos y externos, brindando  alternativas de solución que estén a su alcance, satisfaciendo las necesidades presentadas.',
            'vigencia' => 2016,
            'competencia_id' => 4
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Orienta a los usuarios  en forma correcta, con amabilidad y  prontitud, brindando información completa, clara y oportuna.',
            'vigencia' => 2016,
            'competencia_id' => 4
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Gestiona y utiliza de manera constante y eficiente los recursos requeridos para atender las necesidades del usuario.',
            'vigencia' => 2016,
            'competencia_id' => 4
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'En su calidad de Jefe resuelve conflictos de forma oportuna, asumiendo una actitud conciliadora,  promoviendo un ambiente cálido y agradable tanto para los usuarios externos como para los colaboradores a su cargo.',
            'vigencia' => 2016,
            'competencia_id' => 4
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Escucha a los usuarios sin interrumpirlos, mostrando interés y esmero por atender sus requerimientos.',
            'vigencia' => 2016,
            'competencia_id' => 4
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Manifiesta interés por los usuarios, adoptando   una sincera  disposición de servicio, evitando posturas corporales inadecuadas, el uso de celulares u otros equipos de comunicación y cómputo que interfieran en la atención  personal.',
            'vigencia' => 2016,
            'competencia_id' => 4
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Demuestra una actitud diligente actuando con sentido de la urgencia y prestando la atención con enfoque diferencial  ante circunstancias de los usuarios que requieran anticiparse o responder prioritariamente.',
            'vigencia' => 2016,
            'competencia_id' => 4
        ]);

        DB::table('conductas_asociadas')->insert([
            'name' => 'Muestra empatía frente a las dificultades presentadas  teniendo la capacidad de comprender las situaciones y necesidades del personal a su cargo.',
            'vigencia' => 2016,
            'competencia_id' => 4
        ]);
    }
}
