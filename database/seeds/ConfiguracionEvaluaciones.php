<?php

use Illuminate\Database\Seeder;

class ConfiguracionEvaluaciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configuracion_evaluaciones')->delete();

        DB::table('configuracion_evaluaciones')->insert([
            'n_compromisos_laborales' => 3,
            'n_competencias_comportamentales' => 2,
            'valor_compromisos_laborales' => 80,
            'valor_competencias_comportamentales' => 20,
            'vigencia' => 2015,
        ]);

        DB::table('configuracion_evaluaciones')->insert([
            'n_compromisos_laborales' => 4,
            'n_competencias_comportamentales' => 3,
            'valor_compromisos_laborales' => 75,
            'valor_competencias_comportamentales' => 25,
            'vigencia' => 2016,
        ]);

        DB::table('configuracion_evaluaciones')->insert([
            'n_compromisos_laborales' => 5,
            'n_competencias_comportamentales' => 3,
            'valor_compromisos_laborales' => 70,
            'valor_competencias_comportamentales' => 30,
            'vigencia' => 2017,
        ]);
    }
}
