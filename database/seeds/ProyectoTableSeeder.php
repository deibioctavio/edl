<?php 
use Illuminate\Log\Writer;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Area;


class ProyectoTableSeeder extends Seeder {
	
	public function run() {
		
		DB::table('proyectos')->delete();

        $areas = Area::get();

        if($areas){

            $i = 0;

            Log::info("HAVE AREA",array("count"=>count($areas)));
            
            foreach ($areas as $a) {

                $totalProyectos = rand(3,6);
                $proyectosAttr = array();

                for( $j = 0;  $j < $totalProyectos; $j++ ){

                    $proyectosAttr[$j]['name']        = "{$a->name}p{$j}-{$i}";
                    $proyectosAttr[$j]['description']  = "Área: {$a->name} Proyecto: {$j} -  {$i}";
                    $proyectosAttr[$j]['area_id']         = $a->id;
                    $proyectosAttr[$j]['vigencia']         = 2016;
                    $i++;
                }

                DB::table('proyectos')->insert($proyectosAttr);
                //Log::info("Area {$a->name} Proyectos ARRAY: ",array("regs"=>print_r($proyectosAttr,TRUE)));
            }
        }
	}
}
?>