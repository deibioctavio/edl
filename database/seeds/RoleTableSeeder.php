<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Bican\Roles\Models\Role;

class RoleTableSeeder extends Seeder {
	public function run() {

		DB::table('role_user')->delete();
		DB::table('roles')->delete();

		DB::table('roles')->insert(
			[
				[
					'id' => 1,
					'name' => 'Admin',
				    'slug' => 'admin',
				    'description' => 'Administrador aplicación', // optional
				    'level' => 1, // optional, set to 1 by default
				    'created_at' => time(),
				],
				[
					'id' => 2,
					'name' => 'Evaluador',
				    'slug' => 'evaluador',
				    'description' => 'Rol de Evaluador - EDL',
				    'level' => 2,
				    'created_at' => time(),
				],
				[
					'id' => 3,
					'name' => 'Funcionario',
				    'slug' => 'funcionario',
				    'description' => 'Rol funcionario Evaluado',
				    'level' => 3,
				    'created_at' => time(),
				],
				[
					'id' => 4,
					'name' => 'Asistente',
				    'slug' => 'asistente',
				    'description' => 'Rol de Asistente evaluador/admin',
				    'level' => 4,
				    'created_at' => time(),
				],
				[
					'id' => 5,
					'name' => 'Administrador de Contenidos',
				    'slug' => 'contentadmin',
				    'description' => 'Rol de Administrador de Contenidos',
				    'level' => 5,
				    'created_at' => time(),
				],
			]
		);

		$role = Role::where('slug', 'admin')->first();
		$user = User::where('name', 'usuarioadmin')->first();
		if($role && $user){
		    $user->attachRole($role);
		}

		/*
		$role = Role::where('slug', 'asistente')->first();
		$user = User::where('name', 'deibioctavio')->first();
		if($role && $user){
		    $user->attachRole($role);
		}*/

		$role = Role::where('slug', 'admin')->first();
		$user = User::where('name', 'administradoredl')->first();
		if($role && $user){
		    $user->attachRole($role);
		}

		$role = Role::where('slug', 'evaluador')->first();
		$user = User::where('name', 'evaluadoredl')->first();
		if($role && $user){
		    $user->attachRole($role);
		}

		$role = Role::where('slug', 'funcionario')->first();
		$user = User::where('name', 'funcionarioedl')->first();
		if($role && $user){
		    $user->attachRole($role);
		}

		$role = Role::where('slug', 'asistente')->first();
		$user = User::where('name', 'asistenteedl')->first();
		if($role && $user){
		    $user->attachRole($role);
		}
	}
}
?>