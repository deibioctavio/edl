<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('permission_role')->delete();
        DB::table('permissions')->delete();
        
        /*
        * Areas/Dependencias Institución
        */

        DB::table('permissions')->insert([
            'name' => 'Adicionar Área',
            'slug' => 'area.add',
            'description' => 'Permiso de Adicionar Área',
            'group' => 'area',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Editar Área',
            'slug' => 'area.edit',
            'description' => 'Permiso de Editar Área',
            'group' => 'area',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Eliminar Área',
            'slug' => 'area.delete',
            'description' => 'Permiso de Eliminar Área (soft)',
            'group' => 'area',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Área',
            'slug' => 'area.show',
            'description' => 'Permiso de Ver Área (Información)',
            'group' => 'area',
        ]);

         DB::table('permissions')->insert([
            'name' => 'Ver Todas las Áreas',
            'slug' => 'area.showall',
            'description' => 'Permiso de Ver Todas las Áreas',
            'group' => 'area',
        ]);

        /*
        * Proyectos Institución
        */

        DB::table('permissions')->insert([
            'name' => 'Adicionar Proyecto',
            'slug' => 'proyecto.add',
            'description' => 'Permiso de Adicionar Proyecto',
            'group' => 'proyecto',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Editar Proyecto',
            'slug' => 'proyecto.edit',
            'description' => 'Permiso de Editar Proyecto',
            'group' => 'proyecto',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Eliminar Proyecto',
            'slug' => 'proyecto.delete',
            'description' => 'Permiso de Eliminar Proyecto',
            'group' => 'proyecto',
        ]);

         DB::table('permissions')->insert([
            'name' => 'Ver Proyectos por Área',
            'slug' => 'proyecto.showbyarea',
            'description' => 'Permiso de Ver Proyectos por Área',
            'group' => 'proyecto',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Todos los Proyectos',
            'slug' => 'proyecto.showall',
            'description' => 'Permiso Ver Todos los Proyectos',
            'group' => 'proyecto',
        ]);

         DB::table('permissions')->insert([
            'name' => 'Ver Proyecto',
            'slug' => 'proyecto.show',
            'description' => 'Permiso de Ver Proyecto',
            'group' => 'proyecto',
        ]);

        /*
        * Metas Institucionales
        */
        
        DB::table('permissions')->insert([
            'name' => 'Adicionar Meta',
            'slug' => 'meta.add',
            'description' => 'Permiso de Adicionar Meta',
            'group' => 'meta',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Editar Meta',
            'slug' => 'meta.edit',
            'description' => 'Permiso de Editar Meta',
            'group' => 'meta',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Eliminar Meta',
            'slug' => 'meta.delete',
            'description' => 'Permiso de Eliminar Meta (soft)',
            'group' => 'meta',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Meta',
            'slug' => 'meta.show',
            'description' => 'Permiso de Ver Meta',
            'group' => 'meta',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Metas por Proyecto',
            'slug' => 'meta.showallbyproyecto',
            'description' => 'Permiso de Ver Metas por Proyecto',
            'group' => 'meta',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Todas las Metas',
            'slug' => 'meta.showall',
            'description' => 'Permiso de Ver Todas Metas',
            'group' => 'meta',
        ]);

        /*
        * Niveles jerárquicos
        */
        
        DB::table('permissions')->insert([
            'name' => 'Adicionar Nivel Jerarquico',
            'slug' => 'niveljerarquico.add',
            'description' => 'Permiso de Adicionar Nivel Jerarquico',
            'group' => 'nivel jerarquico',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Editar Nivel Jerarquico',
            'slug' => 'niveljerarquico.edit',
            'description' => 'Permiso de Editar Nivel Jerarquico',
            'group' => 'nivel jerarquico',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Eliminar Nivel Jerarquico',
            'slug' => 'niveljerarquico.delete',
            'description' => 'Permiso de Eliminar Nivel Jerarquico',
            'group' => 'nivel jerarquico',
        ]);


        DB::table('permissions')->insert([
            'name' => 'Ver Todas los Niveles Jerarquicos',
            'slug' => 'niveljerarquico.showall',
            'description' => 'Permiso de Ver Todos Nivel Jerarquico',
            'group' => 'nivel jerarquico',
        ]);

        /*
        * Cargos por niveles jerárquicos
        */
        
        DB::table('permissions')->insert([
            'name' => 'Adicionar Cargo',
            'slug' => 'cargo.add',
            'description' => 'Permiso de Adicionar Cargo',
            'group' => 'cargo',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Editar Cargo',
            'slug' => 'cargo.edit',
            'description' => 'Permiso de Editar Cargo',
            'group' => 'cargo',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Eliminar Cargo',
            'slug' => 'cargo.delete',
            'description' => 'Permiso de Eliminar Cargo',
            'group' => 'cargo',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Cargo',
            'slug' => 'cargo.show',
            'description' => 'Permiso de Ver Cargo',
            'group' => 'cargo',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Metas por Proyecto',
            'slug' => 'cargo.showallbyniveljerarquico',
            'description' => 'Permiso de Ver Cargo por Nivel Jerarquico',
            'group' => 'cargo',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Todas los Cargos',
            'slug' => 'cargo.showall',
            'description' => 'Permiso de Ver Todos Cargos',
            'group' => 'cargo',
        ]);

        /*
        * Compromisos por niveles Cargos
        */
        
        DB::table('permissions')->insert([
            'name' => 'Adicionar Compromiso',
            'slug' => 'compromiso.add',
            'description' => 'Permiso de Adicionar Compromiso',
            'group' => 'compromiso',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Editar Compromiso',
            'slug' => 'compromiso.edit',
            'description' => 'Permiso de Editar Compromiso',
            'group' => 'compromiso',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Eliminar Compromiso',
            'slug' => 'compromiso.delete',
            'description' => 'Permiso de Eliminar Compromiso',
            'group' => 'compromiso',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Compromiso',
            'slug' => 'compromiso.show',
            'description' => 'Permiso de Ver Compromiso',
            'group' => 'compromiso',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Compromisos por Cargo',
            'slug' => 'compromiso.showallbycargo',
            'description' => 'Permiso de Ver Compromiso por Cargo',
            'group' => 'compromiso',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Todas los Compromiso',
            'slug' => 'compromiso.showall',
            'description' => 'Permiso de Ver Todos Compromiso',
            'group' => 'compromiso',
        ]);

        /*
        * Competencias (Evaluación Comportamental)
        */
        
        DB::table('permissions')->insert([
            'name' => 'Adicionar Competencia',
            'slug' => 'competencia.add',
            'description' => 'Permiso de Adicionar Competencia',
            'group' => 'competencia',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Editar Competencia',
            'slug' => 'competencia.edit',
            'description' => 'Permiso de Editar Competencia',
            'group' => 'competencia',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Eliminar Competencia',
            'slug' => 'competencia.delete',
            'description' => 'Permiso de Eliminar Competencia',
            'group' => 'competencia',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Competencia',
            'slug' => 'competencia.show',
            'description' => 'Permiso de Ver Competencia',
            'group' => 'competencia',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Todas las Competencias',
            'slug' => 'competencia.showall',
            'description' => 'Permiso de Ver Todas Las Competencia',
            'group' => 'competencia',
        ]);

        /*
        * Conductas Asociadas (Competencias)
        */
        
        DB::table('permissions')->insert([
            'name' => 'Adicionar Conducta Asociada',
            'slug' => 'conducta.add',
            'description' => 'Permiso de Adicionar Conducta',
            'group' => 'conducta',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Editar Conducta',
            'slug' => 'conducta.edit',
            'description' => 'Permiso de Editar Conducta',
            'group' => 'conducta',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Eliminar Conducta',
            'slug' => 'conducta.delete',
            'description' => 'Permiso de Eliminar Conducta',
            'group' => 'conducta',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Conducta',
            'slug' => 'conducta.show',
            'description' => 'Permiso de Ver Conducta',
            'group' => 'conducta',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Todas las Conducta Asociada',
            'slug' => 'conducta.showall',
            'description' => 'Permiso de Ver Todas Las Conducta',
            'group' => 'conducta',
        ]);

        /*
        * Configuracion Evaluacion
        */
        
        DB::table('permissions')->insert([
            'name' => 'Adicionar Configuración',
            'slug' => 'configuracionevaluacion.add',
            'description' => 'Permiso de Adicionar Configuración de Evaluacion',
            'group' => 'evaluacion',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Editar Configuración de Evaluacion',
            'slug' => 'configuracionevaluacion.edit',
            'description' => 'Permiso de Editar Configuración de Evaluacion',
            'group' => 'evaluacion',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Eliminar Configuración de Evaluacion',
            'slug' => 'configuracionevaluacion.delete',
            'description' => 'Permiso de Eliminar Configuración de Evaluacion',
            'group' => 'evaluacion',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Configuración de Evaluacion',
            'slug' => 'configuracionevaluacion.show',
            'description' => 'Permiso de Ver Configuración de Evaluacion',
            'group' => 'evaluacion',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Todas las Configuraciones',
            'slug' => 'configuracionevaluacion.showall',
            'description' => 'Permiso de Ver Todas Las Configuraciones de Evaluaciones',
            'group' => 'evaluacion',
        ]);

        /*
        * Perfil de usuario
        */

        DB::table('permissions')->insert([
            'name' => 'Editar Información Perfil',
            'slug' => 'profile.edit',
            'description' => 'Permiso de Editar Información Perfil',
            'group' => 'perfil',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Remover Rol a Usuario',
            'slug' => 'userrole.delete',
            'description' => 'Permiso de Remover Rol a Usuario',
            'group' => 'perfil',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver todos los roles por usuario',
            'slug' => 'userrole.showall',
            'description' => 'Permiso de Ver todos los roles por usuario',
            'group' => 'perfil',
        ]);

        /*
        * SGC Procesos
        */

        DB::table('permissions')->insert([
            'name' => 'Adicionar Proceso',
            'slug' => 'proceso.add',
            'description' => 'Permiso de Adicionar Proceso',
            'group' => 'sgc',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Editar Proceso',
            'slug' => 'proceso.edit',
            'description' => 'Permiso de Editar Proceso',
            'group' => 'sgc',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Eliminar Proceso',
            'slug' => 'proceso.delete',
            'description' => 'Permiso de Eliminar Proceso (soft)',
            'group' => 'sgc',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Proceso',
            'slug' => 'proceso.show',
            'description' => 'Permiso de Ver Proceso (Información)',
            'group' => 'sgc',
        ]);

         DB::table('permissions')->insert([
            'name' => 'Ver Todas las Proceso',
            'slug' => 'proceso.showall',
            'description' => 'Permiso de Ver Todas las Proceso',
            'group' => 'sgc',
        ]);

         /*
        * SGC Actividades/Subprocesos
        */

         DB::table('permissions')->insert([
            'name' => 'Adicionar Actividad',
            'slug' => 'actividad.add',
            'description' => 'Permiso de Adicionar Actividad',
            'group' => 'sgc',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Editar Actividad',
            'slug' => 'actividad.edit',
            'description' => 'Permiso de Editar Actividad',
            'group' => 'sgc',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Eliminar Actividad',
            'slug' => 'actividad.delete',
            'description' => 'Permiso de Eliminar Actividad (soft)',
            'group' => 'sgc',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Actividad',
            'slug' => 'actividad.show',
            'description' => 'Permiso de Ver Actividad (Información)',
            'group' => 'sgc',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Todas las Actividades',
            'slug' => 'actividad.showall',
            'description' => 'Permiso de Ver Todas las Actividades',
            'group' => 'sgc',
        ]);


        $role = Role::where('slug', 'admin')->first();
        $permissions = Permission::get();

        if($role && $permissions){

            foreach ($permissions as $p) {
                $role->attachPermission($p);
            }
        }

        $role = Role::where('slug', 'asistente')->first();
        $permissions = Permission::where('slug', 'LIKE', '%.show%')->get();

        if($role && $permissions){

            foreach ($permissions as $p) {
                $role->attachPermission($p);
            }
        }
    }
}
