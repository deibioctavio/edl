<?php

use Illuminate\Database\Seeder;

class TipoEvaluacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_evaluaciones')->delete();

        DB::table('tipo_evaluaciones')->insert([
            'id' => 1,
            'name' => 'Anual u Ordinaria',
            'description' => 'Tipo de Evaluación Anual u Ordinaria - CNSC Acuerdor 137 y 138 de 2010',
            'dias_evaluacion' => 360,
            'porcentaje_evaluado' => 100.00,
        ]);

        DB::table('tipo_evaluaciones')->insert([
            'id' => 2,
            'name' => 'Periodo de Prueba',
            'description' => 'Tipo de Evaluación Periodo de Prueba - CNSC Acuerdor 137 y 138 de 2010',
            'dias_evaluacion' => 90,
            'porcentaje_evaluado' => 100.00,
        ]);

        DB::table('tipo_evaluaciones')->insert([
            'id' => 3,
            'name' => 'Extraordinaria',
            'description' => 'Tipo de Evaluación Extraordinaria - CNSC Acuerdor 137 y 138 de 2010',
            'dias_evaluacion' => 90,
            'porcentaje_evaluado' => 100.00,
        ]);

        DB::table('tipo_evaluaciones')->insert([
            'id' => 4,
            'name' => 'Parcial Primer Semestre',
            'description' => 'Tipo de Evaluación Parcial Primer Semestre - CNSC Acuerdor 137 y 138 de 2010',
            'dias_evaluacion' => 180,
            'porcentaje_evaluado' => 50.00,
        ]);

        DB::table('tipo_evaluaciones')->insert([
            'id' => 5,
            'name' => 'Parcial Segundo Semestre',
            'description' => 'Tipo de Evaluación Parcial Segundo Semestre - CNSC Acuerdor 137 y 138 de 2010',
            'dias_evaluacion' => 180,
            'porcentaje_evaluado' => 50.00,
        ]);

        DB::table('tipo_evaluaciones')->insert([
            'id' => 6,
            'name' => 'Parcial Eventual Primer Semestre',
            'description' => 'Tipo de Evaluación Parcial Segundo Semestre - CNSC Acuerdor 137 y 138 de 2010',
            'dias_evaluacion' => 180,
            'porcentaje_evaluado' => 50.00,
        ]);

        DB::table('tipo_evaluaciones')->insert([
            'id' => 7,
            'name' => 'Parcial Eventual Segundo Semestre',
            'description' => 'Tipo de Evaluación Parcial Segundo Semestre - CNSC Acuerdor 137 y 138 de 2010',
            'dias_evaluacion' => 180,
            'porcentaje_evaluado' => 50.00,
        ]);
    }
}
