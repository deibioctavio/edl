<?php

use Illuminate\Database\Seeder;

class ProcesoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('procesos')->delete();

        DB::table('procesos')->insert([
            'id' => 1,
            'name' => 'Gestión Gerencia',
            'identificator' => 'PGG',
            'description' => 'PGG - Gestión Gerencia',
            'area_id' => 1, 
            'macroproceso_id' => 1, //Estratégico - Blanco        
        ]);

        DB::table('procesos')->insert([
            'id' => 2,
            'name' => 'Direccionamiento Estratégico',
            'identificator' => 'PDE',
            'description' => 'PDE - Direccionamiento Estratégico',
            'area_id' => 35, 
            'macroproceso_id' => 1, //Estratégico - Blanco        
        ]);

        DB::table('procesos')->insert([
            'id' => 3,
            'name' => 'Planificación y Ordenamiento Territorial',
            'identificator' => 'POT',
            'description' => 'POT - Planificación y Ordenamiento Territorial',
            'area_id' => 35, 
            'macroproceso_id' => 2, //Misional - Verde
        ]);

        DB::table('procesos')->insert([
            'id' => 4,
            'name' => 'Desarrollo Económico y Competitividad',
            'identificator' => 'PEC',
            'description' => 'POT - Desarrollo Económico y Competitividad',
            'area_id' => 24, 
            'macroproceso_id' => 2, //Misional - Verde
        ]);

        DB::table('procesos')->insert([
            'id' => 5,
            'name' => 'Obras Públicas e Infraestructura',
            'identificator' => 'POI',
            'description' => 'POI - Obras Públicas e Infraestructura',
            'area_id' => 26, 
            'macroproceso_id' => 2, //Misional - Verde
        ]);

        DB::table('procesos')->insert([
            'id' => 6,
            'name' => 'Desarrollo Social y Político',
            'identificator' => 'PSP',
            'description' => 'PSP - Desarrollo Social y Político',
            'area_id' => 22, 
            'macroproceso_id' => 2, //Misional - Verde
        ]);

        DB::table('procesos')->insert([
            'id' => 7,
            'name' => 'Desarrollo Integral del Sistema General de Seguridad (SGSS)Social en Salud a Nivel Municipal',
            'identificator' => 'PSS',
            'description' => 'PSS - Desarrollo Integral del Sistema General de Seguridad (SGSS)Social en Salud a Nivel Municipal',
            'area_id' => 23, 
            'macroproceso_id' => 2, //Misional - Verde
        ]);

        DB::table('procesos')->insert([
            'id' => 8,
            'name' => 'Servicio Educativo',
            'identificator' => 'PSE',
            'description' => 'PSE - Servicio Educativo',
            'area_id' => 25, 
            'macroproceso_id' => 2, //Misional - Verde
        ]);

        DB::table('procesos')->insert([
            'id' => 9,
            'name' => 'Gobierno',
            'identificator' => 'PGO',
            'description' => 'PGO - Gobierno',
            'area_id' => 21, 
            'macroproceso_id' => 2, //Misional - Verde
        ]);

        DB::table('procesos')->insert([
            'id' => 10,
            'name' => 'Transito y Movilidad',
            'identificator' => 'PTM',
            'description' => 'PTM - Transito y Movilidad',
            'area_id' => 27, 
            'macroproceso_id' => 2, //Misional - Verde
        ]);

        DB::table('procesos')->insert([
            'id' => 11,
            'name' => 'Servicio y Atención al Ciudadano',
            'identificator' => 'PSC',
            'description' => 'PSC - Servicio y Atención al Ciudadano',
            'area_id' => 31, 
            'macroproceso_id' => 3, //De Apoyo - Amarillo
        ]);

        DB::table('procesos')->insert([
            'id' => 12,
            'name' => 'Talento Humano',
            'identificator' => 'PTH',
            'description' => 'PTH - Talento Humano',
            'area_id' => 31, 
            'macroproceso_id' => 3, //De Apoyo - Amarillo
        ]);

        DB::table('procesos')->insert([
            'id' => 13,
            'name' => 'Gestión Administrativa',
            'identificator' => 'PGA',
            'description' => 'PGA - Gestión Administrativa',
            'area_id' => 34, 
            'macroproceso_id' => 3, //De Apoyo - Amarillo
        ]);

        DB::table('procesos')->insert([
            'id' => 14,
            'name' => 'Gestión Financiera',
            'identificator' => 'PGF',
            'description' => 'PGF - Gestión Financiera',
            'area_id' => 33, 
            'macroproceso_id' => 3, //De Apoyo - Amarillo
        ]);

        DB::table('procesos')->insert([
            'id' => 15,
            'name' => 'Juríico',
            'identificator' => 'PJU',
            'description' => 'PJU - Juríico',
            'area_id' => 32, 
            'macroproceso_id' => 3, //De Apoyo - Amarillo
        ]);

        DB::table('procesos')->insert([
            'id' => 16,
            'name' => 'Control de Verificación y Evaluación',
            'identificator' => 'PCE',
            'description' => 'PCE - Control de Verificación y Evaluación',
            'area_id' => 36, 
            'macroproceso_id' => 4, //De Evaliación - Rojo
        ]);

        DB::table('procesos')->insert([
            'id' => 17,
            'name' => 'Gestión TIC',
            'identificator' => 'PGT',
            'description' => 'PGT - Gestión TIC',
            'area_id' => 28, 
            'macroproceso_id' => 2, //Misional - Verde
        ]);

        DB::table('procesos')->insert([
            'id' => 18,
            'name' => 'Infraestructura Tecnológica',
            'identificator' => 'PIT',
            'description' => 'PIT - Infraestructura Tecnológica',
            'area_id' => 28, 
            'macroproceso_id' => 3, //De Apoyo - Amarillo
        ]);
    }
}
