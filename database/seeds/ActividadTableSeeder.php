<?php

use Illuminate\Database\Seeder;
use App\Proceso;

class ActividadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actividades')->delete();

        $procesos = Proceso::get();

        if($procesos){

            $i = 0;
            $k = 0;
            
            foreach ($procesos as $p) {

                $totalActividades = rand(5,12);
                $actividadesAttr = array();

                for( $j = 0;  $j < $totalActividades; $j++ ){

                    $actividadesAttr[$j]['name']  = "Actividad({$k} -> {$p->identificator}): {$p->name} {$j} -  {$i}";
                    $actividadesAttr[$j]['proceso_id'] = $p->id;
                    $actividadesAttr[$j]['description'] = "Descrpción Actividad({$k} -> {$p->identificator}): {$p->name} {$j} -  {$i}";
                    $k++;
                }

                DB::table('actividades')->insert($actividadesAttr);
                $i++;
            }
        }
    }
}