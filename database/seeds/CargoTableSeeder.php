<?php

use Illuminate\Database\Seeder;
use App\NivelJerarquico;

class CargoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cargos')->delete();

        $i = 200;

        for($j = 2; $j < 11; $j+=3){

        	DB::table('cargos')->insert([
	            'name' => 'tecnico'.sprintf("%02d", $j)."-".$i,
	            'description' => 'Técnico',
	            'grade' => sprintf("%02d", $j),
	            'code' => $i,
	            'nivel_jerarquico_id' => 2,
                'vigencia' => 2016
        	]);

        	$i++;
        }

        for($j = 2; $j < 11; $j+=3){

        	DB::table('cargos')->insert([
	            'name' => 'asistencial'.sprintf("%02d", $j)."-".$i,
	            'description' => 'Asistencial',
	            'grade' => sprintf("%02d", $j),
	            'code' => $i,
	            'nivel_jerarquico_id' => 3,
                'vigencia' => 2016
        	]);

        	$i++;
        }

        for($j = 2; $j < 11; $j+=3){

        	DB::table('cargos')->insert([
	            'name' => 'profesional'.sprintf("%02d", $j)."-".$i,
	            'description' => 'Profesional',
	            'grade' => sprintf("%02d", $j),
	            'code' => $i,
	            'nivel_jerarquico_id' => 1,
                'vigencia' => 2016
        	]);

        	$i++;
        }

        for(; $j < 25; $j+=4){

        	DB::table('cargos')->insert([
	            'name' => 'profesional_especializado'.sprintf("%02d", $j)."-".$i,
	            'description' => 'Profesional Especializado',
	            'grade' => sprintf("%02d", $j),
	            'code' => $i,
	            'nivel_jerarquico_id' => 1,
                'vigencia' => 2016
        	]);

        	$i++;
        }

        for(; $j < 43; $j+=2){

        	DB::table('cargos')->insert([
	            'name' => 'profesional_maestria'.sprintf("%02d", $j)."-".$i,
	            'description' => 'Profesional con Maestria',
	            'grade' => sprintf("%02d", $j),
	            'code' => $i,
	            'nivel_jerarquico_id' => 1,
                'vigencia' => 2016
        	]);

        	$i++;
        }

        for(; $j < 57; $j+=3){

        	DB::table('cargos')->insert([
	            'name' => 'profesional_doctorado'.sprintf("%02d", $j)."-".$i,
	            'description' => 'Profesional con Doctorado/Postdocorado',
	            'grade' => sprintf("%02d", $j),
	            'code' => $i,
	            'nivel_jerarquico_id' => 1,
                'vigencia' => 2016
        	]);

        	$i++;
        }
	}
}
