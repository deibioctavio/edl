<?php

use Illuminate\Database\Seeder;

class VigenciasReferenciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vigencias_referencias')->delete();

        for( $i = 2015; $i < date('Y')+50;$i++ ){
        	DB::table('vigencias_referencias')->insert([
            'id' => $i -2014,
            'valor' => $i
        	]);	
        }
    }
}
