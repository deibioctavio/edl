<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'id' => 1,
            'name' => 'usuarioadmin',
            'email' => 'deibioctavio@gmail.com',
            'password' => bcrypt('deibioctavio'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'name' => 'administradoredl',
            'email' => 'edl@armenia.gov.co',
            'password' => bcrypt('edl2016'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 3,
            'name' => 'evaluadoredl',
            'email' => 'evaluadoredl@armenia.gov.co',
            'password' => bcrypt('evaluadoredl'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 4,
            'name' => 'funcionarioedl',
            'email' => 'funcionarioedl@armenia.gov.co',
            'password' => bcrypt('funcionarioedl'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 5,
            'name' => 'user'.getTimestamp(),
            'email' => 'user'.getTimestamp().'@armenia.gov.co',
            'password' => bcrypt('funcionarioedl'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 6,
            'name' => 'asistenteedl',
            'email' => 'asistenteedl@armenia.gov.co',
            'password' => bcrypt('asistenteedl'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 7,
            'name' => 'evaluadoredl001',
            'email' => 'evaluadoredl001@armenia.gov.co',
            'password' => bcrypt('evaluadoredl001'),
            'created_at' => getTimestamp(),
        ]);
        
        DB::table('users')->insert([
            'id' => 8,
            'name' => 'evaluadoredl002',
            'email' => 'evaluadoredl002@armenia.gov.co',
            'password' => bcrypt('evaluadoredl002'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 9,
            'name' => 'funcionarioedl001',
            'email' => 'funcionarioedl001@armenia.gov.co',
            'password' => bcrypt('funcionarioedl001'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 10,
            'name' => 'funcionarioedl002',
            'email' => 'funcionarioedl002@armenia.gov.co',
            'password' => bcrypt('funcionarioedl002'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 11,
            'name' => 'funcionarioedl003',
            'email' => 'funcionarioedl003@armenia.gov.co',
            'password' => bcrypt('funcionarioedl003'),
            'created_at' => getTimestamp(),
        ]);
    }
}
