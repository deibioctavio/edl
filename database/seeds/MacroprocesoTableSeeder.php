<?php

use Illuminate\Database\Seeder;

class MacroprocesoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('macroprocesos')->delete();

        DB::table('macroprocesos')->insert([
            'id' => 1,
            'name' => 'Estratégico',
            'description' => 'Sirven para formular y revisas el direccionamiento estratégico de la Alcaldía enmarcado en los objetivos de los ejes estretégicos y las politicas del plan de Desarrollo Municipal (Carta de Navegación de la Administración)',
            'color' => '#FFFFFF',            
        ]);

        DB::table('macroprocesos')->insert([
            'id' => 2,
            'name' => 'Misional',
            'description' => 'Contribuyen con el cumplimiento de las funciones que establecen la Constitución y la Ley de acuerdo con su nivel y naturaleza Jurídica',
            'color' => '#00FF00',            
        ]);

        DB::table('macroprocesos')->insert([
            'id' => 3,
            'name' => 'De Apoyo',
            'description' => 'Contribuyen con la gestión de los recursos humanos, físicos, financieros y tecnológicos que son necesarios para el cumplimiento de la misión Institucional',
            'color' => '#E4A017',            
        ]);

        DB::table('macroprocesos')->insert([
            'id' => 4,
            'name' => 'De Evaluación',
            'description' => 'Permite hacer seguimiento a los procesos de manera objetiva para medir el desempeño y la mejora continua de la gestión institucional',
            'color' => '#FF5C5E',            
        ]);
    }
}
